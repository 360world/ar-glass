﻿using System;
using System.Collections.Generic;
using System.Linq;
using Soufflage.Core.Entities;
using Soufflage.Core.Geography;

namespace GeographyTool
{
    public class Program
    {
        private static readonly Tuple<GeoCoordinate, double> CameraPosition = Tuple.Create(new GeoCoordinate(47.483715, 19.015117), 224.0);

        private static readonly Dictionary<string, Tuple<GeoCoordinate, double>> Origins = new Dictionary<string, Tuple<GeoCoordinate, double>>
        {
            ["360world"] = Tuple.Create(new GeoCoordinate(47.483715, 19.015117), 224.0),
            ["moszkvater"] = Tuple.Create(new GeoCoordinate(47.507088, 19.024166), 131.0),
            ["citadella"] = Tuple.Create(new GeoCoordinate(47.486747, 19.048014), 220.0),
            //["repter"] = Tuple.Create(new GeoCoordinate(47.439331, 19.261805), 0.0),
        };

        private static readonly Dictionary<string, Tuple<GeoCoordinate, double>> Pois = new Dictionary<string, Tuple<GeoCoordinate, double>>
        {
            ["citadella"] = Tuple.Create(new GeoCoordinate(47.486747, 19.048014), 220.0),
            ["csorsz57"] = Tuple.Create(new GeoCoordinate(47.490689, 19.019438), 157.0),
            ["megyeri"] = Tuple.Create(new GeoCoordinate(47.610391, 19.085956), 100.0),
            ["orfk"] = Tuple.Create(new GeoCoordinate(47.550035, 19.001812), 412.0),
            ["matyas"] = Tuple.Create(new GeoCoordinate(47.5020159, 19.032185), 172.0),
            ["galeria"] = Tuple.Create(new GeoCoordinate(47.496197, 19.039666), 130.0),
            //["repter-tuzoltosag"] = Tuple.Create(new GeoCoordinate(47.437785, 19.255424), 0.0),
        };

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Geography Tool");
            Console.WriteLine();
            Console.WriteLine($"  camera position: latitude {CameraPosition.Item1.Latitude}, longitude {CameraPosition.Item1.Longitude}, altitude {CameraPosition.Item2}");
            Console.WriteLine();
            foreach (var originsKey in Origins.Keys)
            {
                Console.WriteLine($"  available origin for data points -> {originsKey}");
            }
            Console.WriteLine();
            var selectedOrigin = "";
            while (!Origins.ContainsKey(selectedOrigin))
            {
                Console.Write("Please enter a valid origin identifier: ");
                selectedOrigin = Console.ReadLine() ?? "";
            }
            Console.WriteLine();
            var origin = Origins[selectedOrigin].Item1;
            var originHeight = Origins[selectedOrigin].Item2;
            Pois["# CAMERA OFFSET #"] = CameraPosition;
            foreach (var poi in Pois)
            {
                var geoCoordinate = poi.Value.Item1;
                var geoHeight = poi.Value.Item2;
                var onX = new GeoCoordinate
                {
                    Latitude = origin.Latitude,
                    Longitude = geoCoordinate.Longitude
                };
                var latmul = geoCoordinate.Latitude < origin.Latitude ? -1 : 1;
                var lonmul = geoCoordinate.Longitude < origin.Longitude ? -1 : 1;
                if(poi.Key.Contains('#')) Console.WriteLine();
                Console.WriteLine($"  coordinate for {poi.Key} is [{lonmul * origin.CalculateDistanceBetween(onX)}, {latmul * onX.CalculateDistanceBetween(geoCoordinate)}, {geoHeight - originHeight}]");
            }
            Console.WriteLine();
            Console.WriteLine("Done.");
            Console.ReadLine();
        }
    }
}
