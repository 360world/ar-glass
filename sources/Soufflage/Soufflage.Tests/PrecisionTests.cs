﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Should;
using Soufflage.Core.Geography;

namespace Soufflage.Tests
{
    [TestClass]
    public class PrecisionTests
    {
        [TestMethod]
        public void AreaApproximationTest1()
        {
            // accuracy for 100.0 meters
            var a = new Accuracy(100.0);

            var x = a.CircularPointCount();
            x.ShouldBeLessThanOrEqualTo(12);
            x.ShouldBeGreaterThan(0);

            var y = a.ArcPointCount(180.0);
            y.ShouldBeLessThanOrEqualTo(6);
            y.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        public void AreaApproximationTest2()
        {
            // accuracy for 1.0 kilometer
            var a = new Accuracy(1000.0);

            var x = a.CircularPointCount();
            x.ShouldBeLessThanOrEqualTo(29);
            x.ShouldBeGreaterThan(0);

            var y = a.ArcPointCount(180.0);
            y.ShouldBeLessThanOrEqualTo(14);
            y.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        public void AreaApproximationTest3()
        {
            // accuracy for 100.0 kilometers
            var a = new Accuracy(100 * 1000.0);

            var x = a.CircularPointCount();
            x.ShouldBeLessThanOrEqualTo(287);
            x.ShouldBeGreaterThan(0);

            var y = a.ArcPointCount(180.0);
            y.ShouldBeLessThanOrEqualTo(144);
            y.ShouldBeGreaterThan(0);
        }
    }
}
