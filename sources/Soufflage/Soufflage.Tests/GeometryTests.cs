﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ClipperLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Should;
using Soufflage.Core.Clipper;
using Soufflage.Core.Entities;
using Soufflage.Core.Geography;
using Soufflage.Core.Utilities;

namespace Soufflage.Tests
{
    [TestClass]
    public class GeometryTests
    {
        [TestMethod]
        public void UnitConversionTest1()
        {
            // BUD liszt ferenc airport
            var bud = new GeoCoordinate
            {
                Latitude = 47.438437,
                Longitude = 19.252274
            };

            var step1 = bud.ToIntPoint();

            var step2 = step1.ToGeoCoordinate();

            EntityHelpers.ApproximatelyEquals(bud, step2).ShouldBeTrue();
        }

        [TestMethod]
        public void UnitConversionTest2()
        {
            // founders peak
            var fpk = new GeoCoordinate
            {
                Latitude = -37.951033,
                Longitude = 144.424868
            };

            var step1 = fpk.ToIntPoint();

            var step2 = step1.ToGeoCoordinate();

            EntityHelpers.ApproximatelyEquals(fpk, step2).ShouldBeTrue();
        }

        [TestMethod]
        public void BasicPolygonAdditionTest1()
        {
            var p1 = new List<IntPoint>
            {
                new IntPoint(0,0),
                new IntPoint(0,2),
                new IntPoint(2,2),
                new IntPoint(2,0)
            };

            var p2 = new List<IntPoint>
            {
                new IntPoint(1,1),
                new IntPoint(1,3),
                new IntPoint(3,3),
                new IntPoint(3,1)
            };

            var result = p1.UnionWithClipper(p2);

            result.Count.ShouldEqual(8);
            foreach (var c in new List<IntPoint>
            {
                new IntPoint(0,0),
                new IntPoint(0,2),
                new IntPoint(1,2),
                new IntPoint(1,3),
                new IntPoint(3,3),
                new IntPoint(3,1),
                new IntPoint(2,1),
                new IntPoint(2,0),
            })
            {
                result.Count(p => p.X == c.X && p.Y == c.Y).ShouldEqual(1);
            }

        }

        [TestMethod]
        public void BasicPolygonAdditionTest2()
        {
            var p1 = new List<IntPoint>
            {
                new IntPoint(-2,1),
                new IntPoint(1,1),
                new IntPoint(1,-2),
                new IntPoint(-2,-2)
            };

            var p2 = new List<IntPoint>
            {
                new IntPoint(-1,-1),
                new IntPoint(-1,2),
                new IntPoint(2,2),
                new IntPoint(2,-1)
            };

            var result = p1.UnionWithClipper(p2);

            result.Count.ShouldEqual(8);
            foreach (var c in new List<IntPoint>
            {
                new IntPoint(-2,-2),
                new IntPoint(-2,1),
                new IntPoint(-1,1),
                new IntPoint(-1,2),
                new IntPoint(2,2),
                new IntPoint(2,-1),
                new IntPoint(1,-1),
                new IntPoint(1,-2),
            })
            {
                result.Count(p => p.X == c.X && p.Y == c.Y).ShouldEqual(1);
            }
        }

        [TestMethod]
        public void BasicPolygonIntersectionTest1()
        {
            var p1 = new List<IntPoint>
            {
                new IntPoint(0,0),
                new IntPoint(0,2),
                new IntPoint(2,2),
                new IntPoint(2,0)
            };

            var p2 = new List<IntPoint>
            {
                new IntPoint(1,1),
                new IntPoint(1,3),
                new IntPoint(3,3),
                new IntPoint(3,1)
            };

            var result = p1.IntersectWithClipper(p2);

            result.Count.ShouldEqual(4);
            foreach (var c in new List<IntPoint>
            {
                new IntPoint(1,1),
                new IntPoint(1,2),
                new IntPoint(2,2),
                new IntPoint(2,1),
            })
            {
                result.Count(p => p.X == c.X && p.Y == c.Y).ShouldEqual(1);
            }
        }

        [TestMethod]
        public void BasicPolygonIntersectionTest2()
        {
            var p1 = new List<IntPoint>
            {
                new IntPoint(-2,1),
                new IntPoint(1,1),
                new IntPoint(1,-2),
                new IntPoint(-2,-2)
            };

            var p2 = new List<IntPoint>
            {
                new IntPoint(-1,-1),
                new IntPoint(-1,2),
                new IntPoint(2,2),
                new IntPoint(2,-1)
            };

            var result = p1.IntersectWithClipper(p2);

            result.Count.ShouldEqual(4);
            foreach (var c in new List<IntPoint>
            {
                new IntPoint(-1,1),
                new IntPoint(1,1),
                new IntPoint(1,-1),
                new IntPoint(-1,-1),
            })
            {
                result.Count(p => p.X == c.X && p.Y == c.Y).ShouldEqual(1);
            }
        }

        [TestMethod]
        public void BasicPolygonSubtractionTest1()
        {
            var p1 = new List<IntPoint>
            {
                new IntPoint(0,0),
                new IntPoint(0,2),
                new IntPoint(2,2),
                new IntPoint(2,0)
            };

            var p2 = new List<IntPoint>
            {
                new IntPoint(1,1),
                new IntPoint(1,3),
                new IntPoint(3,3),
                new IntPoint(3,1)
            };

            var result = p1.SubtractWithClipper(p2);

            result.Count.ShouldEqual(6);
            foreach (var c in new List<IntPoint>
            {
                new IntPoint(0,0),
                new IntPoint(0,2),
                new IntPoint(1,2),
                new IntPoint(1,1),
                new IntPoint(2,1),
                new IntPoint(2,0),
            })
            {
                result.Count(p => p.X == c.X && p.Y == c.Y).ShouldEqual(1);
            }
        }

        [TestMethod]
        public void BasicPolygonSubtractionTest2()
        {
            var p1 = new List<IntPoint>
            {
                new IntPoint(-2,1),
                new IntPoint(1,1),
                new IntPoint(1,-2),
                new IntPoint(-2,-2)
            };

            var p2 = new List<IntPoint>
            {
                new IntPoint(-1,-1),
                new IntPoint(-1,2),
                new IntPoint(2,2),
                new IntPoint(2,-1)
            };

            var result = p1.SubtractWithClipper(p2);

            result.Count.ShouldEqual(6);
            foreach (var c in new List<IntPoint>
            {
                new IntPoint(-2,-2),
                new IntPoint(-2,1),
                new IntPoint(-1,1),
                new IntPoint(-1,-1),
                new IntPoint(1,-1),
                new IntPoint(1,-2),
            })
            {
                result.Count(p => p.X == c.X && p.Y == c.Y).ShouldEqual(1);
            }
        }

        [TestMethod]
        public void AdvancedPolygonAdditionTest()
        {
            var dist = 1000;

            // BUD liszt ferenc airport
            var bud = new GeoCoordinate
            {
                Latitude = 47.438437,
                Longitude = 19.252274
            };

            // point 1km east of bud
            var budOff = bud.CalculateDestinationPoint(dist, 90);

            var a = new Accuracy(dist);

            var cpc = a.CircularPointCount();
            var apc = a.ArcPointCount(120); // triangle with 3 r = 1000 sides, 60° each, so 120° for the part that's merged

            var c1 = new List<GeoCoordinate>();
            for (var i = 0; i < cpc; i++) c1.Add(bud.CalculateDestinationPoint(dist, i * a.AnglePerIteration()));

            var c2 = new List<GeoCoordinate>();
            for (var i = 0; i < cpc; i++) c2.Add(budOff.CalculateDestinationPoint(dist, i * a.AnglePerIteration()));

            var p1 = c1.ToPolygon();
            var p2 = c2.ToPolygon();

            var result = p1.UnionWithClipper(p2).ToGeoCoordinates();

            var allc = new List<GeoCoordinate>();
            allc.AddRange(c1);
            allc.AddRange(c2);

            result.Count.ShouldEqual(2 * (cpc - apc) + 2);
            result.Count(x => !allc.Any(p => EntityHelpers.ApproximatelyEquals(x, p))).ShouldEqual(2);
        }

        [TestMethod]
        public void AdvancedPolygonSubtractionTest()
        {
            var dist = 1000;

            // BUD liszt ferenc airport
            var bud = new GeoCoordinate
            {
                Latitude = 47.438437,
                Longitude = 19.252274
            };

            // point 1km east of bud
            var budOff = bud.CalculateDestinationPoint(dist, 90);

            var a = new Accuracy(dist);

            var cpc = a.CircularPointCount();
            var apc = a.ArcPointCount(120); // triangle with 3 r = 1000 sides, 60° each, so 120° for the part that's diffed

            var c1 = new List<GeoCoordinate>();
            for (var i = 0; i < cpc; i++) c1.Add(bud.CalculateDestinationPoint(dist, i * a.AnglePerIteration()));

            var c2 = new List<GeoCoordinate>();
            for (var i = 0; i < cpc; i++) c2.Add(budOff.CalculateDestinationPoint(dist, i * a.AnglePerIteration()));

            var p1 = c1.ToPolygon();
            var p2 = c2.ToPolygon();

            var result = p1.SubtractWithClipper(p2).ToGeoCoordinates();

            var allc = new List<GeoCoordinate>();
            allc.AddRange(c1);
            allc.AddRange(c2);

            result.Count.ShouldEqual(cpc + 2); // omitted '- apc + apc' as it's 0
            result.Count(x => !allc.Any(p => EntityHelpers.ApproximatelyEquals(x, p))).ShouldEqual(2);
        }

        [TestMethod]
        public void AdvancedPolygonIntersectionTest()
        {
            var dist = 1000;

            // BUD liszt ferenc airport
            var bud = new GeoCoordinate
            {
                Latitude = 47.438437,
                Longitude = 19.252274
            };

            // point 1km east of bud
            var budOff = bud.CalculateDestinationPoint(dist, 90);

            var a = new Accuracy(dist);

            var cpc = a.CircularPointCount();
            var apc = a.ArcPointCount(120); // triangle with 3 r = 1000 sides, 60° each, so 120° for the part that's diffed

            var c1 = new List<GeoCoordinate>();
            for (var i = 0; i < cpc; i++) c1.Add(bud.CalculateDestinationPoint(dist, i * a.AnglePerIteration()));

            var c2 = new List<GeoCoordinate>();
            for (var i = 0; i < cpc; i++) c2.Add(budOff.CalculateDestinationPoint(dist, i * a.AnglePerIteration()));

            var p1 = c1.ToPolygon();
            var p2 = c2.ToPolygon();

            var result = p1.IntersectWithClipper(p2).ToGeoCoordinates();

            var allc = new List<GeoCoordinate>();
            allc.AddRange(c1);
            allc.AddRange(c2);

            result.Count.ShouldEqual(2*apc + 2);
            result.Count(x => !allc.Any(p => EntityHelpers.ApproximatelyEquals(x,p))).ShouldEqual(2);

            var s = result.Select(x => $"{x.Latitude}, {x.Longitude}").Aggregate((y, n) => y + Environment.NewLine + n);
            Debugger.Break();

            // TODO future improvement: correct coordinates created by polygon math
            // http://www.movable-type.co.uk/scripts/latlong.html - intersection of two paths given start and bearings

            /*
             * pseudocode:
             *
             * - for each calculated point -
             * enumerate original list to compared to result list
             * find the index of the new point in the original, let that be 'i'
             * calculate the bearing for original[i] and original[i+1], let that be 'ib'
             * do the same for the param poly, let the index be 'j' and the bearing be 'jb'
             * calculate intersection point with start point (original[i] & ib) end point (param[j] & jb)
             * replace new point with the precise intersection point
             * */
        }
    }
}
