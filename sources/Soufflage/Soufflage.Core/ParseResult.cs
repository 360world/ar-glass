﻿using System.Collections.Generic;
using Soufflage.Core.Entities;

namespace Soufflage.Core
{
    public class ParseResult
    {
        public bool Success { get;  }

        public List<Group> Groups { get; }

        public IEnumerable<LogEntry> LogEntries { get;  }

        public string Message { get;  }

        public ParseResult(List<Group> groups, IEnumerable<LogEntry> logEntries, string message)
        {
            Success = true;
            Groups = groups;
            LogEntries = logEntries;
            Message = message;
        }

        public ParseResult(IEnumerable<LogEntry> logEntries, string message)
        {
            Success = false;
            LogEntries = logEntries;
            Message = message;
        }
    }
}
