﻿using System.Collections.Generic;

namespace Soufflage.Core.Geography
{
    /// <summary>
    /// Datum types available
    /// </summary>
    public enum DatumType
    {
        WGS84,
        EPSG3857,
        GRS80,
        Airy1830,
        AiryModified,
        Intl1924,
        Bessel1841
    }

    /// <summary>
    /// Ellipsoid
    /// </summary>
    public class Ellipsoid
    {
        /// <summary>
        /// Major axis of the ellipsoid ('a')
        /// </summary>
        public double MajorAxis { get; set; }

        /// <summary>
        /// Minor axis of the ellipsoid ('b')
        /// </summary>
        public double MinorAxis { get; set; }

        /// <summary>
        /// Flattening the ellipsoid ('f')
        /// </summary>
        public double Flattening { get; set; }
    }

    /// <summary>
    /// Geodetic datum
    /// </summary>
    public class Datum
    {
        private readonly DatumType _type;

        public Datum(DatumType type)
        {
            _type = type;
        }

        public Ellipsoid Ellipsoid => DatumMap[_type];

        // transforms are not implemented!
        // more here: http://www.movable-type.co.uk/scripts/js/geodesy/latlon-ellipsoidal.js
        // and here: http://www.movable-type.co.uk/scripts/js/geodesy/latlon-vincenty.js

        private static readonly Dictionary<DatumType, Ellipsoid> DatumMap = new Dictionary<DatumType, Ellipsoid>
        {
            [DatumType.WGS84] = new Ellipsoid
            {
                MajorAxis = 6378137,
                MinorAxis = 6356752.31425,
                Flattening = 1/298.257223563
            },
            [DatumType.EPSG3857] = new Ellipsoid
            {
                MajorAxis = 6378137,
                MinorAxis = 6378137,
                Flattening = 1 / 298.257223563
            },
            [DatumType.GRS80] = new Ellipsoid
            {
                MajorAxis = 6378137,
                MinorAxis = 6356752.31414,
                Flattening = 1/298.257222101
            },
            [DatumType.Airy1830] = new Ellipsoid
            {
                MajorAxis = 6377563.396,
                MinorAxis = 6356256.909,
                Flattening = 1/299.3249646
            },
            [DatumType.AiryModified] = new Ellipsoid
            {
                MajorAxis = 6377340.189,
                MinorAxis = 6356034.448,
                Flattening = 1/299.3249646
            },
            [DatumType.Intl1924] = new Ellipsoid
            {
                MajorAxis = 6378388,
                MinorAxis = 6356911.946,
                Flattening = 1/297.0
            },
            [DatumType.Bessel1841] = new Ellipsoid
            {
                MajorAxis = 6377397.155,
                MinorAxis = 6356078.963,
                Flattening = 1/299.152815351
            },
        };
    }
}
