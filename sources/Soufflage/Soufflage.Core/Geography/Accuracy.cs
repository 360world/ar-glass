﻿using System;

namespace Soufflage.Core.Geography
{
    /// <summary>
    /// Accuracy class
    /// </summary>
    public class Accuracy
    {
        private readonly double _largestScaleInMeters;

        public Accuracy(double largestScaleInMeters)
        {
            _largestScaleInMeters = largestScaleInMeters;

            var rs1 = Convert.ToInt32(Math.Round(_largestScaleInMeters));
            var rs2 = rs1 - (rs1 % 100) + (rs1 %100 > 50 ? 0 : 100);
            var rthScale = rs2/Precision.AreaEpsilonBase;
            var radius = _largestScaleInMeters/2.0;

            var actualSize = Math.PI*radius*radius;
            var epsilon = rthScale*Precision.AreaEpsilon*2.0;

            // we start with 12 as a minimum
            var sideCount = 12;
            while (true)
            {
                var currentArea = sideCount*radius*radius*Math.Cos(Math.PI/sideCount)*Math.Sin(Math.PI/sideCount);
                if(Math.Abs(actualSize - currentArea) < epsilon) break;
                sideCount += 1;
            }
            _baselineCircularPrecision = sideCount;
        }

        private readonly int _baselineCircularPrecision;

        /// <summary>
        /// Angle per iteration for a circle/arc
        /// </summary>
        /// <returns>angle per iteration</returns>
        public double AnglePerIteration() => 360.0 / _baselineCircularPrecision;

        /// <summary>
        /// Points to use for a circular area polygon
        /// </summary>
        /// <returns>point count</returns>
        public int CircularPointCount() => _baselineCircularPrecision;

        /// <summary>
        /// Points to use for an arc polyline
        /// </summary>
        /// <param name="angle">angle of arc</param>
        /// <returns>point count</returns>
        public int ArcPointCount(double angle) => Math.Max(Convert.ToInt32(Math.Floor(_baselineCircularPrecision * (angle / 360.0))), 1);
    }
}
