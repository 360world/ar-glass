﻿using System;
using Soufflage.Core.Entities;

namespace Soufflage.Core.Geography
{
    public static class SimpleSpatialTransforms
    {
        private static readonly Datum Datum = new Datum(DatumType.EPSG3857);

        // methodology from below
        // https://alastaira.wordpress.com/2011/01/23/the-google-maps-bing-maps-spherical-mercator-projection/

        // testing EPSG3857
        // https://epsg.io/transform#s_srs=3857&t_srs=4326&x=1916112.1383870&y=6135193.5181356

        public static Coordinate ToCoordinate(this GeoCoordinate coord, Datum customDatum = null)
        {
            var ellipsoid = customDatum?.Ellipsoid ?? Datum.Ellipsoid;
            var x = coord.Longitude*(ellipsoid.MajorAxis*Math.PI)/180;
            var y = Math.Log(Math.Tan((90 + coord.Latitude)*Math.PI/360))/(Math.PI/180);
            y = y*(ellipsoid.MinorAxis*Math.PI)/180;
            return new Coordinate(x, y);
        }

        public static GeoCoordinate ToGeoCoordinate(this Coordinate coord, Datum customDatum = null)
        {
            var ellipsoid = customDatum?.Ellipsoid ?? Datum.Ellipsoid;
            var lon = (coord.X/(ellipsoid.MajorAxis*Math.PI))*180;
            var lat = (coord.Y/(ellipsoid.MinorAxis*Math.PI))*180;
            lat = 180/Math.PI*(2*Math.Atan(Math.Exp(lat*Math.PI/180)) - Math.PI/2);
            return new GeoCoordinate(lat, lon);
        }
    }
}
