﻿using System;

namespace Soufflage.Core
{
    public static class MathHelpers
    {
        public static double ToRadians(this double d) => d * Math.PI / 180.0;

        public static double ToDegrees(this double r) => r * 180.0 / Math.PI;
    }
}
