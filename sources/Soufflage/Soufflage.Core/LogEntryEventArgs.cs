﻿using System;

namespace Soufflage.Core
{
    public class LogEntryEventArgs : EventArgs
    {
        public LogEntry Entry { get; set; }
    }
}
