﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using Soufflage.Core.Entities;

namespace Soufflage.Core.Clipper
{
    public static class ClipperProxyHelpers
    {
        private static readonly double SignificantDigits = Math.Pow(10.0, 9);

        public static IntPoint ToIntPoint(this GeoCoordinate g)
            => new IntPoint(Convert.ToInt64(Math.Truncate(g.Latitude*SignificantDigits)),
                Convert.ToInt64(Math.Truncate(g.Longitude*SignificantDigits)));

        public static GeoCoordinate ToGeoCoordinate(this IntPoint p) => new GeoCoordinate
        {
            Latitude = p.X/SignificantDigits,
            Longitude = p.Y/SignificantDigits
        };

        public static List<IntPoint> ToPolygon(this IEnumerable<GeoCoordinate> g) => g.Select(x => x.ToIntPoint()).ToList();

        public static List<GeoCoordinate> ToGeoCoordinates(this IEnumerable<IntPoint> p) => p.Select(x => x.ToGeoCoordinate()).ToList();

        public static List<IntPoint> UnionWithClipper(this List<IntPoint> basis, List<IntPoint> param) => ExecuteOperation(ClipType.ctUnion, basis, param);

        public static List<IntPoint> IntersectWithClipper(this List<IntPoint> basis, List<IntPoint> param) => ExecuteOperation(ClipType.ctIntersection, basis, param);

        public static List<IntPoint> SubtractWithClipper(this List<IntPoint> basis, List<IntPoint> param) => ExecuteOperation(ClipType.ctDifference, basis, param);

        private static List<IntPoint> ExecuteOperation(ClipType type, List<IntPoint> basis, List<IntPoint> param)
        {
            var c = new ClipperLib.Clipper();
//#if DEBUG
//            c.OnIntersectionFound += (sender, args) =>
//            {
//                Debugger.Break();
//            };
//#endif
            c.AddPath(basis, PolyType.ptSubject, true);
            c.AddPath(param, PolyType.ptClip, true);
            var solution = new List<List<IntPoint>>();
            c.Execute(type, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);
            if (solution.Count <= 1) return solution[0];
            var msg = "";
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (type)
            {
                case ClipType.ctIntersection:
                    msg = "Intersection";
                    break;
                case ClipType.ctUnion:
                    msg = "Union";
                    break;
                case ClipType.ctDifference:
                    msg = "Subtraction";
                    break;
                case ClipType.ctXor:
                    msg = "Exclusive or";
                    break;
            }
            throw new GeometryException($"{msg} operation should result in a single polygon.");
        }
    }
}
