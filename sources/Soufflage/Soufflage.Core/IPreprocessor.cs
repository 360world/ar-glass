﻿namespace Soufflage.Core
{
    public interface IPreprocessor
    {
        PreprocessingResult Extract(string source);
    }
}
