﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soufflage.Core
{
    public static class GlobalParserHelpers
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public static readonly CultureInfo Culture = new CultureInfo("en-us");

        public static double AsDouble(this string s, double defVal = 0.0)
        {
            double r;
            return
                double.TryParse(s, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign | NumberStyles.AllowLeadingWhite | NumberStyles.AllowExponent, Culture, out r)
                ? r
                : defVal;
        }

        public static double NormalizeDegreeInRange0To360(this double deg)
        {
            var s1 = deg < 0 ? Math.Ceiling(Math.Abs(deg) / 360.0) + deg : deg;
            while (s1 > 360.0) s1 -= 360.0;
            return s1;
        }
    }
}
