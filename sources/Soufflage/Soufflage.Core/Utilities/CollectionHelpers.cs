﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soufflage.Core.Utilities
{
    public static class CollectionHelpers
    {
        public static void AddRange<T>(this ObservableCollection<T> oc, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                oc.Add(item);
            }
        }
    }
}
