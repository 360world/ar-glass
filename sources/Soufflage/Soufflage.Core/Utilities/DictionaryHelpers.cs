﻿using System.Collections.Generic;

namespace Soufflage.Core.Utilities
{
    public static class DictionaryHelpers
    {
        public static TValue FailSafeWithValue<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue defaultValue)
        {
            TValue val;
            return dict.TryGetValue(key, out val) ? val : defaultValue;
        }

        public static TValue FailSafeWithKey<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TKey defaultKey)
        {
            TValue val;
            return dict.TryGetValue(key, out val) ? val : dict[defaultKey];
        }
    }
}
