﻿using System;
using ClipperLib;
using Soufflage.Core.Entities;

namespace Soufflage.Core.Utilities
{
    public static class MathHelpers
    {
        public static bool TryGetIntersection(GeoCoordinate a, GeoCoordinate b, GeoCoordinate c, GeoCoordinate d,
            out GeoCoordinate intersection)
        {
            intersection = new GeoCoordinate();
            double ix, iy;
            if (TryGetIntersection(a.Latitude, a.Longitude, b.Latitude, b.Longitude, c.Latitude, c.Longitude, d.Latitude, d.Longitude, out ix, out iy)) return false;
            intersection = new GeoCoordinate {Latitude = ix, Longitude = iy};
            return true;
        }

        public static bool TryGetIntersection(IntPoint a, IntPoint b, IntPoint c, IntPoint d, out int intersectionX, out int intersectionY)
        {
            intersectionX = 0;
            intersectionY = 0;
            double ix, iy;
            if (!TryGetIntersection(a.X, a.Y, b.X, b.Y, c.X, c.Y, d.X, d.Y, out ix, out iy)) return false;
            intersectionX = Convert.ToInt32(Math.Truncate(ix));
            intersectionY = Convert.ToInt32(Math.Truncate(iy));
            return true;
        }

        public static bool TryGetIntersection(double ax, double ay, double bx, double @by, double cx, double cy, double dx, double dy, out double intersctionX, out double intersectionY)
        {
            intersctionX = 0;
            intersectionY = 0;

            var baX = bx - ax;
            var baY = @by - ay;
            var dcX = dx - cx;
            var dcY = dy - cy;

            var denom = baX * dcY - dcX * baY;
            if (Math.Abs(denom) < 0.000005) return false; // Collinear
            var denomPositive = denom > 0;

            var acX = ax - cx;
            var acY = ay - cy;
            var scalarA = baX * acY - baY * acX;
            if ((scalarA < 0) == denomPositive) return false; // No collision

            var scalarT = dcX * acY - dcY * acX;
            if ((scalarT < 0) == denomPositive) return false; // No collision

            if (((scalarA > denom) == denomPositive) || ((scalarT > denom) == denomPositive)) return false; // No collision

            // Collision detected
            var t = scalarT / denom;
            intersctionX = ax + (t * baX);
            intersectionY = ay + (t * baY);

            return true;
        }
    }
}
