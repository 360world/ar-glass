﻿using System;

namespace Soufflage.Core.Utilities
{
    public static class TypeHelpers
    {
        //TODO rewrite this in C#7 when it comes out using a proper type switch   \\\\\

        public static void Switch<TCase1, TCase2>(this object o,
            Action<TCase1> case1,
            Action<TCase2> case2,
            Action<object> caseDefault = null)
            where TCase1 : class
            where TCase2 : class
        {
            var c1 = o as TCase1;
            if (c1 != null) case1.Invoke(c1);
            var c2 = o as TCase2;
            if (c2 != null) case2.Invoke(c2);
            if(caseDefault != null && c1 == null && c2 == null) caseDefault.Invoke(o);
        }

        public static void Switch<TCase1, TCase2, TCase3>(this object o,
            Action<TCase1> case1,
            Action<TCase2> case2,
            Action<TCase3> case3,
            Action<object> caseDefault = null)
            where TCase1 : class
            where TCase2 : class
            where TCase3 : class
        {
            var c1 = o as TCase1;
            if (c1 != null) case1.Invoke(c1);
            var c2 = o as TCase2;
            if (c2 != null) case2.Invoke(c2);
            var c3 = o as TCase3;
            if (c3 != null) case3.Invoke(c3);
            if (caseDefault != null && c1 == null && c2 == null && c3 == null) caseDefault.Invoke(o);
        }

        public static void Switch<TCase1, TCase2, TCase3, TCase4>(this object o,
            Action<TCase1> case1,
            Action<TCase2> case2,
            Action<TCase3> case3,
            Action<TCase4> case4,
            Action<object> caseDefault = null)
            where TCase1 : class
            where TCase2 : class
            where TCase3 : class
            where TCase4 : class
        {
            var c1 = o as TCase1;
            if (c1 != null) case1.Invoke(c1);
            var c2 = o as TCase2;
            if (c2 != null) case2.Invoke(c2);
            var c3 = o as TCase3;
            if (c3 != null) case3.Invoke(c3);
            var c4 = o as TCase4;
            if (c4 != null) case4.Invoke(c4);
            if (caseDefault != null && c1 == null && c2 == null && c3 == null && c4 == null) caseDefault.Invoke(o);
        }

        //TODO rewrite this in C#7 when it comes out using a proper type switch   /////
    }
}
