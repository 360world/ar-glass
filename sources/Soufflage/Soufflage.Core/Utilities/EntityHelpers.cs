﻿using System;
using Soufflage.Core.Entities;
using Soufflage.Core.Geography;

namespace Soufflage.Core.Utilities
{
    public static class EntityHelpers
    {
        public static bool ApproximatelyEquals(GeoCoordinate a, GeoCoordinate b)
        {
            return Math.Abs(a.Latitude - b.Latitude) < Precision.CoordinateEpsilon &&
                   Math.Abs(a.Longitude - b.Longitude) < Precision.CoordinateEpsilon;
        }

        public static string Failsafe(this string s, string rep) => string.IsNullOrWhiteSpace(s) ? rep : s;

        public static Coordinate SwitchAxis(this Coordinate coord)
        {
            var tmp = coord.X;
            coord.X = coord.Y;
            coord.Y = tmp;
            return coord;
        }

        public static GeoCoordinate SwitchAxis(this GeoCoordinate coord)
        {
            var tmp = coord.Latitude;
            coord.Latitude = coord.Longitude;
            coord.Longitude = tmp;
            return coord;
        }
    }
}
