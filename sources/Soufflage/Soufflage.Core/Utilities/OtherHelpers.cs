﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soufflage.Core.Utilities
{
    public static class OtherHelpers
    {
        public static string CreateLongId() => Guid.NewGuid().ToString().Replace("-","");
    }
}
