﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Soufflage.Core
{
    public interface IParser
    {
        IEnumerable<string> SupportedFormats { get; }

        ParseResult Parse(IEnumerable<Stream> source);

        event EventHandler<LogEntryEventArgs> OnLogEntry;
    }
}
