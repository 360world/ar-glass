﻿namespace Soufflage.Core
{

    public enum EntryType
    {
        Info,
        Warning,
        Error
    }

    public class LogEntry
    {
        public EntryType Type { get; }

        public string Category { get; }

        public string Msg { get; }

        public LogEntry(EntryType type, string msg, string category = "general")
        {
            Type = type;
            Msg = msg;
            Category = category;
        }
    }
}
