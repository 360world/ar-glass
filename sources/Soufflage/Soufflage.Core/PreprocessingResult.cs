﻿using System.Collections.Generic;

namespace Soufflage.Core
{
    public class PreprocessingResult
    {
        public string Result { get; }

        public IDictionary<int, string> Extractions { get; }

        public PreprocessingResult(string result, IDictionary<int, string> extractions)
        {
            Result = result;
            Extractions = extractions;
        }
    }
}
