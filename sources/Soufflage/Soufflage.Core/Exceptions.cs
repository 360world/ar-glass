﻿using System;

namespace Soufflage.Core
{
    public class GeometryException : Exception
    {
        public GeometryException(string msg) : base(msg)
        {
        }

        public GeometryException(string msg, Exception inner) : base(msg, inner)
        {
        }
    }

    public class GeographyException : Exception
    {
        public GeographyException(string msg) : base(msg)
        {
        }

        public GeographyException(string msg, Exception inner) : base(msg, inner)
        {
        }
    }

    public class AixmException : Exception
    {
        public AixmException(string msg) : base(msg)
        {
        }

        public AixmException(string msg, Exception inner) : base(msg, inner)
        {
        }
    }
}
