﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace SourceTool
{
    public class Program
    {
        static void Main(string[] args)
        {
            var file = args.Any() ? args[0] : "D:\\CurrentMockings\\aixm2\\OBST.xml";

            var xml = new XmlDocument();
            xml.Load(file);

            var dict = new Dictionary<string, int>();

            foreach (XmlElement childNode in xml.ChildNodes[2].ChildNodes)
            {
                var n = childNode.ChildNodes[0].Name;
                if (!dict.ContainsKey(n))
                {
                    dict.Add(n, 0);
                }
                dict[n] += 1;
            }

            foreach (var i in dict)
            {
                Console.WriteLine($"{i.Value:D3} {i.Key}");
            }

            Console.ReadKey();
        }
    }
}
