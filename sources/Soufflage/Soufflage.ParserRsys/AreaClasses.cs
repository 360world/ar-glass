﻿using System.Collections.Generic;

namespace Soufflage.ParserRsys
{
    public class Rectangle
    {
        public string x1 { get; set; }
        public string y1 { get; set; }
        public string x2 { get; set; }
        public string y2 { get; set; }
    }

    public class Area
    {
        public string code_id { get; set; }
        public string txt_name { get; set; }
        public List<Rectangle> rectangle { get; set; }
        public string type { get; set; }
        public string geometry { get; set; }
    }

    public class Point
    {
        public string code_id { get; set; }
        public string txt_name { get; set; }
        public string type { get; set; }
        public string geometry { get; set; }
        public string distance { get; set; }
        public string elevation { get; set; }
        public string freq { get; set; }
    }

    public class AreasRoot
    {
        public List<Area> areas { get; set; }
        public List<Point> points { get; set; }
    }
}
