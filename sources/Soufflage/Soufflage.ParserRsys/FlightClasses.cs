﻿using System.Collections.Generic;

namespace Soufflage.ParserRsys
{
    public class Adep
    {
        public string name { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string ss { get; set; }
        public string sr { get; set; }
        public string freq { get; set; }
    }

    public class Ades
    {
        public string name { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string ss { get; set; }
        public string sr { get; set; }
        public string freq { get; set; }
    }

    public class FromPoint
    {
        public string name { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string freq { get; set; }
    }

    public class ToPoint
    {
        public string name { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string freq { get; set; }
    }

    public class Segment
    {
        public string sequence { get; set; }
        public FromPoint fromPoint { get; set; }
        public ToPoint toPoint { get; set; }
        public string height { get; set; }
        public string heightUnit { get; set; }
        public string ias { get; set; }
        public string iasUnit { get; set; }
        public string hdg { get; set; }
        public int distance { get; set; }
        public string segmentTime { get; set; }
        public string eta { get; set; }
        public string ata { get; set; }
    }

    public class Route
    {
        public string name { get; set; }
        public string horizontalBuffer { get; set; }
        public string horizontalBufferUnit { get; set; }
        public string verticalBuffer { get; set; }
        public string verticalBufferUnit { get; set; }
        public List<Segment> segment { get; set; }
        public string distance { get; set; }
        public string routeTime { get; set; }
        public string timeSegment { get; set; }
        public string timeSegmentCount { get; set; }
        public string lastTimeSegment { get; set; }
    }

    public class Alter1
    {
        public string name { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string freq { get; set; }
    }

    public class Alter2
    {
        public string name { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string freq { get; set; }
    }

    public class Aircraft
    {
        public string id { get; set; }
        public string userId { get; set; }
        public string registration { get; set; }
        public string type { get; set; }
        public string colourMark { get; set; }
        public string fuelTaxi { get; set; }
        public string fuelTaxiUnit { get; set; }
        public string fuelLanding { get; set; }
        public string fuelLandingUnit { get; set; }
        public string holdingTime { get; set; }
        public string holdingTimeUnit { get; set; }
        public string fuelTotal { get; set; }
        public string fuelTotalUnit { get; set; }
        public string fuelContingency { get; set; }
        public string fuelContingencyUnit { get; set; }
        public string turbulency { get; set; }
        public string climbRate { get; set; }
        public string climbRateUnit { get; set; }
        public string climbRateCeiling { get; set; }
        public string climbRateCeilingUnit { get; set; }
        public string iasClimb { get; set; }
        public string iasClimbUnit { get; set; }
        public string fuelBurn { get; set; }
        public string fuelBurnUnit { get; set; }
        public string fuelBurnClimb { get; set; }
        public string fuelBurnClimbUnit { get; set; }
        public string ias { get; set; }
        public string iasUnit { get; set; }
        public string fuelBurnCeiling { get; set; }
        public string fuelBurnCeilingUnit { get; set; }
        public string ceilingFl { get; set; }
        public string ceilingFlUnit { get; set; }
        public string descentRate { get; set; }
        public string descentRateUnit { get; set; }
        public string iasDesc { get; set; }
        public string iasDescUnit { get; set; }
        public string fuelBurnDesc { get; set; }
        public string fuelBurnDescUnit { get; set; }
        public string emergencyRadio { get; set; }
        public string survivalEquipment { get; set; }
        public string lifeJacket { get; set; }
        public string dinghiesNumber { get; set; }
        public string dinghiesCapacity { get; set; }
        public string dinghiesCovered { get; set; }
        public string dinghiesColour { get; set; }
        public string otherComm { get; set; }
        public string otherNav { get; set; }
        public string comNav { get; set; }
        public string comNavEquipment { get; set; }
    }

    public class FlightRoot
    {
        public string userId { get; set; }
        public string PIC { get; set; }
        public string crew { get; set; }
        public string POB { get; set; }
        public Adep adep { get; set; }
        public Ades ades { get; set; }
        public string dof { get; set; }
        public string rule { get; set; }
        public string type { get; set; }
        public Route route { get; set; }
        public string aircraftId { get; set; }
        public Alter1 alter1 { get; set; }
        public Alter2 alter2 { get; set; }
        public string etd { get; set; }
        public string eat { get; set; }
        public string eet { get; set; }
        public string ias { get; set; }
        public string iasUnit { get; set; }
        public string xflag { get; set; }
        public string fuel { get; set; }
        public string fuelUnit { get; set; }
        public string consumption { get; set; }
        public string consumptionUnit { get; set; }
        public string flevel { get; set; }
        public string flevelUnit { get; set; }
        public string endurance { get; set; }
        public string qnh { get; set; }
        public string range { get; set; }
        public Aircraft Aircraft { get; set; }
    }
}
