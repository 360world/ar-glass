﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Soufflage.Core;
using Soufflage.Core.Entities;
using Soufflage.Core.Utilities;

namespace Soufflage.ParserRsys
{
    public static class ParserHelpers
    {
        private static readonly Dictionary<string, Func<double, double>> UnitConverters = new Dictionary<string, Func<double, double>>
        {
            /* default */

            ["DEFAULT"] = (x) => x,

            /* distance to meters */

            // deka meter
            ["M"] = (x) => 10 * x,
            // VFR
            ["VFR"] = (x) => 1000.0 * 0.3048,
            // flight level
            ["F"] = (x) => x * 0.3048 * 100,
            ["A"] = (x) => x * 0.3048 * 100,
            // meter
            ["S"] = (x) => x,
        };

        #region ActualUnitConverters

        public static double ToMeters(this string r, string unit) => UnitConverters.FailSafeWithKey(unit, "DEFAULT").Invoke(r.AsDouble());

        #endregion ActualUnitConverters

        #region CoordinateConverters

        public static Coordinate ToCoordinate(this FromPoint p) => new Coordinate(p.lat.AsDouble(), p.lng.AsDouble());

        public static Coordinate ToCoordinate(this ToPoint p) => new Coordinate(p.lat.AsDouble(), p.lng.AsDouble());

        private static IEnumerable<GeoCoordinate> ParseGeometry(string geometry)
        {
            if (geometry.StartsWith("POLYGON"))
            {
                var cleaned = geometry.Substring(10).TrimEnd(')');
                var coords = cleaned.Split(',').Select(x => x.Trim());
                return coords.Select(coord => coord.Split(' ')).Select(cs => new GeoCoordinate(cs[0].AsDouble(), cs[1].AsDouble())).ToList();
            }
            // ReSharper disable once InvertIf
            if (geometry.StartsWith("POINT"))
            {
                var cleaned = geometry.Substring(7).TrimEnd(')');
                var cs = cleaned.Split(' ');
                return new List<GeoCoordinate>
                {
                    new GeoCoordinate(cs[0].AsDouble(), cs[1].AsDouble())
                };
            }
            return new List<GeoCoordinate>();
        }

        public static IEnumerable<GeoCoordinate> ToGeoCoordinates(this Area a)
        {
            var gcs = ParseGeometry(a.geometry).ToList();
            if(gcs.Count() < 3) throw new GeometryException("An area needs at least 3 points.");
            return gcs;
        }

        public static GeoCoordinate ToGeoCoordinate(this Point p)
        {
            var gcs = ParseGeometry(p.geometry).ToList();
            if (gcs.Count() != 1) throw new GeometryException("A point has to have a geometry with exactly one coordinate it.");
            return gcs[0];
        }

        #endregion CoordinateConverters

        #region LimitConverters

        private static readonly Dictionary<string, string> CodeVerticalReferenceTypes = new Dictionary<string, string>
        {
            ["OTHER"] = "OTHER - Other.",
            ["F"] = "F - Flight level above sea level.",
            ["A"] = "A - Flight level above medium sea level.",
            ["S"] = "S - Meters above sea level.",
            ["M"] = "M - Ten meters above sea level.",
            ["VFR"] = "VFR - 1000 ft above terrain.",
        };

        public static Height ToGroundHeightLimits(this FlightRoot fr)
        {
            return new Height
            {
                UpperLimitReference = CodeVerticalReferenceTypes.FailSafeWithKey(fr.flevelUnit, "OTHER"),
                UpperLimit = 0.0
            };
        }

        public static Height ToHeightLimits(this Segment s, FlightRoot fr)
        {
            // ReSharper disable RedundantIfElseBlock
            if (string.IsNullOrWhiteSpace(s.height))
            {
                if (s.heightUnit != "VFR")
                {
                    return new Height
                    {
                        UpperLimitReference = CodeVerticalReferenceTypes.FailSafeWithKey(fr.flevelUnit, "OTHER"),
                        UpperLimit = ToMeters(fr.flevel, fr.flevelUnit)
                    };
                }
                else
                {
                    return new Height
                    {
                        UpperLimitReference = CodeVerticalReferenceTypes.FailSafeWithKey(s.heightUnit, "OTHER"),
                        UpperLimit = ToMeters("0.0", s.heightUnit)
                    };
                }
            }
            else
            {
                return new Height
                {
                    UpperLimitReference = CodeVerticalReferenceTypes.FailSafeWithKey(s.heightUnit, "OTHER"),
                    UpperLimit = ToMeters(s.height, s.heightUnit)
                };
            }
            // ReSharper restore RedundantIfElseBlock
        }

        public static Height ToHeightLimits(this Point p)
        {
            if (string.IsNullOrWhiteSpace(p.elevation)) return null;
            return new Height
            {
                UpperLimit = p.elevation.AsDouble(),
                UpperLimitReference = CodeVerticalReferenceTypes.FailSafeWithKey("OTHER","OTHER")
            };
        }

        #endregion LimitConverters
    }
}
