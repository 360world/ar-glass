﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Soufflage.Core;
using Soufflage.Core.Entities;
using Soufflage.Core.Geography;
using Soufflage.Core.Utilities;

namespace Soufflage.ParserRsys
{
    public class FlightParser : IParser
    {
        public IEnumerable<string> SupportedFormats => new List<string> {"r-sys flight json"};

        public ParseResult Parse(IEnumerable<Stream> source)
        {
            var log = new ObservableCollection<LogEntry>();
            log.CollectionChanged += Log_CollectionChanged;

            var groups = new List<Group>();

            try
            {
                var s = new JsonSerializer();
                foreach (var stream in source)
                {
                    log.Add(new LogEntry(EntryType.Info, "opening new source", "file"));
                    using (var sr = new StreamReader(stream))
                    {
                        using (var jtr = new JsonTextReader(sr))
                        {
                            var jc = s.Deserialize<FlightRoot>(jtr);

                            log.Add(new LogEntry(EntryType.Info, "successfully parsed r-sys flights json file"));

                            /* ######################## route ######################## */

                            var routeGroup = new Group
                            {
                                Description = $"Route for {jc.route.name}",
                                GroupFor = GroupFor.Route,
                                Identifier = jc.route.name,
                                FriendlyName = $"Flight from {jc.adep.name} to {jc.ades.name}"
                            };

                            log.Add(new LogEntry(EntryType.Info, $"found {jc.route.name}", "route"));

                            var routePoly = new Polyline();
                            var l = new List<Core.Entities.Point>();

                            // first two points from the first segment

                            var p1 = new Core.Entities.Point
                            {
                                Location = jc.route.segment[0].fromPoint.ToCoordinate().SwitchAxis().ToGeoCoordinate(),
                                ShapeIdentifier = jc.route.segment[0].fromPoint.name,
                                ShapeFriendlyName = $"Checkpoint {jc.route.segment[0].fromPoint.name}",
                                Height = jc.ToGroundHeightLimits()
                            };
                            l.Add(p1);

                            var p2 = new Core.Entities.Point
                            {
                                Location = jc.route.segment[0].toPoint.ToCoordinate().SwitchAxis().ToGeoCoordinate(),
                                ShapeIdentifier = jc.route.segment[0].toPoint.name,
                                ShapeFriendlyName = $"Checkpoint {jc.route.segment[0].toPoint.name}",
                                Height = jc.route.segment[0].ToHeightLimits(jc)
                            };
                            l.Add(p2);

                            // other points from the other segments

                            l.AddRange(jc.route.segment.Skip(1).Select(segment => new Core.Entities.Point
                            {
                                Location = segment.toPoint.ToCoordinate().SwitchAxis().ToGeoCoordinate(),
                                ShapeIdentifier = segment.toPoint.name,
                                ShapeFriendlyName = $"Checkpoint {segment.toPoint.name}",
                                Height = segment.ToHeightLimits(jc)
                            }));

                            // reset last height
                            l.Last().Height = jc.ToGroundHeightLimits();

                            // finish up
                            routePoly.Locations = l;
                            routeGroup.Shapes.Add(routePoly);
                            groups.Add(routeGroup);

                            log.Add(new LogEntry(EntryType.Info, $"{jc.route.name} built", "route"));
                        }
                    }
                    log.Add(new LogEntry(EntryType.Info, "finished source", "file"));
                }
            }
            catch (Exception e)
            {
                log.Add(new LogEntry(EntryType.Error, e.ToString()));
                return new ParseResult(log, "Unknown error.");
            }

            return new ParseResult(groups, log, null);
        }

        private void Log_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnLogEntry?.Invoke(this, new LogEntryEventArgs
            {
                Entry = (LogEntry)e.NewItems[0]
            });
        }

        public event EventHandler<LogEntryEventArgs> OnLogEntry;
    }
}
