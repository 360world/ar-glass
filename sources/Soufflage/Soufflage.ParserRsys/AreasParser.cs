﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Soufflage.Core;
using Soufflage.Core.Entities;
using Soufflage.Core.Utilities;

namespace Soufflage.ParserRsys
{
    public class AreasParser : IParser
    {
        public IEnumerable<string> SupportedFormats => new List<string> { "r-sys areas json" };

        public ParseResult Parse(IEnumerable<Stream> source)
        {
            var log = new ObservableCollection<LogEntry>();
            log.CollectionChanged += Log_CollectionChanged;

            var groups = new List<Group>();

            try
            {
                var s = new JsonSerializer();
                foreach (var stream in source)
                {
                    log.Add(new LogEntry(EntryType.Info, "opening new source", "file"));
                    using (var sr = new StreamReader(stream))
                    {
                        using (var jtr = new JsonTextReader(sr))
                        {
                            var jc = s.Deserialize<AreasRoot>(jtr);

                            log.Add(new LogEntry(EntryType.Info, "successfully parsed r-sys areas json file"));

                            /* ######################## areas ######################## */

                            var areaGroups = jc.areas.Select(jcArea => new Group
                            {
                                LocalType = jcArea.type,
                                FriendlyName = $"{jcArea.code_id} - {jcArea.txt_name}",
                                Identifier = jcArea.code_id,
                                GroupFor = GroupFor.Airspace,
                                Shapes = new List<Shape>
                                {
                                    new StaticPolygon
                                    {
                                        ExteriorBoundary =
                                            jcArea.ToGeoCoordinates()
                                                .Select(x => new Core.Entities.Point {Location = x.SwitchAxis()})
                                                .ToList(),
                                        UseGlobalHeightOverride = true,
                                        GlobalHeightOverride = new Height()
                                    }
                                }
                            }).ToList();
                            log.AddRange(areaGroups.Select(x => new LogEntry(EntryType.Info, $"found and built {x.Identifier}", "airspace")));
                            groups.AddRange(areaGroups);

                            /* ######################## points ######################## */

                            var pointGroups = jc.points.Select(jcPoint => new Group
                            {
                                LocalType = jcPoint.type,
                                FriendlyName = $"{jcPoint.code_id.Failsafe("n/a")} - {jcPoint.txt_name}",
                                Identifier = jcPoint.code_id.Failsafe(OtherHelpers.CreateLongId()),
                                GroupFor =
                                    string.IsNullOrWhiteSpace(jcPoint.elevation)
                                        ? GroupFor.PointOfInterest
                                        : GroupFor.GroundObstacle,
                                Shapes = new List<Shape>
                                {
                                    new Core.Entities.Point
                                    {
                                        Location = jcPoint.ToGeoCoordinate().SwitchAxis(),
                                        Height = jcPoint.ToHeightLimits()
                                    }
                                }
                            }).ToList();
                            log.AddRange(pointGroups.Select(x => new LogEntry(EntryType.Info, $"found and built {x.Identifier}",
                                                                x.GroupFor == GroupFor.GroundObstacle ? "groundobstacle" : "pointofinterest")));
                            groups.AddRange(pointGroups);
                        }
                    }
                    log.Add(new LogEntry(EntryType.Info, "finished source", "file"));
                }
            }
            catch (Exception e)
            {
                log.Add(new LogEntry(EntryType.Error, e.ToString()));
                return new ParseResult(log, "Unknown error.");
            }

            return new ParseResult(groups, log, null);
        }

        private void Log_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnLogEntry?.Invoke(this, new LogEntryEventArgs
            {
                Entry = (LogEntry)e.NewItems[0]
            });
        }

        public event EventHandler<LogEntryEventArgs> OnLogEntry;
    }
}
