using System.Xml.Serialization;
using System.Collections.Generic;

namespace Xml2CSharp
{
	[XmlRoot(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
	public class Identifier {
		[XmlAttribute(AttributeName="codeSpace")]
		public string CodeSpace { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="endPosition", Namespace="http://www.opengis.net/gml/3.2")]
	public class EndPosition {
		[XmlAttribute(AttributeName="indeterminatePosition")]
		public string IndeterminatePosition { get; set; }
	}

    [XmlRoot(ElementName = "horizontalAccuracy", Namespace = "http://www.aixm.aero/schema/5.1")]
    public class HorizontalAccuracy
    {
        [XmlAttribute(AttributeName = "uom")]
        public string Uom { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "verticalAccuracy", Namespace = "http://www.aixm.aero/schema/5.1")]
    public class VerticalAccuracy
    {
        [XmlAttribute(AttributeName = "uom")]
        public string Uom { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName="TimePeriod", Namespace="http://www.opengis.net/gml/3.2")]
	public class TimePeriod {
		[XmlElement(ElementName="beginPosition", Namespace="http://www.opengis.net/gml/3.2")]
		public string BeginPosition { get; set; }
		[XmlElement(ElementName="endPosition", Namespace="http://www.opengis.net/gml/3.2")]
		public EndPosition EndPosition { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
	public class ValidTime {
		[XmlElement(ElementName="TimePeriod", Namespace="http://www.opengis.net/gml/3.2")]
		public TimePeriod TimePeriod { get; set; }
	}

	[XmlRoot(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
	public class FeatureLifetime {
		[XmlElement(ElementName="TimePeriod", Namespace="http://www.opengis.net/gml/3.2")]
		public TimePeriod TimePeriod { get; set; }
	}

	[XmlRoot(ElementName="AeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
	public class AeroDBPk {
		[XmlElement(ElementName="AeroDBFeaturePk", Namespace="http://www.idscompany/schema/5.1")]
		public string AeroDBFeaturePk { get; set; }
		[XmlElement(ElementName="AeroDBOccurrencyPk", Namespace="http://www.idscompany/schema/5.1")]
		public string AeroDBOccurrencyPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
	public class TheAeroDBPk {
		[XmlElement(ElementName="AeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public AeroDBPk AeroDBPk { get; set; }
	}

	[XmlRoot(ElementName="OrganisationAuthorityExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class OrganisationAuthorityExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Extension {
        [XmlElement(ElementName = "VerticalStructurePartExtension", Namespace = "http://www.idscompany/schema/5.1")]
        public VerticalStructurePartExtension VerticalStructurePartExtension { get; set; }
        [XmlElement(ElementName = "VerticalStructureExtension", Namespace = "http://www.idscompany/schema/5.1")]
        public VerticalStructureExtension VerticalStructureExtension { get; set; }
        [XmlElement(ElementName="OrganisationAuthorityExtension", Namespace="http://www.idscompany/schema/5.1")]
		public OrganisationAuthorityExtension OrganisationAuthorityExtension { get; set; }
		[XmlElement(ElementName="UnitExtension", Namespace="http://www.idscompany/schema/5.1")]
		public UnitExtension UnitExtension { get; set; }
		[XmlElement(ElementName="UnitDependencyExtension", Namespace="http://www.idscompany/schema/5.1")]
		public UnitDependencyExtension UnitDependencyExtension { get; set; }
		[XmlElement(ElementName="AirportHeliportExtension", Namespace="http://www.idscompany/schema/5.1")]
		public AirportHeliportExtension AirportHeliportExtension { get; set; }
		[XmlElement(ElementName="ServiceExtension", Namespace="http://www.idscompany/schema/5.1")]
		public ServiceExtension ServiceExtension { get; set; }
		[XmlElement(ElementName="TaxiwayExtension", Namespace="http://www.idscompany/schema/5.1")]
		public TaxiwayExtension TaxiwayExtension { get; set; }
		[XmlElement(ElementName="TaxiwayLightSystemExtension", Namespace="http://www.idscompany/schema/5.1")]
		public TaxiwayLightSystemExtension TaxiwayLightSystemExtension { get; set; }
		[XmlElement(ElementName="ApronExtension", Namespace="http://www.idscompany/schema/5.1")]
		public ApronExtension ApronExtension { get; set; }
		[XmlElement(ElementName="ApronLightSystemExtension", Namespace="http://www.idscompany/schema/5.1")]
		public ApronLightSystemExtension ApronLightSystemExtension { get; set; }
		[XmlElement(ElementName="RunwayExtension", Namespace="http://www.idscompany/schema/5.1")]
		public RunwayExtension RunwayExtension { get; set; }
		[XmlElement(ElementName="RunwayDirectionExtension", Namespace="http://www.idscompany/schema/5.1")]
		public RunwayDirectionExtension RunwayDirectionExtension { get; set; }
		[XmlElement(ElementName="RunwayProtectAreaExtension", Namespace="http://www.idscompany/schema/5.1")]
		public RunwayProtectAreaExtension RunwayProtectAreaExtension { get; set; }
		[XmlElement(ElementName="RunwayDirectionLightSystemExtension", Namespace="http://www.idscompany/schema/5.1")]
		public RunwayDirectionLightSystemExtension RunwayDirectionLightSystemExtension { get; set; }
		[XmlElement(ElementName="TouchDownLiftOffExtension", Namespace="http://www.idscompany/schema/5.1")]
		public TouchDownLiftOffExtension TouchDownLiftOffExtension { get; set; }
		[XmlElement(ElementName="RunwayCentrelinePointExtension", Namespace="http://www.idscompany/schema/5.1")]
		public RunwayCentrelinePointExtension RunwayCentrelinePointExtension { get; set; }
		[XmlElement(ElementName="RunwayDeclaredDistanceExtension", Namespace="http://www.idscompany/schema/5.1")]
		public RunwayDeclaredDistanceExtension RunwayDeclaredDistanceExtension { get; set; }
		[XmlElement(ElementName="NavaidExtension", Namespace="http://www.idscompany/schema/5.1")]
		public NavaidExtension NavaidExtension { get; set; }
		[XmlElement(ElementName="LocalizerExtension", Namespace="http://www.idscompany/schema/5.1")]
		public LocalizerExtension LocalizerExtension { get; set; }
		[XmlElement(ElementName="GlidepathExtension", Namespace="http://www.idscompany/schema/5.1")]
		public GlidepathExtension GlidepathExtension { get; set; }
		[XmlElement(ElementName="DMEExtension", Namespace="http://www.idscompany/schema/5.1")]
		public DMEExtension DMEExtension { get; set; }
		[XmlElement(ElementName="VORExtension", Namespace="http://www.idscompany/schema/5.1")]
		public VORExtension VORExtension { get; set; }
		[XmlElement(ElementName="NDBExtension", Namespace="http://www.idscompany/schema/5.1")]
		public NDBExtension NDBExtension { get; set; }
		[XmlElement(ElementName="CallsignDetailExtension", Namespace="http://www.idscompany/schema/5.1")]
		public CallsignDetailExtension CallsignDetailExtension { get; set; }
		[XmlElement(ElementName="RadioCommunicationChannelExtension", Namespace="http://www.idscompany/schema/5.1")]
		public RadioCommunicationChannelExtension RadioCommunicationChannelExtension { get; set; }
		[XmlElement(ElementName="DesignatedPointExtension", Namespace="http://www.idscompany/schema/5.1")]
		public DesignatedPointExtension DesignatedPointExtension { get; set; }
		[XmlElement(ElementName="GeoBorderExtension", Namespace="http://www.idscompany/schema/5.1")]
		public GeoBorderExtension GeoBorderExtension { get; set; }
		[XmlElement(ElementName="AirspaceVolumeDependencyExtension", Namespace="http://www.idscompany/schema/5.1")]
		public AirspaceVolumeDependencyExtension AirspaceVolumeDependencyExtension { get; set; }
		[XmlElement(ElementName="AirspaceVolumeExtension", Namespace="http://www.idscompany/schema/5.1")]
		public AirspaceVolumeExtension AirspaceVolumeExtension { get; set; }
		[XmlElement(ElementName="AirspaceExtension", Namespace="http://www.idscompany/schema/5.1")]
		public AirspaceExtension AirspaceExtension { get; set; }
		[XmlElement(ElementName="AuthorityForAirspaceExtension", Namespace="http://www.idscompany/schema/5.1")]
		public AuthorityForAirspaceExtension AuthorityForAirspaceExtension { get; set; }
	}

	[XmlRoot(ElementName="OrganisationAuthorityTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class OrganisationAuthorityTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TimeSlice {
		[XmlElement(ElementName="OrganisationAuthorityTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public OrganisationAuthorityTimeSlice OrganisationAuthorityTimeSlice { get; set; }
		[XmlElement(ElementName="UnitTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public UnitTimeSlice UnitTimeSlice { get; set; }
		[XmlElement(ElementName="AirportHeliportTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportHeliportTimeSlice AirportHeliportTimeSlice { get; set; }
		[XmlElement(ElementName="FireFightingServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public FireFightingServiceTimeSlice FireFightingServiceTimeSlice { get; set; }
		[XmlElement(ElementName="AirportClearanceServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportClearanceServiceTimeSlice AirportClearanceServiceTimeSlice { get; set; }
		[XmlElement(ElementName="AircraftGroundServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public AircraftGroundServiceTimeSlice AircraftGroundServiceTimeSlice { get; set; }
		[XmlElement(ElementName="AirportSuppliesServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportSuppliesServiceTimeSlice AirportSuppliesServiceTimeSlice { get; set; }
		[XmlElement(ElementName="PassengerServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public PassengerServiceTimeSlice PassengerServiceTimeSlice { get; set; }
		[XmlElement(ElementName="TaxiwayTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TaxiwayTimeSlice TaxiwayTimeSlice { get; set; }
		[XmlElement(ElementName="TaxiwayLightSystemTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TaxiwayLightSystemTimeSlice TaxiwayLightSystemTimeSlice { get; set; }
		[XmlElement(ElementName="ApronTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public ApronTimeSlice ApronTimeSlice { get; set; }
		[XmlElement(ElementName="ApronLightSystemTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public ApronLightSystemTimeSlice ApronLightSystemTimeSlice { get; set; }
		[XmlElement(ElementName="RunwayTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayTimeSlice RunwayTimeSlice { get; set; }
		[XmlElement(ElementName="RunwayDirectionTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayDirectionTimeSlice RunwayDirectionTimeSlice { get; set; }
		[XmlElement(ElementName="RunwayProtectAreaTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayProtectAreaTimeSlice RunwayProtectAreaTimeSlice { get; set; }
		[XmlElement(ElementName="RunwayDirectionLightSystemTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayDirectionLightSystemTimeSlice RunwayDirectionLightSystemTimeSlice { get; set; }
		[XmlElement(ElementName="VisualGlideSlopeIndicatorTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public VisualGlideSlopeIndicatorTimeSlice VisualGlideSlopeIndicatorTimeSlice { get; set; }
		[XmlElement(ElementName="TouchDownLiftOffTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TouchDownLiftOffTimeSlice TouchDownLiftOffTimeSlice { get; set; }
		[XmlElement(ElementName="RunwayCentrelinePointTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayCentrelinePointTimeSlice RunwayCentrelinePointTimeSlice { get; set; }
		[XmlElement(ElementName="NavaidTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public NavaidTimeSlice NavaidTimeSlice { get; set; }
		[XmlElement(ElementName="LocalizerTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public LocalizerTimeSlice LocalizerTimeSlice { get; set; }
		[XmlElement(ElementName="GlidepathTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public GlidepathTimeSlice GlidepathTimeSlice { get; set; }
		[XmlElement(ElementName="DMETimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public DMETimeSlice DMETimeSlice { get; set; }
		[XmlElement(ElementName="VORTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public VORTimeSlice VORTimeSlice { get; set; }
		[XmlElement(ElementName="NDBTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public NDBTimeSlice NDBTimeSlice { get; set; }
		[XmlElement(ElementName="RadioFrequencyAreaTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public RadioFrequencyAreaTimeSlice RadioFrequencyAreaTimeSlice { get; set; }
		[XmlElement(ElementName="InformationServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public InformationServiceTimeSlice InformationServiceTimeSlice { get; set; }
		[XmlElement(ElementName="AirTrafficControlServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirTrafficControlServiceTimeSlice AirTrafficControlServiceTimeSlice { get; set; }
		[XmlElement(ElementName="RadioCommunicationChannelTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public RadioCommunicationChannelTimeSlice RadioCommunicationChannelTimeSlice { get; set; }
		[XmlElement(ElementName="DesignatedPointTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public DesignatedPointTimeSlice DesignatedPointTimeSlice { get; set; }
		[XmlElement(ElementName="GeoBorderTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public GeoBorderTimeSlice GeoBorderTimeSlice { get; set; }
		[XmlElement(ElementName="AirspaceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirspaceTimeSlice AirspaceTimeSlice { get; set; }
		[XmlElement(ElementName="AuthorityForAirspaceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public AuthorityForAirspaceTimeSlice AuthorityForAirspaceTimeSlice { get; set; }
        [XmlElement(ElementName = "VerticalStructureTimeSlice", Namespace = "http://www.aixm.aero/schema/5.1")]
        public VerticalStructureTimeSlice VerticalStructureTimeSlice { get; set; }
    }

    [XmlRoot(ElementName = "VerticalStructure", Namespace = "http://www.aixm.aero/schema/5.1")]
    public class VerticalStructure
    {
        [XmlElement(ElementName = "identifier", Namespace = "http://www.opengis.net/gml/3.2")]
        public Identifier Identifier { get; set; }
        [XmlElement(ElementName = "timeSlice", Namespace = "http://www.aixm.aero/schema/5.1")]
        public TimeSlice TimeSlice { get; set; }
        [XmlAttribute(AttributeName = "id", Namespace = "http://www.opengis.net/gml/3.2")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "VerticalStructureTimeSlice", Namespace = "http://www.aixm.aero/schema/5.1")]
    public class VerticalStructureTimeSlice
    {
        [XmlElement(ElementName = "validTime", Namespace = "http://www.opengis.net/gml/3.2")]
        public ValidTime ValidTime { get; set; }
        [XmlElement(ElementName = "interpretation", Namespace = "http://www.aixm.aero/schema/5.1")]
        public string Interpretation { get; set; }
        [XmlElement(ElementName = "sequenceNumber", Namespace = "http://www.aixm.aero/schema/5.1")]
        public string SequenceNumber { get; set; }
        [XmlElement(ElementName = "featureLifetime", Namespace = "http://www.aixm.aero/schema/5.1")]
        public FeatureLifetime FeatureLifetime { get; set; }
        [XmlElement(ElementName = "name", Namespace = "http://www.aixm.aero/schema/5.1")]
        public string Name { get; set; }
        [XmlElement(ElementName = "type", Namespace = "http://www.aixm.aero/schema/5.1")]
        public string Type { get; set; }
        [XmlElement(ElementName = "lighted", Namespace = "http://www.aixm.aero/schema/5.1")]
        public string Lighted { get; set; }
        [XmlElement(ElementName = "group", Namespace = "http://www.aixm.aero/schema/5.1")]
        public string Group { get; set; }
        [XmlElement(ElementName = "part", Namespace = "http://www.aixm.aero/schema/5.1")]
        public Part Part { get; set; }
        [XmlElement(ElementName = "annotation", Namespace = "http://www.aixm.aero/schema/5.1")]
        public List<Annotation> Annotation { get; set; }
        [XmlElement(ElementName = "extension", Namespace = "http://www.aixm.aero/schema/5.1")]
        public Extension Extension { get; set; }
        [XmlAttribute(AttributeName = "id", Namespace = "http://www.opengis.net/gml/3.2")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "VerticalStructurePartExtension", Namespace = "http://www.idscompany/schema/5.1")]
    public class VerticalStructurePartExtension
    {
        [XmlElement(ElementName = "customXMLExtension", Namespace = "http://www.idscompany/schema/5.1")]
        public string CustomXMLExtension { get; set; }
        [XmlElement(ElementName = "sequenceNumber", Namespace = "http://www.idscompany/schema/5.1")]
        public string SequenceNumber { get; set; }
        [XmlAttribute(AttributeName = "id", Namespace = "http://www.opengis.net/gml/3.2")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "VerticalStructurePart", Namespace = "http://www.aixm.aero/schema/5.1")]
    public class VerticalStructurePart
    {
        [XmlElement(ElementName = "horizontalProjection_location", Namespace = "http://www.aixm.aero/schema/5.1")]
        public HorizontalProjection_location HorizontalProjection_location { get; set; }
        [XmlElement(ElementName = "extension", Namespace = "http://www.aixm.aero/schema/5.1")]
        public Extension Extension { get; set; }
        [XmlAttribute(AttributeName = "id", Namespace = "http://www.opengis.net/gml/3.2")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "horizontalProjection_location", Namespace = "http://www.aixm.aero/schema/5.1")]
    public class HorizontalProjection_location
    {
        [XmlElement(ElementName = "ElevatedPoint", Namespace = "http://www.aixm.aero/schema/5.1")]
        public ElevatedPoint ElevatedPoint { get; set; }
    }

    [XmlRoot(ElementName = "part", Namespace = "http://www.aixm.aero/schema/5.1")]
    public class Part
    {
        [XmlElement(ElementName = "VerticalStructurePart", Namespace = "http://www.aixm.aero/schema/5.1")]
        public List<VerticalStructurePart> VerticalStructurePart { get; set; }
    }

    [XmlRoot(ElementName = "VerticalStructureExtension", Namespace = "http://www.idscompany/schema/5.1")]
    public class VerticalStructureExtension
    {
        [XmlElement(ElementName = "theAeroDBPk", Namespace = "http://www.idscompany/schema/5.1")]
        public TheAeroDBPk TheAeroDBPk { get; set; }
        [XmlElement(ElementName = "identifier", Namespace = "http://www.idscompany/schema/5.1")]
        public string Identifier2 { get; set; }
        [XmlAttribute(AttributeName = "id", Namespace = "http://www.opengis.net/gml/3.2")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName="OrganisationAuthority", Namespace="http://www.aixm.aero/schema/5.1")]
	public class OrganisationAuthority {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="hasMember", Namespace="http://www.aixm.aero/schema/5.1/message")]
	public class HasMember {
		[XmlElement(ElementName="OrganisationAuthority", Namespace="http://www.aixm.aero/schema/5.1")]
		public OrganisationAuthority OrganisationAuthority { get; set; }
		[XmlElement(ElementName="Unit", Namespace="http://www.aixm.aero/schema/5.1")]
		public Unit Unit { get; set; }
		[XmlElement(ElementName="AirportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportHeliport AirportHeliport { get; set; }
		[XmlElement(ElementName="FireFightingService", Namespace="http://www.aixm.aero/schema/5.1")]
		public FireFightingService FireFightingService { get; set; }
		[XmlElement(ElementName="AirportClearanceService", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportClearanceService AirportClearanceService { get; set; }
		[XmlElement(ElementName="AircraftGroundService", Namespace="http://www.aixm.aero/schema/5.1")]
		public AircraftGroundService AircraftGroundService { get; set; }
		[XmlElement(ElementName="AirportSuppliesService", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportSuppliesService AirportSuppliesService { get; set; }
		[XmlElement(ElementName="PassengerService", Namespace="http://www.aixm.aero/schema/5.1")]
		public PassengerService PassengerService { get; set; }
		[XmlElement(ElementName="Taxiway", Namespace="http://www.aixm.aero/schema/5.1")]
		public Taxiway Taxiway { get; set; }
		[XmlElement(ElementName="TaxiwayLightSystem", Namespace="http://www.aixm.aero/schema/5.1")]
		public TaxiwayLightSystem TaxiwayLightSystem { get; set; }
		[XmlElement(ElementName="Apron", Namespace="http://www.aixm.aero/schema/5.1")]
		public Apron Apron { get; set; }
		[XmlElement(ElementName="ApronLightSystem", Namespace="http://www.aixm.aero/schema/5.1")]
		public ApronLightSystem ApronLightSystem { get; set; }
		[XmlElement(ElementName="Runway", Namespace="http://www.aixm.aero/schema/5.1")]
		public Runway Runway { get; set; }
		[XmlElement(ElementName="RunwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayDirection RunwayDirection { get; set; }
		[XmlElement(ElementName="RunwayProtectArea", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayProtectArea RunwayProtectArea { get; set; }
		[XmlElement(ElementName="RunwayDirectionLightSystem", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayDirectionLightSystem RunwayDirectionLightSystem { get; set; }
		[XmlElement(ElementName="VisualGlideSlopeIndicator", Namespace="http://www.aixm.aero/schema/5.1")]
		public VisualGlideSlopeIndicator VisualGlideSlopeIndicator { get; set; }
		[XmlElement(ElementName="TouchDownLiftOff", Namespace="http://www.aixm.aero/schema/5.1")]
		public TouchDownLiftOff TouchDownLiftOff { get; set; }
		[XmlElement(ElementName="RunwayCentrelinePoint", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayCentrelinePoint RunwayCentrelinePoint { get; set; }
		[XmlElement(ElementName="Navaid", Namespace="http://www.aixm.aero/schema/5.1")]
		public Navaid Navaid { get; set; }
		[XmlElement(ElementName="Localizer", Namespace="http://www.aixm.aero/schema/5.1")]
		public Localizer Localizer { get; set; }
		[XmlElement(ElementName="Glidepath", Namespace="http://www.aixm.aero/schema/5.1")]
		public Glidepath Glidepath { get; set; }
		[XmlElement(ElementName="DME", Namespace="http://www.aixm.aero/schema/5.1")]
		public DME DME { get; set; }
		[XmlElement(ElementName="VOR", Namespace="http://www.aixm.aero/schema/5.1")]
		public VOR VOR { get; set; }
		[XmlElement(ElementName="NDB", Namespace="http://www.aixm.aero/schema/5.1")]
		public NDB NDB { get; set; }
		[XmlElement(ElementName="RadioFrequencyArea", Namespace="http://www.aixm.aero/schema/5.1")]
		public RadioFrequencyArea RadioFrequencyArea { get; set; }
		[XmlElement(ElementName="InformationService", Namespace="http://www.aixm.aero/schema/5.1")]
		public InformationService InformationService { get; set; }
		[XmlElement(ElementName="AirTrafficControlService", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirTrafficControlService AirTrafficControlService { get; set; }
		[XmlElement(ElementName="RadioCommunicationChannel", Namespace="http://www.aixm.aero/schema/5.1")]
		public RadioCommunicationChannel RadioCommunicationChannel { get; set; }
		[XmlElement(ElementName="DesignatedPoint", Namespace="http://www.aixm.aero/schema/5.1")]
		public DesignatedPoint DesignatedPoint { get; set; }
		[XmlElement(ElementName="GeoBorder", Namespace="http://www.aixm.aero/schema/5.1")]
		public GeoBorder GeoBorder { get; set; }
		[XmlElement(ElementName="Airspace", Namespace="http://www.aixm.aero/schema/5.1")]
		public Airspace Airspace { get; set; }
		[XmlElement(ElementName="AuthorityForAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
		public AuthorityForAirspace AuthorityForAirspace { get; set; }
        [XmlElement(ElementName = "VerticalStructure", Namespace = "http://www.aixm.aero/schema/5.1")]
        public VerticalStructure VerticalStructure { get; set; }
    }

	[XmlRoot(ElementName="ElevatedPoint", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ElevatedPoint {
		[XmlElement(ElementName="pos", Namespace="http://www.opengis.net/gml/3.2")]
		public string Pos { get; set; }
		[XmlAttribute(AttributeName="srsDimension")]
		public string SrsDimension { get; set; }
		[XmlAttribute(AttributeName="srsName")]
		public string SrsName { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="elevation", Namespace="http://www.aixm.aero/schema/5.1")]
		public Elevation Elevation { get; set; }
		[XmlElement(ElementName="verticalDatum", Namespace="http://www.aixm.aero/schema/5.1")]
		public string VerticalDatum { get; set; }
        [XmlElement(ElementName = "verticalAccuracy", Namespace = "http://www.aixm.aero/schema/5.1")]
        public VerticalAccuracy VerticalAccuracy { get; set; }
        [XmlElement(ElementName = "horizontalAccuracy", Namespace = "http://www.aixm.aero/schema/5.1")]
        public HorizontalAccuracy HorizontalAccuracy { get; set; }
    }

	[XmlRoot(ElementName="position", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Position {
		[XmlElement(ElementName="ElevatedPoint", Namespace="http://www.aixm.aero/schema/5.1")]
		public ElevatedPoint ElevatedPoint { get; set; }
	}

	[XmlRoot(ElementName="airportLocation", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportLocation {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="ownerOrganisation", Namespace="http://www.aixm.aero/schema/5.1")]
	public class OwnerOrganisation {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="UnitExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class UnitExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="UnitTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class UnitTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="compliantICAO", Namespace="http://www.aixm.aero/schema/5.1")]
		public string CompliantICAO { get; set; }
		[XmlElement(ElementName="position", Namespace="http://www.aixm.aero/schema/5.1")]
		public Position Position { get; set; }
		[XmlElement(ElementName="airportLocation", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportLocation AirportLocation { get; set; }
		[XmlElement(ElementName="ownerOrganisation", Namespace="http://www.aixm.aero/schema/5.1")]
		public OwnerOrganisation OwnerOrganisation { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="relatedUnit", Namespace="http://www.aixm.aero/schema/5.1")]
		public RelatedUnit RelatedUnit { get; set; }
	}

	[XmlRoot(ElementName="Unit", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Unit {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="theUnit", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TheUnit {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="UnitDependencyExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class UnitDependencyExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="UnitDependency", Namespace="http://www.aixm.aero/schema/5.1")]
	public class UnitDependency {
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="theUnit", Namespace="http://www.aixm.aero/schema/5.1")]
		public TheUnit TheUnit { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="relatedUnit", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RelatedUnit {
		[XmlElement(ElementName="UnitDependency", Namespace="http://www.aixm.aero/schema/5.1")]
		public UnitDependency UnitDependency { get; set; }
	}

	[XmlRoot(ElementName="fieldElevation", Namespace="http://www.aixm.aero/schema/5.1")]
	public class FieldElevation {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="referenceTemperature", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ReferenceTemperature {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="transitionAltitude", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TransitionAltitude {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="City", Namespace="http://www.aixm.aero/schema/5.1")]
	public class City {
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="servedCity", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ServedCity {
		[XmlElement(ElementName="City", Namespace="http://www.aixm.aero/schema/5.1")]
		public City City { get; set; }
	}

	[XmlRoot(ElementName="theOrganisationAuthority", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TheOrganisationAuthority {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="AirportHeliportResponsibilityOrganisation", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportHeliportResponsibilityOrganisation {
		[XmlElement(ElementName="theOrganisationAuthority", Namespace="http://www.aixm.aero/schema/5.1")]
		public TheOrganisationAuthority TheOrganisationAuthority { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="responsibleOrganisation", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ResponsibleOrganisation {
		[XmlElement(ElementName="AirportHeliportResponsibilityOrganisation", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportHeliportResponsibilityOrganisation AirportHeliportResponsibilityOrganisation { get; set; }
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="ARP", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ARP {
		[XmlElement(ElementName="ElevatedPoint", Namespace="http://www.aixm.aero/schema/5.1")]
		public ElevatedPoint ElevatedPoint { get; set; }
	}

	[XmlRoot(ElementName="note", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Note {
		[XmlAttribute(AttributeName="lang")]
		public string Lang { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="LinguisticNote", Namespace="http://www.aixm.aero/schema/5.1")]
	public class LinguisticNote {
		[XmlElement(ElementName="note", Namespace="http://www.aixm.aero/schema/5.1")]
		public Note Note { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="translatedNote", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TranslatedNote {
		[XmlElement(ElementName="LinguisticNote", Namespace="http://www.aixm.aero/schema/5.1")]
		public LinguisticNote LinguisticNote { get; set; }
	}

	[XmlRoot(ElementName="Note", Namespace="http://www.aixm.aero/schema/5.1")] // edited Note
	public class AnnotationNote {
		[XmlElement(ElementName="purpose", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Purpose { get; set; }
		[XmlElement(ElementName="translatedNote", Namespace="http://www.aixm.aero/schema/5.1")]
		public TranslatedNote TranslatedNote { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="annotation", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Annotation {
		[XmlElement(ElementName="Note", Namespace="http://www.aixm.aero/schema/5.1")]
		public AnnotationNote Note { get; set; } //edited Note
	}

	[XmlRoot(ElementName="AirportHeliportExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class AirportHeliportExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AirportHeliportTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportHeliportTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="locationIndicatorICAO", Namespace="http://www.aixm.aero/schema/5.1")]
		public string LocationIndicatorICAO { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="fieldElevation", Namespace="http://www.aixm.aero/schema/5.1")]
		public FieldElevation FieldElevation { get; set; }
		[XmlElement(ElementName="verticalDatum", Namespace="http://www.aixm.aero/schema/5.1")]
		public string VerticalDatum { get; set; }
		[XmlElement(ElementName="magneticVariation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string MagneticVariation { get; set; }
		[XmlElement(ElementName="dateMagneticVariation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string DateMagneticVariation { get; set; }
		[XmlElement(ElementName="magneticVariationChange", Namespace="http://www.aixm.aero/schema/5.1")]
		public string MagneticVariationChange { get; set; }
		[XmlElement(ElementName="referenceTemperature", Namespace="http://www.aixm.aero/schema/5.1")]
		public ReferenceTemperature ReferenceTemperature { get; set; }
		[XmlElement(ElementName="transitionAltitude", Namespace="http://www.aixm.aero/schema/5.1")]
		public TransitionAltitude TransitionAltitude { get; set; }
		[XmlElement(ElementName="servedCity", Namespace="http://www.aixm.aero/schema/5.1")]
		public ServedCity ServedCity { get; set; }
		[XmlElement(ElementName="responsibleOrganisation", Namespace="http://www.aixm.aero/schema/5.1")]
		public ResponsibleOrganisation ResponsibleOrganisation { get; set; }
		[XmlElement(ElementName="ARP", Namespace="http://www.aixm.aero/schema/5.1")]
		public ARP ARP { get; set; }
		[XmlElement(ElementName="annotation", Namespace="http://www.aixm.aero/schema/5.1")]
		public Annotation Annotation { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="designatorIATA", Namespace="http://www.aixm.aero/schema/5.1")]
		public string DesignatorIATA { get; set; }
	}

	[XmlRoot(ElementName="AirportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportHeliport {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="airportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportHeliportRef {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="ServiceExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class ServiceExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.idscompany/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="FireFightingServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class FireFightingServiceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="airportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportHeliportRef AirportHeliport { get; set; }
		[XmlElement(ElementName="category", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Category { get; set; }
		[XmlElement(ElementName="standard", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Standard { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="availability", Namespace="http://www.aixm.aero/schema/5.1")]
		public Availability Availability { get; set; }
	}

	[XmlRoot(ElementName="FireFightingService", Namespace="http://www.aixm.aero/schema/5.1")]
	public class FireFightingService {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="Timesheet", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Timesheet {
		[XmlElement(ElementName="timeReference", Namespace="http://www.aixm.aero/schema/5.1")]
		public string TimeReference { get; set; }
		[XmlElement(ElementName="startDate", Namespace="http://www.aixm.aero/schema/5.1")]
		public string StartDate { get; set; }
		[XmlElement(ElementName="endDate", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EndDate { get; set; }
		[XmlElement(ElementName="day", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Day { get; set; }
		[XmlElement(ElementName="startTime", Namespace="http://www.aixm.aero/schema/5.1")]
		public string StartTime { get; set; }
		[XmlElement(ElementName="endTime", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EndTime { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="endEvent", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EndEvent { get; set; }
	}

	[XmlRoot(ElementName="timeInterval", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TimeInterval {
		[XmlElement(ElementName="Timesheet", Namespace="http://www.aixm.aero/schema/5.1")]
		public Timesheet Timesheet { get; set; }
	}

	[XmlRoot(ElementName="ServiceOperationalStatus", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ServiceOperationalStatus {
		[XmlElement(ElementName="timeInterval", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<TimeInterval> TimeInterval { get; set; }
		[XmlElement(ElementName="operationalStatus", Namespace="http://www.aixm.aero/schema/5.1")]
		public string OperationalStatus { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="availability", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Availability {
		[XmlElement(ElementName="ServiceOperationalStatus", Namespace="http://www.aixm.aero/schema/5.1")]
		public ServiceOperationalStatus ServiceOperationalStatus { get; set; }
	}

	[XmlRoot(ElementName="AirportClearanceServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportClearanceServiceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="airportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportHeliportRef AirportHeliport { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AirportClearanceService", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportClearanceService {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AircraftGroundServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AircraftGroundServiceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="airportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportHeliportRef AirportHeliport { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="availability", Namespace="http://www.aixm.aero/schema/5.1")]
		public Availability Availability { get; set; }
	}

	[XmlRoot(ElementName="AircraftGroundService", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AircraftGroundService {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AirportSuppliesServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportSuppliesServiceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="availability", Namespace="http://www.aixm.aero/schema/5.1")]
		public Availability Availability { get; set; }
		[XmlElement(ElementName="airportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportHeliportRef AirportHeliport { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="oilSupply", Namespace="http://www.aixm.aero/schema/5.1")]
		public OilSupply OilSupply { get; set; }
		[XmlElement(ElementName="fuelSupply", Namespace="http://www.aixm.aero/schema/5.1")]
		public FuelSupply FuelSupply { get; set; }
	}

	[XmlRoot(ElementName="AirportSuppliesService", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirportSuppliesService {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="Oil", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Oil {
		[XmlElement(ElementName="category", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Category { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="oilSupply", Namespace="http://www.aixm.aero/schema/5.1")]
	public class OilSupply {
		[XmlElement(ElementName="Oil", Namespace="http://www.aixm.aero/schema/5.1")]
		public Oil Oil { get; set; }
	}

	[XmlRoot(ElementName="PassengerServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class PassengerServiceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="airportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirportHeliportRef AirportHeliport { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="availability", Namespace="http://www.aixm.aero/schema/5.1")]
		public Availability Availability { get; set; }
	}

	[XmlRoot(ElementName="PassengerService", Namespace="http://www.aixm.aero/schema/5.1")]
	public class PassengerService {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="Fuel", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Fuel {
		[XmlElement(ElementName="category", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Category { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="fuelSupply", Namespace="http://www.aixm.aero/schema/5.1")]
	public class FuelSupply {
		[XmlElement(ElementName="Fuel", Namespace="http://www.aixm.aero/schema/5.1")]
		public Fuel Fuel { get; set; }
	}

	[XmlRoot(ElementName="width", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Width {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="SurfaceCharacteristics", Namespace="http://www.aixm.aero/schema/5.1")]
	public class SurfaceCharacteristics {
		[XmlElement(ElementName="composition", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Composition { get; set; }
		[XmlElement(ElementName="classPCN", Namespace="http://www.aixm.aero/schema/5.1")]
		public string ClassPCN { get; set; }
		[XmlElement(ElementName="pavementTypePCN", Namespace="http://www.aixm.aero/schema/5.1")]
		public string PavementTypePCN { get; set; }
		[XmlElement(ElementName="pavementSubgradePCN", Namespace="http://www.aixm.aero/schema/5.1")]
		public string PavementSubgradePCN { get; set; }
		[XmlElement(ElementName="maxTyrePressurePCN", Namespace="http://www.aixm.aero/schema/5.1")]
		public string MaxTyrePressurePCN { get; set; }
		[XmlElement(ElementName="evaluationMethodPCN", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EvaluationMethodPCN { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="surfaceCondition", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SurfaceCondition { get; set; }
		[XmlElement(ElementName="preparation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Preparation { get; set; }
	}

	[XmlRoot(ElementName="surfaceProperties", Namespace="http://www.aixm.aero/schema/5.1")]
	public class SurfaceProperties {
		[XmlElement(ElementName="SurfaceCharacteristics", Namespace="http://www.aixm.aero/schema/5.1")]
		public SurfaceCharacteristics SurfaceCharacteristics { get; set; }
	}

	[XmlRoot(ElementName="associatedAirportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AssociatedAirportHeliport {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="TaxiwayExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class TaxiwayExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="TaxiwayTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TaxiwayTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="width", Namespace="http://www.aixm.aero/schema/5.1")]
		public Width Width { get; set; }
		[XmlElement(ElementName="surfaceProperties", Namespace="http://www.aixm.aero/schema/5.1")]
		public SurfaceProperties SurfaceProperties { get; set; }
		[XmlElement(ElementName="associatedAirportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AssociatedAirportHeliport AssociatedAirportHeliport { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="Taxiway", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Taxiway {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="lightedTaxiway", Namespace="http://www.aixm.aero/schema/5.1")]
	public class LightedTaxiway {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="TaxiwayLightSystemExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class TaxiwayLightSystemExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="TaxiwayLightSystemTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TaxiwayLightSystemTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="position", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Position { get; set; }
		[XmlElement(ElementName="lightedTaxiway", Namespace="http://www.aixm.aero/schema/5.1")]
		public LightedTaxiway LightedTaxiway { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="colour", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Colour { get; set; }
		[XmlElement(ElementName="annotation", Namespace="http://www.aixm.aero/schema/5.1")]
		public Annotation Annotation { get; set; }
	}

	[XmlRoot(ElementName="TaxiwayLightSystem", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TaxiwayLightSystem {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="ApronExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class ApronExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="ApronTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ApronTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="surfaceProperties", Namespace="http://www.aixm.aero/schema/5.1")]
		public SurfaceProperties SurfaceProperties { get; set; }
		[XmlElement(ElementName="associatedAirportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AssociatedAirportHeliport AssociatedAirportHeliport { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="Apron", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Apron {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="lightedApron", Namespace="http://www.aixm.aero/schema/5.1")]
	public class LightedApron {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="ApronLightSystemExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class ApronLightSystemExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="ApronLightSystemTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ApronLightSystemTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="position", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Position { get; set; }
		[XmlElement(ElementName="lightedApron", Namespace="http://www.aixm.aero/schema/5.1")]
		public LightedApron LightedApron { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="intensityLevel", Namespace="http://www.aixm.aero/schema/5.1")]
		public string IntensityLevel { get; set; }
		[XmlElement(ElementName="colour", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Colour { get; set; }
	}

	[XmlRoot(ElementName="ApronLightSystem", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ApronLightSystem {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="nominalWidth", Namespace="http://www.aixm.aero/schema/5.1")]
	public class NominalWidth {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="RunwayExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class RunwayExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RunwayTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="nominalWidth", Namespace="http://www.aixm.aero/schema/5.1")]
		public NominalWidth NominalWidth { get; set; }
		[XmlElement(ElementName="surfaceProperties", Namespace="http://www.aixm.aero/schema/5.1")]
		public SurfaceProperties SurfaceProperties { get; set; }
		[XmlElement(ElementName="associatedAirportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AssociatedAirportHeliport AssociatedAirportHeliport { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="widthStrip", Namespace="http://www.aixm.aero/schema/5.1")]
		public WidthStrip WidthStrip { get; set; }
		[XmlElement(ElementName="annotation", Namespace="http://www.aixm.aero/schema/5.1")]
		public Annotation Annotation { get; set; }
	}

	[XmlRoot(ElementName="Runway", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Runway {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="widthStrip", Namespace="http://www.aixm.aero/schema/5.1")]
	public class WidthStrip {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="usedRunway", Namespace="http://www.aixm.aero/schema/5.1")]
	public class UsedRunway {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="RunwayDirectionExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class RunwayDirectionExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlElement(ElementName="customXMLExtension", Namespace="http://www.idscompany/schema/5.1")]
		public string CustomXMLExtension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RunwayDirectionTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayDirectionTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="trueBearing", Namespace="http://www.aixm.aero/schema/5.1")]
		public string TrueBearing { get; set; }
		[XmlElement(ElementName="magneticBearing", Namespace="http://www.aixm.aero/schema/5.1")]
		public string MagneticBearing { get; set; }
		[XmlElement(ElementName="usedRunway", Namespace="http://www.aixm.aero/schema/5.1")]
		public UsedRunway UsedRunway { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="elevationTDZ", Namespace="http://www.aixm.aero/schema/5.1")]
		public ElevationTDZ ElevationTDZ { get; set; }
		[XmlElement(ElementName="annotation", Namespace="http://www.aixm.aero/schema/5.1")]
		public Annotation Annotation { get; set; }
	}

	[XmlRoot(ElementName="RunwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayDirection {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="elevationTDZ", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ElevationTDZ {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="protectedRunwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ProtectedRunwayDirection {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="RunwayProtectAreaExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class RunwayProtectAreaExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RunwayProtectAreaTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayProtectAreaTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="width", Namespace="http://www.aixm.aero/schema/5.1")]
		public Width Width { get; set; }
		[XmlElement(ElementName="surfaceProperties", Namespace="http://www.aixm.aero/schema/5.1")]
		public SurfaceProperties SurfaceProperties { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="protectedRunwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
		public ProtectedRunwayDirection ProtectedRunwayDirection { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RunwayProtectArea", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayProtectArea {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="associatedRunwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AssociatedRunwayDirection {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="RunwayDirectionLightSystemExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class RunwayDirectionLightSystemExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RunwayDirectionLightSystemTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayDirectionLightSystemTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="colour", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Colour { get; set; }
		[XmlElement(ElementName="position", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Position { get; set; }
		[XmlElement(ElementName="associatedRunwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
		public AssociatedRunwayDirection AssociatedRunwayDirection { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="intensityLevel", Namespace="http://www.aixm.aero/schema/5.1")]
		public string IntensityLevel { get; set; }
	}

	[XmlRoot(ElementName="RunwayDirectionLightSystem", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayDirectionLightSystem {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="minimumEyeHeightOverThreshold", Namespace="http://www.aixm.aero/schema/5.1")]
	public class MinimumEyeHeightOverThreshold {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="runwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayDirectionRef {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="VisualGlideSlopeIndicatorTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class VisualGlideSlopeIndicatorTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="slopeAngle", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SlopeAngle { get; set; }
		[XmlElement(ElementName="minimumEyeHeightOverThreshold", Namespace="http://www.aixm.aero/schema/5.1")]
		public MinimumEyeHeightOverThreshold MinimumEyeHeightOverThreshold { get; set; }
		[XmlElement(ElementName="runwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayDirectionRef RunwayDirection { get; set; } //edited RunwayDirection
        [XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="position", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Position { get; set; }
		[XmlElement(ElementName="numberBox", Namespace="http://www.aixm.aero/schema/5.1")]
		public string NumberBox { get; set; }
	}

	[XmlRoot(ElementName="VisualGlideSlopeIndicator", Namespace="http://www.aixm.aero/schema/5.1")]
	public class VisualGlideSlopeIndicator {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="aimingPoint", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AimingPoint {
		[XmlElement(ElementName="ElevatedPoint", Namespace="http://www.aixm.aero/schema/5.1")]
		public ElevatedPoint ElevatedPoint { get; set; }
	}

	[XmlRoot(ElementName="TouchDownLiftOffExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class TouchDownLiftOffExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="TouchDownLiftOffTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TouchDownLiftOffTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="aimingPoint", Namespace="http://www.aixm.aero/schema/5.1")]
		public AimingPoint AimingPoint { get; set; }
		[XmlElement(ElementName="associatedAirportHeliport", Namespace="http://www.aixm.aero/schema/5.1")]
		public AssociatedAirportHeliport AssociatedAirportHeliport { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="width", Namespace="http://www.aixm.aero/schema/5.1")]
		public Width Width { get; set; }
		[XmlElement(ElementName="surfaceProperties", Namespace="http://www.aixm.aero/schema/5.1")]
		public SurfaceProperties SurfaceProperties { get; set; }
	}

	[XmlRoot(ElementName="TouchDownLiftOff", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TouchDownLiftOff {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="elevation", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Elevation {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Location {
		[XmlElement(ElementName="ElevatedPoint", Namespace="http://www.aixm.aero/schema/5.1")]
		public ElevatedPoint ElevatedPoint { get; set; }
		[XmlElement(ElementName="Point", Namespace="http://www.aixm.aero/schema/5.1")]
		public Point Point { get; set; }
	}

	[XmlRoot(ElementName="onRunway", Namespace="http://www.aixm.aero/schema/5.1")]
	public class OnRunway {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="RunwayCentrelinePointExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class RunwayCentrelinePointExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlElement(ElementName="customXMLExtension", Namespace="http://www.idscompany/schema/5.1")]
		public string CustomXMLExtension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RunwayCentrelinePointTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayCentrelinePointTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="onRunway", Namespace="http://www.aixm.aero/schema/5.1")]
		public OnRunway OnRunway { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="role", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Role { get; set; }
		[XmlElement(ElementName="associatedDeclaredDistance", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<AssociatedDeclaredDistance> AssociatedDeclaredDistance { get; set; }
	}

	[XmlRoot(ElementName="RunwayCentrelinePoint", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayCentrelinePoint {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="distance", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Distance {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="RunwayDeclaredDistanceValue", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayDeclaredDistanceValue {
		[XmlElement(ElementName="distance", Namespace="http://www.aixm.aero/schema/5.1")]
		public Distance Distance { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="declaredValue", Namespace="http://www.aixm.aero/schema/5.1")]
	public class DeclaredValue {
		[XmlElement(ElementName="RunwayDeclaredDistanceValue", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayDeclaredDistanceValue RunwayDeclaredDistanceValue { get; set; }
	}

	[XmlRoot(ElementName="RunwayDeclaredDistanceExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class RunwayDeclaredDistanceExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RunwayDeclaredDistance", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RunwayDeclaredDistance {
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="declaredValue", Namespace="http://www.aixm.aero/schema/5.1")]
		public DeclaredValue DeclaredValue { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="annotation", Namespace="http://www.aixm.aero/schema/5.1")]
		public Annotation Annotation { get; set; }
	}

	[XmlRoot(ElementName="associatedDeclaredDistance", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AssociatedDeclaredDistance {
		[XmlElement(ElementName="RunwayDeclaredDistance", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayDeclaredDistance RunwayDeclaredDistance { get; set; }
	}

	[XmlRoot(ElementName="theNavaidEquipment", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TheNavaidEquipment {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="NavaidComponent", Namespace="http://www.aixm.aero/schema/5.1")]
	public class NavaidComponent {
		[XmlElement(ElementName="theNavaidEquipment", Namespace="http://www.aixm.aero/schema/5.1")]
		public TheNavaidEquipment TheNavaidEquipment { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="navaidEquipment", Namespace="http://www.aixm.aero/schema/5.1")]
	public class NavaidEquipment {
		[XmlElement(ElementName="NavaidComponent", Namespace="http://www.aixm.aero/schema/5.1")]
		public NavaidComponent NavaidComponent { get; set; }
	}

	[XmlRoot(ElementName="servedAirport", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ServedAirport {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="NavaidExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class NavaidExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlElement(ElementName="customXMLExtension", Namespace="http://www.idscompany/schema/5.1")]
		public string CustomXMLExtension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="NavaidTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class NavaidTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="signalPerformance", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SignalPerformance { get; set; }
		[XmlElement(ElementName="navaidEquipment", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<NavaidEquipment> NavaidEquipment { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="runwayDirection", Namespace="http://www.aixm.aero/schema/5.1")]
		public RunwayDirectionRef RunwayDirection { get; set; } //edited RunwayDirection
		[XmlElement(ElementName="servedAirport", Namespace="http://www.aixm.aero/schema/5.1")]
		public ServedAirport ServedAirport { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
	}

	[XmlRoot(ElementName="Navaid", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Navaid {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="frequency", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Frequency {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="LocalizerExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class LocalizerExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="LocalizerTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class LocalizerTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="emissionClass", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EmissionClass { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="frequency", Namespace="http://www.aixm.aero/schema/5.1")]
		public Frequency Frequency { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="magneticVariation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string MagneticVariation { get; set; }
		[XmlElement(ElementName="dateMagneticVariation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string DateMagneticVariation { get; set; }
		[XmlElement(ElementName="magneticBearing", Namespace="http://www.aixm.aero/schema/5.1")]
		public string MagneticBearing { get; set; }
	}

	[XmlRoot(ElementName="Localizer", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Localizer {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="GlidepathExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class GlidepathExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="GlidepathTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class GlidepathTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="emissionClass", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EmissionClass { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="frequency", Namespace="http://www.aixm.aero/schema/5.1")]
		public Frequency Frequency { get; set; }
		[XmlElement(ElementName="slope", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Slope { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="Glidepath", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Glidepath {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AuthorityForNavaidEquipment", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AuthorityForNavaidEquipment {
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="theOrganisationAuthority", Namespace="http://www.aixm.aero/schema/5.1")]
		public TheOrganisationAuthority TheOrganisationAuthority { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="authority", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Authority {
		[XmlElement(ElementName="AuthorityForNavaidEquipment", Namespace="http://www.aixm.aero/schema/5.1")]
		public AuthorityForNavaidEquipment AuthorityForNavaidEquipment { get; set; }
	}

	[XmlRoot(ElementName="DMEExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class DMEExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="DMETimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class DMETimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="emissionClass", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EmissionClass { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="authority", Namespace="http://www.aixm.aero/schema/5.1")]
		public Authority Authority { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="channel", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Channel { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="DME", Namespace="http://www.aixm.aero/schema/5.1")]
	public class DME {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="VORExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class VORExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="VORTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class VORTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="authority", Namespace="http://www.aixm.aero/schema/5.1")]
		public Authority Authority { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="frequency", Namespace="http://www.aixm.aero/schema/5.1")]
		public Frequency Frequency { get; set; }
		[XmlElement(ElementName="zeroBearingDirection", Namespace="http://www.aixm.aero/schema/5.1")]
		public string ZeroBearingDirection { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="emissionClass", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EmissionClass { get; set; }
		[XmlElement(ElementName="declination", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Declination { get; set; }
	}

	[XmlRoot(ElementName="VOR", Namespace="http://www.aixm.aero/schema/5.1")]
	public class VOR {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="NDBExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class NDBExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="NDBTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class NDBTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="emissionClass", Namespace="http://www.aixm.aero/schema/5.1")]
		public string EmissionClass { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="authority", Namespace="http://www.aixm.aero/schema/5.1")]
		public Authority Authority { get; set; }
		[XmlElement(ElementName="frequency", Namespace="http://www.aixm.aero/schema/5.1")]
		public Frequency Frequency { get; set; }
		[XmlElement(ElementName="class", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Class { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="magneticVariation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string MagneticVariation { get; set; }
		[XmlElement(ElementName="dateMagneticVariation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string DateMagneticVariation { get; set; }
	}

	[XmlRoot(ElementName="NDB", Namespace="http://www.aixm.aero/schema/5.1")]
	public class NDB {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="outerDistance", Namespace="http://www.aixm.aero/schema/5.1")]
	public class OuterDistance {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="lowerLimit", Namespace="http://www.aixm.aero/schema/5.1")]
	public class LowerLimit {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="CircleSector", Namespace="http://www.aixm.aero/schema/5.1")]
	public class CircleSector {
		[XmlElement(ElementName="fromAngle", Namespace="http://www.aixm.aero/schema/5.1")]
		public string FromAngle { get; set; }
		[XmlElement(ElementName="toAngle", Namespace="http://www.aixm.aero/schema/5.1")]
		public string ToAngle { get; set; }
		[XmlElement(ElementName="angleType", Namespace="http://www.aixm.aero/schema/5.1")]
		public string AngleType { get; set; }
		[XmlElement(ElementName="outerDistance", Namespace="http://www.aixm.aero/schema/5.1")]
		public OuterDistance OuterDistance { get; set; }
		[XmlElement(ElementName="lowerLimit", Namespace="http://www.aixm.aero/schema/5.1")]
		public LowerLimit LowerLimit { get; set; }
		[XmlElement(ElementName="lowerLimitReference", Namespace="http://www.aixm.aero/schema/5.1")]
		public string LowerLimitReference { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="sector", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Sector {
		[XmlElement(ElementName="CircleSector", Namespace="http://www.aixm.aero/schema/5.1")]
		public CircleSector CircleSector { get; set; }
	}

	[XmlRoot(ElementName="RadioFrequencyAreaTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RadioFrequencyAreaTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="angleScallop", Namespace="http://www.aixm.aero/schema/5.1")]
		public string AngleScallop { get; set; }
		[XmlElement(ElementName="sector", Namespace="http://www.aixm.aero/schema/5.1")]
		public Sector Sector { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RadioFrequencyArea", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RadioFrequencyArea {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="serviceProvider", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ServiceProvider {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="CallsignDetailExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class CallsignDetailExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="CallsignDetail", Namespace="http://www.aixm.aero/schema/5.1")]
	public class CallsignDetail {
		[XmlElement(ElementName="callSign", Namespace="http://www.aixm.aero/schema/5.1")]
		public string CallSign { get; set; }
		[XmlElement(ElementName="language", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Language { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="call-sign", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Callsign {
		[XmlElement(ElementName="CallsignDetail", Namespace="http://www.aixm.aero/schema/5.1")]
		public CallsignDetail CallsignDetail { get; set; }
	}

	[XmlRoot(ElementName="radioCommunication", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RadioCommunication {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="clientAirport", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ClientAirport {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="InformationServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class InformationServiceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="serviceProvider", Namespace="http://www.aixm.aero/schema/5.1")]
		public ServiceProvider ServiceProvider { get; set; }
		[XmlElement(ElementName="call-sign", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<Callsign> Callsign { get; set; }
		[XmlElement(ElementName="radioCommunication", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<RadioCommunication> RadioCommunication { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="clientAirport", Namespace="http://www.aixm.aero/schema/5.1")]
		public ClientAirport ClientAirport { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="annotation", Namespace="http://www.aixm.aero/schema/5.1")]
		public Annotation Annotation { get; set; }
	}

	[XmlRoot(ElementName="InformationService", Namespace="http://www.aixm.aero/schema/5.1")]
	public class InformationService {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="clientAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ClientAirspace {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="AirTrafficControlServiceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirTrafficControlServiceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="serviceProvider", Namespace="http://www.aixm.aero/schema/5.1")]
		public ServiceProvider ServiceProvider { get; set; }
		[XmlElement(ElementName="call-sign", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<Callsign> Callsign { get; set; }
		[XmlElement(ElementName="radioCommunication", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<RadioCommunication> RadioCommunication { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="clientAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<ClientAirspace> ClientAirspace { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="flightOperations", Namespace="http://www.aixm.aero/schema/5.1")]
		public string FlightOperations { get; set; }
	}

	[XmlRoot(ElementName="AirTrafficControlService", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirTrafficControlService {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RadioCommunicationChannelExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class RadioCommunicationChannelExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RadioCommunicationChannelTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RadioCommunicationChannelTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="RadioCommunicationChannel", Namespace="http://www.aixm.aero/schema/5.1")]
	public class RadioCommunicationChannel {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="Point", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Point {
		[XmlElement(ElementName="pos", Namespace="http://www.opengis.net/gml/3.2")]
		public string Pos { get; set; }
		[XmlAttribute(AttributeName="srsDimension")]
		public string SrsDimension { get; set; }
		[XmlAttribute(AttributeName="srsName")]
		public string SrsName { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="DesignatedPointExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class DesignatedPointExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="DesignatedPointTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class DesignatedPointTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="location", Namespace="http://www.aixm.aero/schema/5.1")]
		public Location Location { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="annotation", Namespace="http://www.aixm.aero/schema/5.1")]
		public Annotation Annotation { get; set; }
	}

	[XmlRoot(ElementName="DesignatedPoint", Namespace="http://www.aixm.aero/schema/5.1")]
	public class DesignatedPoint {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="GeodesicString", Namespace="http://www.opengis.net/gml/3.2")]
	public class GeodesicString {
		[XmlElement(ElementName="posList", Namespace="http://www.opengis.net/gml/3.2")]
		public string PosList { get; set; }
	}

	[XmlRoot(ElementName="segments", Namespace="http://www.opengis.net/gml/3.2")]
	public class Segments {
		//[XmlElement(ElementName="GeodesicString", Namespace="http://www.opengis.net/gml/3.2")]
		//public List<GeodesicString> GeodesicString { get; set; }
		//[XmlElement(ElementName="Geodesic", Namespace="http://www.opengis.net/gml/3.2")]
		//public List<Geodesic> Geodesic { get; set; }
		//[XmlElement(ElementName="CircleByCenterPoint", Namespace="http://www.opengis.net/gml/3.2")]
		//public CircleByCenterPoint CircleByCenterPoint { get; set; }
		//[XmlElement(ElementName="ArcByCenterPoint", Namespace="http://www.opengis.net/gml/3.2")]
		//public List<ArcByCenterPoint> ArcByCenterPoint { get; set; }
        [XmlElement(Type = typeof(CircleByCenterPoint)),
         XmlElement(Type = typeof(ArcByCenterPoint)),
         XmlElement(Type = typeof(Geodesic)),
         XmlElement(Type = typeof(GeodesicString))]
        public object[] SegmentElements { get; set; }
	}

	[XmlRoot(ElementName="Curve", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Curve {
		[XmlElement(ElementName="segments", Namespace="http://www.opengis.net/gml/3.2")]
		public Segments Segments { get; set; }
		[XmlAttribute(AttributeName="srsDimension")]
		public string SrsDimension { get; set; }
		[XmlAttribute(AttributeName="srsName")]
		public string SrsName { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="border", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Border {
		[XmlElement(ElementName="Curve", Namespace="http://www.aixm.aero/schema/5.1")]
		public Curve Curve { get; set; }
	}

	[XmlRoot(ElementName="GeoBorderExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class GeoBorderExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="GeoBorderTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class GeoBorderTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="border", Namespace="http://www.aixm.aero/schema/5.1")]
		public Border Border { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="GeoBorder", Namespace="http://www.aixm.aero/schema/5.1")]
	public class GeoBorder {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="theAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TheAirspace {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="AirspaceVolumeDependencyExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class AirspaceVolumeDependencyExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AirspaceVolumeDependency", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirspaceVolumeDependency {
		[XmlElement(ElementName="dependency", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Dependency { get; set; }
		[XmlElement(ElementName="theAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
		public TheAirspace TheAirspace { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="contributorAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
	public class ContributorAirspace {
		[XmlElement(ElementName="AirspaceVolumeDependency", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirspaceVolumeDependency AirspaceVolumeDependency { get; set; }
	}

	[XmlRoot(ElementName="AirspaceVolume", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirspaceVolume {
		[XmlElement(ElementName="contributorAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
		public ContributorAirspace ContributorAirspace { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlElement(ElementName="upperLimit", Namespace="http://www.aixm.aero/schema/5.1")]
		public UpperLimit UpperLimit { get; set; }
		[XmlElement(ElementName="upperLimitReference", Namespace="http://www.aixm.aero/schema/5.1")]
		public string UpperLimitReference { get; set; }
		[XmlElement(ElementName="lowerLimit", Namespace="http://www.aixm.aero/schema/5.1")]
		public LowerLimit LowerLimit { get; set; }
		[XmlElement(ElementName="lowerLimitReference", Namespace="http://www.aixm.aero/schema/5.1")]
		public string LowerLimitReference { get; set; }
		[XmlElement(ElementName="horizontalProjection", Namespace="http://www.aixm.aero/schema/5.1")]
		public HorizontalProjection HorizontalProjection { get; set; }
		[XmlElement(ElementName="minimumLimit", Namespace="http://www.aixm.aero/schema/5.1")]
		public MinimumLimit MinimumLimit { get; set; }
		[XmlElement(ElementName="minimumLimitReference", Namespace="http://www.aixm.aero/schema/5.1")]
		public string MinimumLimitReference { get; set; }
	}

	[XmlRoot(ElementName="theAirspaceVolume", Namespace="http://www.aixm.aero/schema/5.1")]
	public class TheAirspaceVolume {
		[XmlElement(ElementName="AirspaceVolume", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirspaceVolume AirspaceVolume { get; set; }
	}

	[XmlRoot(ElementName="AirspaceGeometryComponent", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirspaceGeometryComponent {
		[XmlElement(ElementName="operation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Operation { get; set; }
		[XmlElement(ElementName="operationSequence", Namespace="http://www.aixm.aero/schema/5.1")]
		public string OperationSequence { get; set; }
		[XmlElement(ElementName="theAirspaceVolume", Namespace="http://www.aixm.aero/schema/5.1")]
		public TheAirspaceVolume TheAirspaceVolume { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="geometryComponent", Namespace="http://www.aixm.aero/schema/5.1")]
	public class GeometryComponent {
		[XmlElement(ElementName="AirspaceGeometryComponent", Namespace="http://www.aixm.aero/schema/5.1")]
		public AirspaceGeometryComponent AirspaceGeometryComponent { get; set; }
	}

	[XmlRoot(ElementName="AirspaceVolumeExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class AirspaceVolumeExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AirspaceExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class AirspaceExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AirspaceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AirspaceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="designator", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Designator { get; set; }
		[XmlElement(ElementName="name", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Name { get; set; }
		[XmlElement(ElementName="geometryComponent", Namespace="http://www.aixm.aero/schema/5.1")]
		public List<GeometryComponent> GeometryComponent { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
		[XmlElement(ElementName="designatorICAO", Namespace="http://www.aixm.aero/schema/5.1")]
		public string DesignatorICAO { get; set; }
		[XmlElement(ElementName="localType", Namespace="http://www.aixm.aero/schema/5.1")]
		public string LocalType { get; set; }
		[XmlElement(ElementName="controlType", Namespace="http://www.aixm.aero/schema/5.1")]
		public string ControlType { get; set; }
	}

	[XmlRoot(ElementName="Airspace", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Airspace {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="upperLimit", Namespace="http://www.aixm.aero/schema/5.1")]
	public class UpperLimit {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="Geodesic", Namespace="http://www.opengis.net/gml/3.2")]
	public class Geodesic {
		[XmlElement(ElementName="pos", Namespace="http://www.opengis.net/gml/3.2")]
		public List<Pos> Pos { get; set; }
		//public List<string> Pos { get; set; }
	}

	[XmlRoot(ElementName="curveMember", Namespace="http://www.opengis.net/gml/3.2")]
	public class CurveMember {
		[XmlElement(ElementName="Curve", Namespace="http://www.aixm.aero/schema/5.1")]
		public Curve Curve { get; set; }
	}

	[XmlRoot(ElementName="Ring", Namespace="http://www.opengis.net/gml/3.2")]
	public class Ring {
		[XmlElement(ElementName="curveMember", Namespace="http://www.opengis.net/gml/3.2")]
		public List<CurveMember> CurveMember { get; set; }
	}

	[XmlRoot(ElementName="exterior", Namespace="http://www.opengis.net/gml/3.2")]
	public class Exterior {
		[XmlElement(ElementName="Ring", Namespace="http://www.opengis.net/gml/3.2")]
		public Ring Ring { get; set; }
	}

	[XmlRoot(ElementName="PolygonPatch", Namespace="http://www.opengis.net/gml/3.2")]
	public class PolygonPatch {
		[XmlElement(ElementName="exterior", Namespace="http://www.opengis.net/gml/3.2")]
		public Exterior Exterior { get; set; }
	}

	[XmlRoot(ElementName="patches", Namespace="http://www.opengis.net/gml/3.2")]
	public class Patches {
		[XmlElement(ElementName="PolygonPatch", Namespace="http://www.opengis.net/gml/3.2")]
		public List<PolygonPatch> PolygonPatch { get; set; }
	}

	[XmlRoot(ElementName="Surface", Namespace="http://www.aixm.aero/schema/5.1")]
	public class Surface {
		[XmlElement(ElementName="patches", Namespace="http://www.opengis.net/gml/3.2")]
		public Patches Patches { get; set; }
		[XmlAttribute(AttributeName="srsDimension")]
		public string SrsDimension { get; set; }
		[XmlAttribute(AttributeName="srsName")]
		public string SrsName { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="horizontalProjection", Namespace="http://www.aixm.aero/schema/5.1")]
	public class HorizontalProjection {
		[XmlElement(ElementName="Surface", Namespace="http://www.aixm.aero/schema/5.1")]
		public Surface Surface { get; set; }
	}

	[XmlRoot(ElementName="minimumLimit", Namespace="http://www.aixm.aero/schema/5.1")]
	public class MinimumLimit {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="pos", Namespace="http://www.opengis.net/gml/3.2")]
	public class Pos {
		[XmlAttribute(AttributeName="srsDimension")]
		public string SrsDimension { get; set; }
		[XmlAttribute(AttributeName="srsName")]
		public string SrsName { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="radius", Namespace="http://www.opengis.net/gml/3.2")]
	public class Radius {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="CircleByCenterPoint", Namespace="http://www.opengis.net/gml/3.2")]
	public class CircleByCenterPoint {
		[XmlElement(ElementName="pos", Namespace="http://www.opengis.net/gml/3.2")]
		public Pos Pos { get; set; }
		[XmlElement(ElementName="radius", Namespace="http://www.opengis.net/gml/3.2")]
		public Radius Radius { get; set; }
		[XmlAttribute(AttributeName="numArc")]
		public string NumArc { get; set; }
	}

	[XmlRoot(ElementName="startAngle", Namespace="http://www.opengis.net/gml/3.2")]
	public class StartAngle {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="endAngle", Namespace="http://www.opengis.net/gml/3.2")]
	public class EndAngle {
		[XmlAttribute(AttributeName="uom")]
		public string Uom { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="ArcByCenterPoint", Namespace="http://www.opengis.net/gml/3.2")]
	public class ArcByCenterPoint {
		[XmlElement(ElementName="pos", Namespace="http://www.opengis.net/gml/3.2")]
		public Pos Pos { get; set; }
		[XmlElement(ElementName="radius", Namespace="http://www.opengis.net/gml/3.2")]
		public Radius Radius { get; set; }
		[XmlElement(ElementName="startAngle", Namespace="http://www.opengis.net/gml/3.2")]
		public StartAngle StartAngle { get; set; }
		[XmlElement(ElementName="endAngle", Namespace="http://www.opengis.net/gml/3.2")]
		public EndAngle EndAngle { get; set; }
		[XmlAttribute(AttributeName="numArc")]
		public string NumArc { get; set; }
	}

	[XmlRoot(ElementName="assignedAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AssignedAirspace {
		[XmlAttribute(AttributeName="href", Namespace="http://www.w3.org/1999/xlink")]
		public string Href { get; set; }
	}

	[XmlRoot(ElementName="AuthorityForAirspaceExtension", Namespace="http://www.idscompany/schema/5.1")]
	public class AuthorityForAirspaceExtension {
		[XmlElement(ElementName="theAeroDBPk", Namespace="http://www.idscompany/schema/5.1")]
		public TheAeroDBPk TheAeroDBPk { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AuthorityForAirspaceTimeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AuthorityForAirspaceTimeSlice {
		[XmlElement(ElementName="validTime", Namespace="http://www.opengis.net/gml/3.2")]
		public ValidTime ValidTime { get; set; }
		[XmlElement(ElementName="interpretation", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Interpretation { get; set; }
		[XmlElement(ElementName="sequenceNumber", Namespace="http://www.aixm.aero/schema/5.1")]
		public string SequenceNumber { get; set; }
		[XmlElement(ElementName="featureLifetime", Namespace="http://www.aixm.aero/schema/5.1")]
		public FeatureLifetime FeatureLifetime { get; set; }
		[XmlElement(ElementName="type", Namespace="http://www.aixm.aero/schema/5.1")]
		public string Type { get; set; }
		[XmlElement(ElementName="responsibleOrganisation", Namespace="http://www.aixm.aero/schema/5.1")]
		public ResponsibleOrganisation ResponsibleOrganisation { get; set; }
		[XmlElement(ElementName="assignedAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
		public AssignedAirspace AssignedAirspace { get; set; }
		[XmlElement(ElementName="extension", Namespace="http://www.aixm.aero/schema/5.1")]
		public Extension Extension { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AuthorityForAirspace", Namespace="http://www.aixm.aero/schema/5.1")]
	public class AuthorityForAirspace {
		[XmlElement(ElementName="identifier", Namespace="http://www.opengis.net/gml/3.2")]
		public Identifier Identifier { get; set; }
		[XmlElement(ElementName="timeSlice", Namespace="http://www.aixm.aero/schema/5.1")]
		public TimeSlice TimeSlice { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

	[XmlRoot(ElementName="AIXMBasicMessage", Namespace="http://www.aixm.aero/schema/5.1/message")]
	public class AIXMBasicMessage {
		[XmlElement(ElementName="hasMember", Namespace="http://www.aixm.aero/schema/5.1/message")]
		public List<HasMember> HasMember { get; set; }
		[XmlAttribute(AttributeName="aixm", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Aixm { get; set; }
		[XmlAttribute(AttributeName="gco", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Gco { get; set; }
		[XmlAttribute(AttributeName="gmd", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Gmd { get; set; }
		[XmlAttribute(AttributeName="gml", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Gml { get; set; }
		[XmlAttribute(AttributeName="ids", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Ids { get; set; }
		[XmlAttribute(AttributeName="message", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Message { get; set; }
		[XmlAttribute(AttributeName="xlink", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Xlink { get; set; }
		[XmlAttribute(AttributeName="xsi", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName="id", Namespace="http://www.opengis.net/gml/3.2")]
		public string Id { get; set; }
	}

}
