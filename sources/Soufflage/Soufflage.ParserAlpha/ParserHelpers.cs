﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Soufflage.Core;using Soufflage.Core.Entities;
using Soufflage.Core.Utilities;
using Xml2CSharp;

namespace Soufflage.ParserAlpha
{
    public static class ParserHelpers
    {
        private static readonly Dictionary<string, Func<double, double>> UnitConverters = new Dictionary<string, Func<double, double>>
        {
            /* default */

            ["DEFAULT"] = (x) => x,

            /* distance to meters */

            // meter
            ["M"] = (x) => x,
            // foot
            ["FT"] = (x) => x*0.3048,
            // flight level
            ["FL"] = (x) => x*0.3048*100,
            // kilometer
            ["KM"] = (x) => x*1000,
            // nautical mile
            ["NM"] = (x) => x*1852,
            // international mile
            ["MI"] = (x) => x*1.609344*1000,
            // centimeter
            ["CM"] = (x) => x/1000.0,

            /* temperature to celsius */

            // celsius
            ["C"] = (x) => x,
            // kelvin
            ["K"] = (x) => x - 272.15,
            // fahrenheit
            ["F"] = (x) => (x-32)/1.8,

            /* angle to degrees */

            ["DEG"] = (x) => x,

            ["RAD"] = (x) => x.ToDegrees(),

            /* frequency to khz */

            ["HZ"] = (x) => x/1000.0,
            ["KHZ"] = (x) => x,
            ["MHZ"] = (x) => x*1000.0,
        };

        #region ActualUnitConverters

        public static double ToMeters(this Radius r) => UnitConverters.FailSafeWithKey(r.Uom, "DEFAULT").Invoke(r.Text.AsDouble());

        public static double ToMeters(this LowerLimit r) => UnitConverters.FailSafeWithKey(r.Uom, "DEFAULT").Invoke(r.Text.AsDouble());

        public static double ToMeters(this UpperLimit r) => UnitConverters.FailSafeWithKey(r.Uom, "DEFAULT").Invoke(r.Text.AsDouble());

        public static double ToMeters(this FieldElevation r) => UnitConverters.FailSafeWithKey(r.Uom, "DEFAULT").Invoke(r.Text.AsDouble());

        public static double ToMeters(this Elevation r) => UnitConverters.FailSafeWithKey(r.Uom, "DEFAULT").Invoke(r.Text.AsDouble());

        #endregion ActualUnitConverters

        #region CoordinateConverters

        public static GeoCoordinate ToGeoCoordinate(this ElevatedPoint p)
        {
            var pos = new Pos
            {
                Text = p.Pos
            };
            return pos.ToGeoCoordinate();
        }

        public static GeoCoordinate ToGeoCoordinate(this Pos p)
        {
            var s = p.Text.Split(' ');
            return new GeoCoordinate
            {
                Latitude = s[0].AsDouble(),
                Longitude = s[1].AsDouble()
            };
        }

        public static IEnumerable<GeoCoordinate> ToGeoCoordinates(this Geodesic g)
        {
            return g.Pos.Select(x => x.ToGeoCoordinate()).ToList();
        }

        public static IEnumerable<GeoCoordinate> ToGeoCoordinates(this GeodesicString g)
        {
            var gcpsp = g.PosList.Split(' ');
            var l = new List<GeoCoordinate>();
            for (var i = 0; i < gcpsp.Length - 1; i += 2)
            {
                l.Add(new GeoCoordinate
                {
                    Latitude = gcpsp[i].AsDouble(),
                    Longitude = gcpsp[i+1].AsDouble()
                });
            }
            return l;
        }

        #endregion CoordinateConverters

        #region LimitConverters

        private static readonly Dictionary<string, string> CodeVerticalReferenceTypes = new Dictionary<string, string>
        {
            ["OTHER"] = "OTHER - Other.",
            ["MSL"] = "MSL - The distance measured from mean sea level (equivalent to altitude).",
            ["SFC"] = "SFC - The distance measured from the surface of the Earth (equivalent to AGL - Above Ground Level).",
            ["STD"] = "STD - The vertical distance is measured with an altimeter set to the standard atmosphere.",
            ["W84"] = "W84 - The distance measured from the WGS84 ellipsoid.",
        };

        public static Height ToHeightLimits(this AirspaceVolume v)
        {
            var h = new Height();
            if (v.LowerLimit != null)
            {
                h.LowerLimit = v.LowerLimit.ToMeters();
                if (!string.IsNullOrWhiteSpace(v.LowerLimitReference)) v.LowerLimitReference = CodeVerticalReferenceTypes.FailSafeWithKey(v.LowerLimitReference, "OTHER");
            }
            if (v.UpperLimit != null)
            {
                h.UpperLimit = v.UpperLimit.ToMeters();
                if (!string.IsNullOrWhiteSpace(v.UpperLimitReference)) v.UpperLimitReference = CodeVerticalReferenceTypes.FailSafeWithKey(v.UpperLimitReference, "OTHER");
            }
            return h;
        }

        #endregion LimitConverters

        #region ShapeConverters

        public static double CalculateArcSize(double start, double end)
        {
            // if the begin angle is smaller than the end angle, we go CW -> this is used in this calculation as well
            // as specified by 'Guidance and Profile of GML for use with Aviation Data - OGC Portal'
            // see pdf in ~/docs/ (page 26)
            return start > end
                ? start.NormalizeDegreeInRange0To360() - end.NormalizeDegreeInRange0To360()
                : 360.0 - (start.NormalizeDegreeInRange0To360() - end.NormalizeDegreeInRange0To360());
        }

        #endregion ShapeConverters
    }
}
