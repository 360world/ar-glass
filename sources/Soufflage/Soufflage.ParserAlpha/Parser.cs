﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Soufflage.Core;
using Soufflage.Core.Entities;
using Soufflage.Core.Geography;
using Soufflage.Core.Utilities;
using Xml2CSharp;
using Point = Soufflage.Core.Entities.Point;

namespace Soufflage.ParserAlpha
{
    public class Parser : IParser
    {
        public IEnumerable<string> SupportedFormats => new List<string> {"aixm 5.1"};

        public ParseResult Parse(IEnumerable<Stream> sources)
        {
            var log = new ObservableCollection<LogEntry>();
            log.CollectionChanged += Log_CollectionChanged;
            var groups = new Dictionary<string, Group>();
            foreach (var source in sources)
            {
                log.Add(new LogEntry(EntryType.Info, "opening new source", "file"));
                try
                {
                    var serializer = new XmlSerializer(typeof(AIXMBasicMessage));

                    /* #################################################################################### */
                    /* ######################## 1st pass: collect relevant objects ######################## */
                    /* #################################################################################### */

                    log.Add(new LogEntry(EntryType.Info, "starting 1st pass", "state"));

                    var airspaces = new Dictionary<string, Airspace>();
                    var airspacesActual = new List<string>();

                    using (var reader = XmlReader.Create(source))
                    {
                        var msg = (AIXMBasicMessage)serializer.Deserialize(reader);
                        log.Add(new LogEntry(EntryType.Info, "successfully parsed aixm file"));
                        foreach (var hasMember in msg.HasMember)
                        {
                            /* ######################## airspaces ######################## */

                            if (hasMember.Airspace != null)
                            {
                                var ai = hasMember.Airspace.Identifier.Text;
                                airspaces.Add(ai, hasMember.Airspace);
                                log.Add(new LogEntry(EntryType.Info, $"found {ai}", "airspace"));
                                var ts = hasMember.Airspace.TimeSlice.AirspaceTimeSlice;
                                if (ts.GeometryComponent != null
                                    && ts.GeometryComponent.Any(x => x.AirspaceGeometryComponent.TheAirspaceVolume.AirspaceVolume.HorizontalProjection != null))
                                {
                                    log.Add(new LogEntry(EntryType.Info, $"{ai} has actual volumetric information, storing in airspaces actual", "airspace"));
                                    airspacesActual.Add(hasMember.Airspace.Identifier.Text);
                                }
                            }

                            /* ######################## airports and heliports ######################## */

                            else if (hasMember.AirportHeliport != null)
                            {
                                var ahi = hasMember.AirportHeliport.Identifier.Text;
                                log.Add(new LogEntry(EntryType.Info, $"found {ahi}", "airportheliport"));
                                var ts = hasMember.AirportHeliport.TimeSlice.AirportHeliportTimeSlice;
                                if (ts.ARP?.ElevatedPoint == null || ts.FieldElevation == null) continue;
                                var gc = ts.ARP.ElevatedPoint.ToGeoCoordinate();
                                var elevation = ts.FieldElevation.ToMeters();
                                // ReSharper disable RedundantVerbatimPrefix
                                var @group = new Group
                                {
                                    GroupFor = GroupFor.AirportHeliport,
                                    Identifier = ahi,
                                    LocalType = ts.Type,
                                    FriendlyName = $"{ahi} - {ts.Designator} ({ts.Name})",
                                    Shapes = new List<Shape>
                                {
                                    new Point
                                    {
                                        Height = new Height
                                        {
                                            UpperLimit = elevation,
                                            UpperLimitReference = "EGM_96"
                                        },
                                        Location = gc
                                    }
                                }
                                };
                                log.Add(new LogEntry(EntryType.Info, $"{ahi} built", "airportheliport"));
                                // add to groups
                                groups[@group.Identifier] = @group;
                                // ReSharper restore RedundantVerbatimPrefix
                            }

                            /* ######################## ground obstacles ######################## */

                            else if (hasMember.VerticalStructure != null)
                            {
                                var vi = hasMember.VerticalStructure.Identifier.Text;
                                log.Add(new LogEntry(EntryType.Info, $"found {vi}", "verticalstructure"));
                                var ts = hasMember.VerticalStructure.TimeSlice.VerticalStructureTimeSlice;
                                if (ts.Part.VerticalStructurePart == null || !ts.Part.VerticalStructurePart.Any()) continue;
                                var group = new Group
                                {
                                    GroupFor = GroupFor.GroundObstacle,
                                    Identifier = vi,
                                    LocalType = ts.Type,
                                    FriendlyName = $"{vi} - {ts.Name}",
                                    Shapes = new List<Shape>()
                                };
                                foreach (var verticalStructurePart in ts.Part.VerticalStructurePart)
                                {
                                    var gc = verticalStructurePart.HorizontalProjection_location.ElevatedPoint.ToGeoCoordinate();
                                    var elevation = verticalStructurePart.HorizontalProjection_location.ElevatedPoint.Elevation.ToMeters();
                                    group.Shapes.Add(new Point
                                    {
                                        Height = new Height
                                        {
                                            UpperLimit = elevation,
                                            UpperLimitReference = verticalStructurePart.HorizontalProjection_location.ElevatedPoint.VerticalDatum
                                        },
                                        Location = gc
                                    });
                                }
                                log.Add(new LogEntry(EntryType.Info, $"{vi} built", "verticalstructure"));
                                // add to groups
                                groups[@group.Identifier] = @group;
                            }

                            //TODO add other object types later when supported
                        }
                    }

                    log.Add(new LogEntry(EntryType.Info, "1st pass done", "state"));

                    /* ############################################################################################################################################### */
                    /* ######################## 2nd pass: now that all possible cross-references are available we can build the final objects ######################## */
                    /* ############################################################################################################################################### */

                    log.Add(new LogEntry(EntryType.Info, "starting 2nd pass", "state"));

                    /* ######################## airspaces ######################## */

                    // # airspaces with geometry
                    foreach (var aaid in airspacesActual)
                    {
                        var current = airspaces[aaid].TimeSlice.AirspaceTimeSlice;
                        if (current.GeometryComponent.Count != 1)
                        {
                            log.Add(new LogEntry(EntryType.Warning,
                                $"actual {aaid} has more than 1 geometry component defined and this is currently not supported, skipping...",
                                "airspace"));
                            continue;
                        }
                        var airspaceVolume = current.GeometryComponent[0].AirspaceGeometryComponent.TheAirspaceVolume.AirspaceVolume;
                        var group = new Group
                        {
                            GroupFor = GroupFor.Airspace,
                            Identifier = aaid,
                            LocalType = current.LocalType,
                            FriendlyName = $"{aaid} - {current.Designator}"
                        };
                        var segmentElements = airspaceVolume.HorizontalProjection.Surface.Patches.PolygonPatch[0].Exterior.Ring.CurveMember[0].Curve.Segments.SegmentElements;
                        if (segmentElements == null) continue;
                        var l = new List<GeoCoordinate>();

                        // type 1: circle by centerpoint
                        if (segmentElements.Length == 1 && segmentElements[0] is CircleByCenterPoint)
                        {
                            var cbcp = (CircleByCenterPoint)segmentElements[0];
                            var radius = cbcp.Radius.ToMeters();
                            var gc = cbcp.Pos.ToGeoCoordinate();
                            var a = new Accuracy(radius);
                            for (var i = 0; i < a.CircularPointCount(); i++)
                            {
                                l.Add(gc.CalculateDestinationPoint(radius, i * a.AnglePerIteration()));
                            }
                            // check for redundant points
                            log.Add(new LogEntry(EntryType.Info, $"{aaid} checked for redundancies", "airspace"));
                            for (var i = l.Count - 1; i > 0; i--)
                            {
                                if (EntityHelpers.ApproximatelyEquals(l[i], l[i - 1])) l.RemoveAt(i);
                            }
                            if (EntityHelpers.ApproximatelyEquals(l[0], l[l.Count - 1])) l.RemoveAt(l.Count - 1);
                            group.Shapes.Add(new StaticPolygon
                            {
                                ExteriorBoundary = l.Select(x => new Point
                                {
                                    Location = x
                                }).ToList(),
                                UseGlobalHeightOverride = true,
                                GlobalHeightOverride = airspaceVolume.ToHeightLimits()
                            });
                            //TODO description actual
                            group.GenerateDescription();
                            log.Add(new LogEntry(EntryType.Info, $"{aaid} built with circle by centerpoint algorithm", "airspace"));
                        }

                        // type 2: geodesic / geodesic string / arc by centerpoint
                        else
                        {
                            log.Add(new LogEntry(EntryType.Info, $"{aaid} building started", "airspace"));
                            foreach (var segmentElement in segmentElements)
                            {
                                segmentElement.Switch<ArcByCenterPoint, GeodesicString, Geodesic>(
                                        arcByCenterPoint =>
                                        {
                                            var radius = arcByCenterPoint.Radius.ToMeters();
                                            var gc = arcByCenterPoint.Pos.ToGeoCoordinate();
                                            var a = new Accuracy(radius);
                                            var angleStart = arcByCenterPoint.StartAngle.Text.AsDouble();
                                            var angleEnd = arcByCenterPoint.EndAngle.Text.AsDouble();
                                            var angleDiff = ParserHelpers.CalculateArcSize(angleStart, angleEnd);
                                            // if the begin angle is smaller than the end angle, we go CW
                                            // as specified by 'Guidance and Profile of GML for use with Aviation Data - OGC Portal'
                                            // see pdf in ~/docs/ (page 26)
                                            var addModifier = angleStart < angleEnd ? 1 : -1;
                                            for (var i = 0; i < a.ArcPointCount(angleDiff); i++)
                                            {
                                                l.Add(gc.CalculateDestinationPoint(radius, angleStart + (addModifier * i * a.AnglePerIteration())));
                                            }
                                            l.Add(gc.CalculateDestinationPoint(radius, angleEnd));
                                            log.Add(new LogEntry(EntryType.Info, $"{aaid} segment built with arc by centerpoint algorithm", "airspace"));
                                        },
                                        geodesicString =>
                                        {
                                            l.AddRange(geodesicString.ToGeoCoordinates());
                                            log.Add(new LogEntry(EntryType.Info, $"{aaid} segment built with geodesic string", "airspace"));
                                        },
                                        geodesic =>
                                        {
                                            l.AddRange(geodesic.ToGeoCoordinates());
                                            log.Add(new LogEntry(EntryType.Info, $"{aaid} segment built with geodesic", "airspace"));
                                        });
                            }
                            log.Add(new LogEntry(EntryType.Info, $"{aaid} segments completed", "airspace"));
                            // check for redundant points
                            log.Add(new LogEntry(EntryType.Info, $"{aaid} checked for redundancies", "airspace"));
                            for (var i = l.Count - 1; i > 0; i--)
                            {
                                if (EntityHelpers.ApproximatelyEquals(l[i], l[i - 1])) l.RemoveAt(i);
                            }
                            if (EntityHelpers.ApproximatelyEquals(l[0], l[l.Count - 1])) l.RemoveAt(l.Count - 1);
                            log.Add(new LogEntry(EntryType.Info, $"{aaid} built", "airspace"));
                            group.Shapes.Add(new StaticPolygon
                            {
                                ExteriorBoundary = l.Select(x => new Point
                                {
                                    Location = x
                                }).ToList(),
                                UseGlobalHeightOverride = true,
                                GlobalHeightOverride = airspaceVolume.ToHeightLimits()
                            });
                            //TODO description actual
                            group.GenerateDescription();
                        }

                        // add to group dict
                        groups[group.Identifier] = group;
                    }

                    log.Add(new LogEntry(EntryType.Info, "skipping dynamic polygons...", "state"));

                    /*

                    // # airspaces defined with references

                    // generational resolving to avoid recursive parsing - probably a bit slower, but the code should be much nicer
                    var todoAirspaces = airspaces.Keys.Where(x => !groups.ContainsKey(x)).ToList();
                    var currentGeneration = new List<string>();

                    while (todoAirspaces.Any())
                    {
                        currentGeneration.Clear();
                        foreach (var aaid in todoAirspaces)
                        {
                            var current = airspaces[aaid].TimeSlice.AirspaceTimeSlice;
                            var parts = current.GeometryComponent
                                .Select(x => x.AirspaceGeometryComponent.TheAirspaceVolume.AirspaceVolume.ContributorAirspace
                                                .AirspaceVolumeDependency.TheAirspace.Href.Replace("#", ""));
                            if(parts.Any(x => !groups.ContainsKey(x))) continue;
                            currentGeneration.Add(aaid);
                        }

                        foreach (var aaid in currentGeneration)
                        {
                            var current = airspaces[aaid].TimeSlice.AirspaceTimeSlice;
                            var baseAirspace = current.GeometryComponent.Single(x => x.AirspaceGeometryComponent.Operation == "BASE");
                            var baseGroup = groups[baseAirspace.AirspaceGeometryComponent.TheAirspaceVolume
                                .AirspaceVolume.ContributorAirspace
                                .AirspaceVolumeDependency.TheAirspace.Href.Replace("#", "")];
                            var resultShape = (Polygon)baseGroup.Shapes[0];
                            // here we go with the assumption that every airspace is a single polygon
                            // (which seems to be true based on the sample aixm files I have, and the code I've written above)
                            foreach (var airspacePart in current.GeometryComponent
                                .Where(x => x.AirspaceGeometryComponent.Operation != "BASE")
                                .OrderBy(y => y.AirspaceGeometryComponent.OperationSequence))
                                {
                                    var partGroup = groups[airspacePart.AirspaceGeometryComponent.TheAirspaceVolume
                                        .AirspaceVolume.ContributorAirspace
                                        .AirspaceVolumeDependency.TheAirspace.Href.Replace("#", "")];
                                    switch (airspacePart.AirspaceGeometryComponent.Operation)
                                    {
                                        case "UNION":
                                            resultShape = new Polygon
                                            {
                                                ExteriorBoundary = resultShape.ExteriorBoundary.ToPolygon()
                                                    .UnionWithClipper(((Polygon) partGroup.Shapes[0]).ExteriorBoundary.ToPolygon())
                                                    .ToGeoCoordinates(),
                                            };
                                            break;
                                        case "INTERS":
                                            break;
                                        case "SUBTR":
                                            break;
                                }
                                    //todo clipper
                                }

                            todoAirspaces.Remove(aaid);
                        }
                    }

                    */

                    log.Add(new LogEntry(EntryType.Info, "2nd pass done", "state"));

                    log.Add(new LogEntry(EntryType.Info, "finished source", "file"));
                }
                catch (Exception e)
                {
                    log.Add(new LogEntry(EntryType.Error, e.ToString()));
                    return new ParseResult(log, "Unknown error.");
                }
            }
            return new ParseResult(groups.Values.ToList(), log, null);
        }

        private void Log_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnLogEntry?.Invoke(this, new LogEntryEventArgs
            {
                Entry = (LogEntry)e.NewItems[0]
            });
        }

        public event EventHandler<LogEntryEventArgs> OnLogEntry;
    }
}
