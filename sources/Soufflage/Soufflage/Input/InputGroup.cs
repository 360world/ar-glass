﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Soufflage.Core.Entities;

namespace Soufflage.Input
{
    public class InputGroup
    {
        public List<SourceDescription> Sources { get; set; }

        public GeoCoordinate CameraPosition { get; set; }

        public double CameraHeight { get; set; }

        public bool IsCameraFirstPerson { get; set; }

        public GeoCoordinate CenterPoint { get; set; }

        public double IncludeRadius { get; set; }

        public List<Predicate<Group>> Ignores { get; set; }
    }
}
