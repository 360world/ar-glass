﻿namespace Soufflage.Input
{
    public class SourceDescription
    {
        public string Path { get; set; }

        public string Format { get; set; }
    }
}
