﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Soufflage.Converters;
using Soufflage.Core;
using Soufflage.Core.Entities;
using Soufflage.Core.Geography;
using Soufflage.Core.Utilities;
using Soufflage.Input;
using Soufflage.ParserAlpha;
using Soufflage.ParserRsys;

namespace Soufflage
{
    class Program
    {
        static void Main(string[] args)
        {
            //set up some constants
            const double additionalTerrain = 10.0*1000.0;

            // set up parsers and logging
            _sb = new StringBuilder();
            var parsers = new Dictionary<string, IParser>();
            var aixmParser = new Parser();
            aixmParser.OnLogEntry += OnLogEntry;
            foreach (var supportedFormat in aixmParser.SupportedFormats) parsers.Add(supportedFormat, aixmParser);
            var rsysFlightParser = new FlightParser();
            rsysFlightParser.OnLogEntry += OnLogEntry;
            foreach (var supportedFormat in rsysFlightParser.SupportedFormats) parsers.Add(supportedFormat, rsysFlightParser);
            var rsysAreasParser = new AreasParser();
            rsysAreasParser.OnLogEntry += OnLogEntry;
            foreach (var supportedFormat in rsysAreasParser.SupportedFormats) parsers.Add(supportedFormat, rsysAreasParser);

            // set up our sources
            var sourceDict = new Dictionary<string, InputGroup>
            {
                ["hc1"] = new InputGroup
                {
                    Sources = new List<SourceDescription>
                    {
                        new SourceDescription
                        {
                            Path = @"D:\CurrentMockings\aixm\HC_AIXM_5_1.xml",
                            Format = "aixm 5.1"
                        },
                        new SourceDescription
                        {
                            Path = @"D:\CurrentMockings\aixm2\OBST.xml",
                            Format = "aixm 5.1"
                        },
                    },
                    CameraHeight = 224.0,
                    CameraPosition = new GeoCoordinate(47.4836337, 19.0151144),
                    CenterPoint = new GeoCoordinate(47.4333, 19.25),
                    IncludeRadius = 30.0 * 1000.0,
                    IsCameraFirstPerson = true,
                    Ignores = new List<Predicate<Group>>
                    {
                        (x) => x.GroupFor == GroupFor.GroundObstacle && x.FriendlyName.ToUpper().Contains("LHBP_AREA")
                    }
                } ,
                ["rsys1"] = new InputGroup
                {
                    Sources = new List<SourceDescription>
                    {
                        new SourceDescription
                        {
                            Path = @"D:\CurrentMockings\slo_file\areas.json",
                            Format = "r-sys areas json"
                        },
                        new SourceDescription
                        {
                            Path = @"D:\CurrentMockings\slo_file\flight.json",
                            Format = "r-sys flight json"
                        },
                    },
                    IsCameraFirstPerson = false,
                    CenterPoint = new GeoCoordinate(48.231400, 17.571028),
                    Ignores = new List<Predicate<Group>>(),
                    IncludeRadius = -1.0
                }
            };

            // choose input source
            const string sourceKey = "rsys1";

            // parse files
            var sourceSet = sourceDict[sourceKey];
            var resultGroups = new List<Group>();
            foreach (var sourceGroup in sourceSet.Sources.GroupBy(x => x.Format))
            {
                var result = parsers[sourceGroup.Key].Parse(sourceGroup.Select(x => x.Path).Select(File.OpenRead).ToList());
                resultGroups.AddRange(result.Groups);
            }

            // print totals
            Log();
            Log(" Totals by group:");
            Log();
            foreach (var gc in resultGroups.GroupBy(x => x.GroupFor).Select(y => new { Type = y.Key, Count = y.Count()}))
            {
                Log($"  {gc.Type.ToString().PadRight(20)}{gc.Count.ToString().PadLeft(5)}");
            }
            Log();

            // set centerpoint
            var centerpoint = sourceSet.CenterPoint;

            // filter results
            foreach (var resultGroup in resultGroups)
            {
                var ignoreDistance = !(sourceSet.IncludeRadius < 0)
                                     && !resultGroup.Shapes.SelectMany(x => x.GetAllCoordinates()).Any(y => y.CalculateDistanceBetween(centerpoint) < sourceSet.IncludeRadius);
                resultGroup.Ignore = ignoreDistance || sourceSet.Ignores.Select(x => x.Invoke(resultGroup)).Any(x => x);
            }

            // find terrain edges
            var ac = resultGroups.Where(x => !x.Ignore).SelectMany(y => y.Shapes).SelectMany(z => z.GetAllCoordinates()).ToList();
            var northernmost = ac.Max(z => z.Latitude);
            var southernmost = ac.Min(z => z.Latitude);
            var easternmost = ac.Max(z => z.Longitude);
            var westernmost = ac.Min(z => z.Longitude);
            var topLeftPre = new GeoCoordinate(northernmost, westernmost);
            var bottomRightPre = new GeoCoordinate(southernmost, easternmost);
            var topLeft = topLeftPre.CalculateDestinationPoint(additionalTerrain*Math.Sqrt(2), 315.0);
            var bottomRight = bottomRightPre.CalculateDestinationPoint(additionalTerrain*Math.Sqrt(2), 135.0);

            // Wright export
            var converter = new DistortedFlattenAroundCenterpoint(centerpoint);
            var encoded = JsonConvert.SerializeObject(new ResultWrapper
            {
                Groups = resultGroups,
                TerrainTopLeftPosition = converter.Convert(topLeft),
                TerrainBottomRightPosition = converter.Convert(bottomRight),
                IsCameraFirstPerson = sourceSet.IsCameraFirstPerson,
                CameraPosition = sourceSet.IsCameraFirstPerson ? converter.Convert(sourceSet.CameraPosition) : null,
                CameraHeight = sourceSet.IsCameraFirstPerson ? default(double) : sourceSet.CameraHeight
            }, new JsonSerializerSettings
            {
                Converters = new List<JsonConverter>
                {
                    converter,
                    new StringEnumConverter()
                },
                NullValueHandling = NullValueHandling.Ignore,
            });

            File.WriteAllText($@"D:\soufflage_result_{sourceKey}.json", encoded);
            Log();
            Log($"  Wright serialization completed, {AbstractGeoCoordinateConverter.Counter} coordinates converted with {converter} successfully.");

            // .kml export
            var kmlSb = new StringBuilder();
            kmlSb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            kmlSb.AppendLine("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
            kmlSb.AppendLine("<Document>");
            kmlSb.AppendLine($"<name>{sourceKey}</name>");
            kmlSb.AppendLine("<open>1</open>");
            kmlSb.AppendLine($"<description>Soufflage .kml export from source key {sourceKey} at {DateTime.Now:f}</description>");
            foreach (var groupCat in resultGroups.GroupBy(x => x.GroupFor))
            {
                kmlSb.AppendLine("<Folder>");
                kmlSb.AppendLine($"<name>{groupCat.Key}</name>");
                kmlSb.AppendLine($"<description>{groupCat.Count()} item(s) total</description>");
                foreach (var group in groupCat)
                {
                    foreach (var groupShape in group.Shapes)
                    {
                        groupShape.Switch<Core.Entities.Point, Polyline, StaticPolygon>(
                            point =>
                            {
                                kmlSb.AppendLine("<Placemark>");
                                kmlSb.AppendLine($"<name>{group.FriendlyName}</name>");
                                kmlSb.AppendLine("<Point>");
                                if (point.Height?.UpperLimit != null)
                                {
                                    kmlSb.AppendLine("<extrude>1</extrude>");
                                    kmlSb.AppendLine("<altitudeMode>absolute</altitudeMode>");
                                    kmlSb.AppendLine($"<coordinates>{point.Location.Longitude},{point.Location.Latitude},{point.Height.UpperLimit}</coordinates>");
                                }
                                else
                                {
                                    kmlSb.AppendLine($"<coordinates>{point.Location.Longitude},{point.Location.Latitude},0</coordinates>");
                                }
                                kmlSb.AppendLine("</Point>");
                                kmlSb.AppendLine("</Placemark>");
                            },
                            polyline =>
                            {
                                foreach (var polylineLocation in polyline.Locations)
                                {
                                    kmlSb.AppendLine("<Placemark>");
                                    kmlSb.AppendLine($"<name>{polylineLocation.ShapeFriendlyName}</name>");
                                    kmlSb.AppendLine("<Point>");
                                    if (polylineLocation.Height.UpperLimit != null || polyline.UseGlobalHeightOverride)
                                    {
                                        kmlSb.AppendLine("<extrude>1</extrude>");
                                        kmlSb.AppendLine("<altitudeMode>absolute</altitudeMode>");
                                        if (polyline.UseGlobalHeightOverride)
                                        {
                                            kmlSb.AppendLine($"<coordinates>{polylineLocation.Location.Longitude},{polylineLocation.Location.Latitude},{polyline.GlobalHeightOverride.UpperLimit}</coordinates>");
                                        }
                                        else
                                        {
                                            kmlSb.AppendLine($"<coordinates>{polylineLocation.Location.Longitude},{polylineLocation.Location.Latitude},{polylineLocation.Height.UpperLimit}</coordinates>");
                                        }
                                    }
                                    else
                                    {
                                        kmlSb.AppendLine($"<coordinates>{polylineLocation.Location.Longitude},{polylineLocation.Location.Latitude},0</coordinates>");
                                    }
                                    kmlSb.AppendLine("</Point>");
                                    kmlSb.AppendLine("</Placemark>");
                                }
                                kmlSb.AppendLine("<Placemark>");
                                kmlSb.AppendLine($"<name>{group.FriendlyName}</name>");
                                kmlSb.AppendLine("<LineString>");
                                kmlSb.AppendLine("<extrude>1</extrude>");
                                kmlSb.AppendLine("<tesselate>1</tesselate>");
                                kmlSb.AppendLine("<altitudeMode>absolute</altitudeMode>");
                                kmlSb.Append("<coordinates>");
                                foreach (var point in polyline.Locations)
                                {
                                    var h = polyline.UseGlobalHeightOverride
                                        ? polyline.GlobalHeightOverride
                                        : point.Height;
                                    kmlSb.AppendLine($"{point.Location.Longitude},{point.Location.Latitude},{h.UpperLimit ?? 0.0}");
                                }
                                kmlSb.Append("</coordinates>");
                                kmlSb.AppendLine("</LineString>");
                                kmlSb.AppendLine("</Placemark>");
                            },
                            staticpolygon =>
                            {
                                kmlSb.AppendLine("<Placemark>");
                                kmlSb.AppendLine($"<name>{group.FriendlyName}</name>");
                                kmlSb.AppendLine("<Polygon>");
                                kmlSb.AppendLine("<extrude>1</extrude>");
                                kmlSb.AppendLine("<altitudeMode>absolute</altitudeMode>");
                                var lower = staticpolygon.GlobalHeightOverride.LowerLimit ?? 0.0;
                                var upperPre = staticpolygon.GlobalHeightOverride.UpperLimit ?? 15.0 * 1000.0;
                                var upper = upperPre - lower;
                                kmlSb.AppendLine($"<altitude>{lower}</altitude>");
                                kmlSb.AppendLine("<outerBoundaryIs>");
                                kmlSb.AppendLine("<LinearRing>");
                                kmlSb.Append("<coordinates>");
                                foreach (var point in staticpolygon.ExteriorBoundary)
                                {
                                    kmlSb.AppendLine($"{point.Location.Longitude},{point.Location.Latitude},{upper}");
                                }
                                kmlSb.Append("</coordinates>");
                                kmlSb.AppendLine("</LinearRing>");
                                kmlSb.AppendLine("</outerBoundaryIs>");
                                //todo inner boundary... (not used)
                                kmlSb.AppendLine("</Polygon>");
                                kmlSb.AppendLine("</Placemark>");
                            }
                            );
                    }
                }
                kmlSb.AppendLine("</Folder>");
            }
            kmlSb.AppendLine("</Document>");
            kmlSb.AppendLine("</kml>");

            File.WriteAllText($@"D:\soufflage_result_{sourceKey}.kml", kmlSb.ToString());
            Log();
            Log($"  Kml serialization completed, {resultGroups.SelectMany(x => x.Shapes).SelectMany(y => y.GetAllCoordinates()).Count()} coordinates converted successfully.");

            // finalize

            Log();
            Log("  RWTH output:");
            Log($"  <Coords tlx=\"{topLeft.Longitude}\" tly=\"{topLeft.Latitude}\" brx=\"{bottomRight.Longitude}\" bry=\"{bottomRight.Latitude}\"/>");

            File.WriteAllText($@"D:\soufflage_log_{DateTime.Now:yy-MM-dd_hh-mm}.txt", _sb.ToString());

            Console.ReadKey();
        }

        private static void OnLogEntry(object sender, LogEntryEventArgs e)
        {
            var resultLogEntry = e.Entry;
            var s = $"#{_logCount}. {resultLogEntry.Type} | {resultLogEntry.Category} | {resultLogEntry.Msg}";
            Log(s);
            _logCount += 1;
        }

        private static StringBuilder _sb;

        private static int _logCount = 1;

        private static void Log(string s = "")
        {
            Console.WriteLine(s);
            _sb.AppendLine(s);
        }

        //private static void DebugTestAccuracy()
        //{
        //    // 1km
        //    var a = new Accuracy(1000.0);

        //    // 100kms
        //    var b = new Accuracy(1000 * 100.0);

        //    Debugger.Break();
        //}

        //private static void DebugGenerateCircularAreaPolygon()
        //{
        //    var bud = new GeoCoordinate
        //    {
        //        Latitude = 47.438437,
        //        Longitude = 19.252274
        //    };

        //    var gertrudRaskLand = new GeoCoordinate
        //    {
        //        Latitude = 83.331922,
        //        Longitude = -34.781282
        //    };

        //    var l = new List<GeoCoordinate>();
        //    for (int i = 0; i < 360; i++)
        //    {
        //        l.Add(gertrudRaskLand.CalculateDestinationPoint(1000000.0, i));
        //    }

        //    var s = l.Select(x => $"{x.Latitude}, {x.Longitude}").Aggregate((y, n) => y + Environment.NewLine + n);

        //    Debugger.Break();
        //}
    }
}
