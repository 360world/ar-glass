﻿using Soufflage.Core.Entities;

namespace Soufflage.Converters
{
    public interface IGeoCoordinateConverter
    {
        Coordinate Convert(GeoCoordinate gc);
    }
}
