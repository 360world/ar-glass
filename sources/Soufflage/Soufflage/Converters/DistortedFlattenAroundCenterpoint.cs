﻿using Newtonsoft.Json;
using Soufflage.Core.Entities;
using Soufflage.Core.Geography;

namespace Soufflage.Converters
{
    public class DistortedFlattenAroundCenterpoint : AbstractGeoCoordinateConverter, IGeoCoordinateConverter
    {
        private readonly GeoCoordinate _centerpoint;

        public DistortedFlattenAroundCenterpoint(GeoCoordinate centerpoint)
        {
            _centerpoint = centerpoint;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Counter += 1;
            var gc = (GeoCoordinate) value;
            serializer.Serialize(writer, Convert(gc));
        }

        public override string ToString() => $"{nameof(DistortedFlattenAroundCenterpoint)}[{_centerpoint.Latitude}, {_centerpoint.Longitude}]";

        public Coordinate Convert(GeoCoordinate gc)
        {
            var onX = new GeoCoordinate
            {
                Latitude = _centerpoint.Latitude,
                Longitude = gc.Longitude
            };
            return new Coordinate
            {
                X = (gc.Longitude < _centerpoint.Longitude ? -1.0 : 1.0) * onX.CalculateDistanceBetween(_centerpoint),
                Y = (gc.Latitude < _centerpoint.Latitude ? -1.0 : 1.0) * onX.CalculateDistanceBetween(gc),
            };
        }
    }
}
