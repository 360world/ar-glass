﻿using System;
using Newtonsoft.Json;
using Soufflage.Core.Entities;

namespace Soufflage.Converters
{
    public abstract class AbstractGeoCoordinateConverter : JsonConverter
    {
        public static int Counter { get; protected set; }

        public sealed override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public sealed override bool CanConvert(Type objectType) => objectType == typeof(GeoCoordinate);
    }
}
