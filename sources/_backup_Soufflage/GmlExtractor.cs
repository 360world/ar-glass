﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Soufflage.Core;

namespace Soufflage.PreprocessorAlpha
{
    [Obsolete]
    public class GmlExtractor : IPreprocessor
    {
        public PreprocessingResult Extract(string source)
        {
            var step1 = new Regex(@"<gml.[\s\S]*?(?=\s*<\/?aixm)", RegexOptions.Compiled | RegexOptions.CultureInvariant);
            var repIdi = 0;
            var d = new Dictionary<int, string>();
            var proc1 = step1.Replace(source, (m) =>
            {
                d[repIdi] = m.Value; //   start from 5 to skip <gml: part ˇ  ˇ and get the index of the first space for the type
                return $"<aixm:GmlEmbed originalType=\"{m.Value.Substring(5, m.Value.IndexOf(" ", StringComparison.InvariantCulture))}\" id=\"{repIdi}\" />";
            });
            var step2 = new Regex("(?<=<aixm.*?)gml:\\w+=\".*? \"", RegexOptions.Compiled | RegexOptions.CultureInvariant);
            var proc2 = step2.Replace(proc1, (m) => m.Value.Replace("gml:", "aixmGmlAttrib"));
            return new PreprocessingResult(proc2, d);
        }
    }
}

// #1: get gml embeds out
// <gml /> embeds inside <aixm /> -> remove to separate block, and mark with ID
// <aixm:GmlEmbed OriginalType="" Identifier="" />

// #2: get gml attribs out
// gml:XX= within <aixm > tags -> gmlXX=

// #3: build normal model with VS/XSD

// #4: parse aixm as usual

// #5: parse referenced gml embeds when required
