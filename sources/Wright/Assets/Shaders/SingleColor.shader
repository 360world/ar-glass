﻿Shader "Custom/SimpleColor" {
	Properties{
		_color("Color", Color) = (1, 0, 0, 1)
	}

		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		Pass{
		CGPROGRAM

		half4 _color;

#pragma vertex vert
#pragma fragment frag

	struct vertInput {
		float4 pos : POSITION;
	};

	struct vertOutput {
		float4 pos : SV_POSITION;
	};

	vertOutput vert(vertInput input) {
		vertOutput o;
		o.pos = mul(UNITY_MATRIX_MVP, input.pos);
		return o;
	}

	half4 frag(vertOutput output) : COLOR{
		return _color;

	}
		ENDCG
	}
	}

		FallBack "Diffuse"
}
