﻿Shader "Custom/FastTerrain" {
	Properties{
		_PeakColor("PeakColor", Color) = (0.8,0.9,0.9,1)
		_PeakLevel("PeakLevel", Float) = 300
		_Level3Color("Level3Color", Color) = (0.75,0.53,0,1)
		_Level3("Level3", Float) = 200
		_Level2Color("Level2Color", Color) = (0.69,0.63,0.31,1)
		_Level2("Level2", Float) = 100
		_Level1Color("Level1Color", Color) = (0.65,0.86,0.63,1)
		_WaterLevel("WaterLevel", Float) = 0
		_WaterColor("WaterColor", Color) = (0.37,0.78,0.92,1)
		_Slope("Slope Fader", Range(0,1)) = 0
	}
	SubShader{
		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

		struct v2f {
		float4 pos : SV_POSITION;
		fixed4 color : COLOR;
		float4 worldPos : WORLDPOS;
	};

	v2f vert(appdata_base v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.color = abs(v.normal.y);
		o.worldPos = v.vertex;
		return o;
	}

	float _PeakLevel;
	float4 _PeakColor;
	float _Level3;
	float4 _Level3Color;
	float _Level2;
	float4 _Level2Color;
	float _Level1;
	float4 _Level1Color;
	float _Slope;
	float _WaterLevel;
	float4 _WaterColor;

	fixed4 frag(v2f i) : SV_Target {
		fixed4 o = fixed4(0,0,0,0);
		if (i.worldPos.y >= _PeakLevel)
			o = _PeakColor;
		if (i.worldPos.y <= _PeakLevel)
			o = lerp(_Level3Color, _PeakColor, (i.worldPos.y - _Level3) / (_PeakLevel - _Level3));
		if (i.worldPos.y <= _Level3)
			o = lerp(_Level2Color, _Level3Color, (i.worldPos.y - _Level2) / (_Level3 - _Level2));
		if (i.worldPos.y <= _Level2)
			o = lerp(_Level1Color, _Level2Color, (i.worldPos.y - _WaterLevel) / (_Level2 - _WaterLevel));
		if (i.worldPos.y <= _WaterLevel)
			o = _WaterColor;
		o *= saturate(i.color + _Slope);
		return o;
	}
		ENDCG
	}
	}
}