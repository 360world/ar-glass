﻿Shader "Custom/Airspace" {
	Properties {
		_Color("Color", Color) = (1, 0, 0, 1) // (R, G, B, A)
		_RimValue("Rim value", Range(0, 1)) = 0.5
	}
	SubShader {
		Tags {
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
		}

		CGPROGRAM

#pragma surface surf Lambert alpha

		fixed _RimValue;
		half4 _Color;

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
			float3 worldNormal;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = _Color.rgb;
			float3 normal = normalize(IN.worldNormal);
			float3 dir = normalize(IN.viewDir);
			float val = 1 - (abs(dot(dir, normal)));
			float rim = val * val * _RimValue;
			o.Alpha = _Color.a * rim;
		}

		ENDCG
	}

	FallBack "Diffuse"
}