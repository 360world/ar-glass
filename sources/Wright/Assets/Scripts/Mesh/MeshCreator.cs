﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshCreator
{
    public static Mesh CreateMesh(Vector2[] vertices2D, float bottom, float top, GameObject infoPoint)
    {
        // Use the triangulator to get indices for creating triangles
        var tr = new Triangulator(vertices2D);
        var indices = tr.Triangulate();

        // Create the Vector3 vertices
        var vertices = new Vector3[vertices2D.Length * 2];
        for (var i = 0; i < vertices2D.Length; i++)
        {
            vertices[i] = new Vector3(vertices2D[i].x, bottom, vertices2D[i].y);
            vertices[i + vertices2D.Length] = new Vector3(vertices2D[i].x, top, vertices2D[i].y);
        }

        /* debug */
        //var gp = new GoProxy();
        //for (var index = 0; index < vertices.Length; index++)
        //{
        //    var vector3 = vertices[index];
        //    var a = gp.InstantiateProxy(infoPoint);
        //    a.transform.position = vector3;
        //    a.GetComponent<TextMesh>().text = index.ToString();
        //}
        /* debug */

        // Create triangles (top, bottom, sides, last special side-pair)
        var finalTriangles = new int[indices.Length * 2 + vertices2D.Length * 6];
        for (var i = 0; i < indices.Length; i++)
        {
            finalTriangles[i] = indices[i];
            finalTriangles[i + indices.Length] = indices[i] + vertices2D.Length;
        }
        for (var i = 0; i < indices.Length; i += 3)
        {
            var temp = finalTriangles[i + 0];
            finalTriangles[i + 0] = finalTriangles[i + 1];
            finalTriangles[i + 1] = temp;
        }
        //for (var i = 0; i < vertices2D.Length; i++)
        //{
        //    finalTriangles[indices.Length * 2 + i * 6 + 0] = i;
        //    finalTriangles[indices.Length * 2 + i * 6 + 1] = vertices2D.Length == i + 1 ? 0 : i + 1;
        //    finalTriangles[indices.Length * 2 + i * 6 + 2] = i + vertices2D.Length;
        //    finalTriangles[indices.Length * 2 + i * 6 + 3] = vertices2D.Length == i + 1 ? 0 : i + 1;
        //    finalTriangles[indices.Length * 2 + i * 6 + 4] = i + vertices2D.Length + 1 == vertices2D.Length * 2 ? vertices2D.Length : i + vertices2D.Length + 1;
        //    finalTriangles[indices.Length * 2 + i * 6 + 5] = i + vertices2D.Length;
        //}
        for (var i = 0; i < vertices2D.Length - 1; i++)
        {
            finalTriangles[indices.Length * 2 + i * 6 + 0] = i;
            finalTriangles[indices.Length * 2 + i * 6 + 1] = i + 1;
            finalTriangles[indices.Length * 2 + i * 6 + 2] = i + vertices2D.Length;
            finalTriangles[indices.Length * 2 + i * 6 + 3] = i + 1;
            finalTriangles[indices.Length * 2 + i * 6 + 4] = i + vertices2D.Length + 1;
            finalTriangles[indices.Length * 2 + i * 6 + 5] = i + vertices2D.Length;
        }
        var j = (vertices2D.Length - 1);
        finalTriangles[indices.Length * 2 + j * 6 + 0] = j;
        finalTriangles[indices.Length * 2 + j * 6 + 1] = 0;
        finalTriangles[indices.Length * 2 + j * 6 + 2] = j + vertices2D.Length;
        finalTriangles[indices.Length * 2 + j * 6 + 3] = 0;
        finalTriangles[indices.Length * 2 + j * 6 + 4] = vertices2D.Length;
        finalTriangles[indices.Length * 2 + j * 6 + 5] = j + vertices2D.Length;

        // UVs
        var finalUvs = new Vector2[vertices.Length];
        for (var i = 0; i < vertices.Length; i++)
        {
            finalUvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }

        // Create the mesh
        var mesh = new Mesh
        {
            vertices = vertices,
            triangles = finalTriangles,
            uv = finalUvs
        };
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        return mesh;
    }

    public static void ReverseNormals(Mesh mesh)
    {
        var normals = mesh.normals;
        for (var i = 0; i < normals.Length; i++) normals[i] = -normals[i];
        mesh.normals = normals;
        for (var m = 0; m < mesh.subMeshCount; m++)
        {
            var triangles = mesh.GetTriangles(m);
            for (var i = 0; i < triangles.Length; i += 3)
            {
                var temp = triangles[i + 0];
                triangles[i + 0] = triangles[i + 1];
                triangles[i + 1] = temp;
            }
            mesh.SetTriangles(triangles, m);
        }
    }
}
