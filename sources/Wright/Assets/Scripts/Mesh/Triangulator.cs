﻿using UnityEngine;
using System.Collections.Generic;

public class Triangulator
{
    private readonly List<Vector2> _mPoints;

    public Triangulator(IEnumerable<Vector2> points)
    {
        _mPoints = new List<Vector2>(points);
    }

    public int[] Triangulate()
    {
        var indices = new List<int>();

        var n = _mPoints.Count;
        if (n < 3) return indices.ToArray();

        var vectors = new int[n];
        if (Area() > 0)
        {
            for (var v = 0; v < n; v++) vectors[v] = v;
        }
        else
        {
            for (var v = 0; v < n; v++) vectors[v] = (n - 1) - v;
        }

        var nv = n;
        var count = 2 * nv;
        for (var v = nv - 1; nv > 2;)
        {
            if ((count--) <= 0) return indices.ToArray();

            var u = v;
            if (nv <= u) u = 0;
            v = u + 1;
            if (nv <= v) v = 0;
            var w = v + 1;
            if (nv <= w) w = 0;

            if (!Snip(u, v, w, nv, vectors)) continue;

            int s, t;
            var a = vectors[u];
            var b = vectors[v];
            var c = vectors[w];
            indices.Add(a);
            indices.Add(b);
            indices.Add(c);
            for (s = v, t = v + 1; t < nv; s++, t++) vectors[s] = vectors[t];
            nv--;
            count = 2 * nv;
        }

        indices.Reverse();
        return indices.ToArray();
    }

    private float Area()
    {
        var n = _mPoints.Count;
        var area = 0.0f;
        for (int p = n - 1, q = 0; q < n; p = q++)
        {
            var pval = _mPoints[p];
            var qval = _mPoints[q];
            area += pval.x * qval.y - qval.x * pval.y;
        }
        return (area * 0.5f);
    }

    private bool Snip(int u, int v, int w, int n, int[] vectors)
    {
        int p;
        var a = _mPoints[vectors[u]];
        var b = _mPoints[vectors[v]];
        var c = _mPoints[vectors[w]];
        if (Mathf.Epsilon > (((b.x - a.x) * (c.y - a.y)) - ((b.y - a.y) * (c.x - a.x)))) return false;
        for (p = 0; p < n; p++)
        {
            if ((p == u) || (p == v) || (p == w)) continue;
            if (InsideTriangle(a, b, c, _mPoints[vectors[p]])) return false;
        }
        return true;
    }

    private static bool InsideTriangle(Vector2 a, Vector2 b, Vector2 c, Vector2 p)
    {
        var ax = c.x - b.x;
        var ay = c.y - b.y;
        var bx = a.x - c.x;
        var @by = a.y - c.y;
        var cx = b.x - a.x;
        var cy = b.y - a.y;
        var apx = p.x - a.x;
        var apy = p.y - a.y;
        var bpx = p.x - b.x;
        var bpy = p.y - b.y;
        var cpx = p.x - c.x;
        var cpy = p.y - c.y;

        var aCrossBp = ax * bpy - ay * bpx;
        var cCrossAp = cx * apy - cy * apx;
        var bCrossCp = bx * cpy - @by * cpx;

        return ((aCrossBp >= 0.0f) && (bCrossCp >= 0.0f) && (cCrossAp >= 0.0f));
    }
}