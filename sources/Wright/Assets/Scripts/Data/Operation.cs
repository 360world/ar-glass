﻿namespace Soufflage.Core.Entities
{
    public enum OperationType
    {
        Union,
        Intersect,
        Subtract
    }

    public class Operation
    {
        public OperationType Type { get; set; }

        public int Order { get; set; }

        public Polygon Element { get; set; }
    }
}
