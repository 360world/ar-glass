﻿using System.Collections.Generic;
using Soufflage.Core.Entities;

namespace Soufflage
{
    public class ResultWrapper
    {
        public List<Group> Groups { get; set; }

        public Coordinate CameraPosition { get; set; }

        public Coordinate TerrainTopLeftPosition { get; set; }

        public Coordinate TerrainBottomRightPosition { get; set; }

        public double CameraHeight { get; set; }

        public bool IsCameraFirstPerson { get; set; }
    }
}
