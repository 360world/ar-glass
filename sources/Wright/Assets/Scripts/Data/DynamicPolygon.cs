﻿using System.Collections.Generic;
using System.Linq;

namespace Soufflage.Core.Entities
{
    public class DynamicPolygon : Polygon
    {
        public Polygon Base { get; set; }

        public List<Operation> Modifiers { get; set; }

#if UNITY_5
        public override IEnumerable<Coordinate> GetAllCoordinates()
        {
            return new List<Polygon> { Base }.Union(Modifiers.Select(x => x.Element)).SelectMany(y => y.GetAllCoordinates());
        }
#else
        public override IEnumerable<GeoCoordinate> GetAllCoordinates()
        {
            return new List<Polygon> {Base}.Union(Modifiers.Select(x => x.Element)).SelectMany(y => y.GetAllCoordinates());
        }
#endif

        public override string ShapeType
        {
            get { return "CompositePolygon"; }
        }
    }
}
