﻿using System.Collections.Generic;
using System.Linq;

namespace Soufflage.Core.Entities
{
    public class StaticPolygon : Polygon
    {
        public List<Point> ExteriorBoundary { get; set; }

        public List<List<Point>> InteriorBoundaries { get; set; }

        public bool UseGlobalHeightOverride { get; set; }

        public Height GlobalHeightOverride { get; set; }

#if UNITY_5
        public override IEnumerable<Coordinate> GetAllCoordinates()
        {
            return ExteriorBoundary.SelectMany(x => x.GetAllCoordinates());
        }
#else
        public override IEnumerable<GeoCoordinate> GetAllCoordinates()
        {
            return ExteriorBoundary.SelectMany(x => x.GetAllCoordinates());
        }
#endif

        public override string ShapeType
        {
            get { return "StaticPolygon"; }
        }
    }
}
