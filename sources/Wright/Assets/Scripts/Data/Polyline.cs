﻿using System.Collections.Generic;
using System.Linq;

namespace Soufflage.Core.Entities
{
    public class Polyline : Shape
    {
        public List<Point> Locations { get; set; }

        public bool UseGlobalHeightOverride { get; set; }

        public Height GlobalHeightOverride { get; set; }

#if UNITY_5
        public override IEnumerable<Coordinate> GetAllCoordinates()
        {
            return Locations.SelectMany(x => x.GetAllCoordinates());
        }
#else
        public override IEnumerable<GeoCoordinate> GetAllCoordinates()
        {
            return Locations.SelectMany(x => x.GetAllCoordinates());
        }
#endif

        public override string ShapeType
        {
            get { return "Polyline"; }
        }
    }
}
