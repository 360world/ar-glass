﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Soufflage.Core.Entities;

public class ShapeConverter : JsonConverter
{

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        var jObject = JObject.Load(reader);
        JToken shapeType;
        if (jObject.TryGetValue("ShapeType", out shapeType))
        {
            switch (shapeType.ToString())
            {
                case "Point":
                    var p = new Point();
                    serializer.Populate(jObject.CreateReader(), p);
                    return p;
                case "Polyline":
                    var pl = new Polyline();
                    serializer.Populate(jObject.CreateReader(), pl);
                    return pl;
                case "StaticPolygon":
                    var sp = new StaticPolygon();
                    serializer.Populate(jObject.CreateReader(), sp);
                    return sp;
                case "CompositePolygon":
                    var dp = new DynamicPolygon();
                    serializer.Populate(jObject.CreateReader(), dp);
                    return dp;
                default:
                    throw new NotImplementedException();
            }
        }
        else
        {
            throw new NotImplementedException();
        }
    }

    public override bool CanConvert(Type objectType)
    {
        return typeof(Shape).IsAssignableFrom(objectType);
    }
}
