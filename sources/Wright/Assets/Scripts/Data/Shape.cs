﻿using System;
using System.Collections.Generic;

namespace Soufflage.Core.Entities
{
    public abstract class Shape
    {

#if UNITY_5
        public abstract IEnumerable<Coordinate> GetAllCoordinates();
#else
        public abstract IEnumerable<GeoCoordinate> GetAllCoordinates();
#endif

        public abstract string ShapeType { get; }

        public string ShapeIdentifier { get; set; }

        public string ShapeFriendlyName { get; set; }
    }
}
