﻿using System.Collections.Generic;

namespace Soufflage.Core.Entities
{
    public class Point : Shape
    {
        public Height Height { get; set; }

#if UNITY_5
        public Coordinate Location { get; set; }
#else
        public GeoCoordinate Location { get; set; }
#endif

#if UNITY_5
        public override IEnumerable<Coordinate> GetAllCoordinates()
        {
            return new List<Coordinate> { Location };
        }
#else
        public override IEnumerable<GeoCoordinate> GetAllCoordinates()
        {
            return new List<GeoCoordinate> {Location};
        }
#endif

        public override string ShapeType
        {
            get { return "Point"; }
        }
    }
}
