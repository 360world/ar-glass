﻿using System.Collections.Generic;

namespace Soufflage.Core.Entities
{

#if !UNITY_5
    public enum GroupFor
    {
        Airspace,
        PointOfInterest,
        Reference,
        StateBorder,
        NOTAM,
        Building,
        GroundObstacle,
        AirportHeliport,
        Route
    }
#endif

    public class Group
    {
        public Group()
        {
            Shapes = new List<Shape>();
        }

        public List<Shape> Shapes { get; set; }

#if UNITY_5
        public string GroupFor { get; set; }
#else
        public GroupFor GroupFor { get; set; }
#endif

        public string Identifier { get; set; }

        public string FriendlyName { get; set; }

        public string Description { get; set; }

        public string LocalType { get; set; }

        public void GenerateDescription()
        {
            //todo
            Description = "todo";
        }

        public bool Ignore { get; set; }
    }
}
