﻿namespace Soufflage.Core.Entities
{
    public class Height
    {
        public string LowerLimitReference { get; set; }

        public double? LowerLimit { get; set; }

        public string UpperLimitReference { get; set; }

        public double? UpperLimit { get; set; }
    }
}
