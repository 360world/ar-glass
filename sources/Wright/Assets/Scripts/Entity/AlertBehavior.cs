﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlertBehavior : MonoBehaviour
{
    void Start()
    {
        timeout = 3.0f;
    }

    public Text AlertText;

    public float timeout;

    /*
     * 0 = not hit, not visible
     * 1 = hit, waiting
     * 2 = hit, is displayed
     * */
    private int _hitState = 0;

    private AlertProvider _currentAlertProvider;
    private float _timeHitStart;

    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
        {
            var ap = hit.collider.gameObject.GetComponent<AlertProvider>();
            if (ap == null)
            {
                if (_hitState == 2)
                {
                    AlertText.text = "";
                }
                _hitState = 0;
                _currentAlertProvider = null;

            }
            else
            {
                if (_hitState == 0)
                {
                    _hitState = 1;
                    _currentAlertProvider = ap;
                    _timeHitStart = Time.time;
                }
                if (_hitState == 1)
                {
                    if (_timeHitStart + timeout < Time.time)
                    {
                        _hitState = 2;
                        AlertText.text = ap.AlertText;
                    }
                }
            }
        }
        else
        {
            if (_hitState == 2)
            {
                AlertText.text = "";
            }
            _hitState = 0;
            _currentAlertProvider = null;
        }

    }

    private void CheckNewProvider(AlertProvider p)
    {
        switch (_hitState)
        {
            case 0:
                _currentAlertProvider = p;
                _timeHitStart = Time.time;
                _hitState += 1;
                break;
            case 1:
                if (_currentAlertProvider == p)
                {
                    if (_timeHitStart + timeout < Time.time)
                    {
                        Debug.Log("alert visible");
                        _hitState += 1;
                    };
                }
                else
                {

                }
                break;
            case 2:
                break;
        }
    }
}
