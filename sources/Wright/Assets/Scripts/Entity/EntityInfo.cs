﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityInfo : MonoBehaviour
{
    public string Description;
    public string LocalType;
    public string Type;
    public string Information;
}
