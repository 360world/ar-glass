﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PositionMarkerBehavior : MonoBehaviour
{
    public Image Background;

    public Text TextBox;

    public Vector3 DefaultSize;

    void LateUpdate()
    {
        var cam = Camera.main;
        var target = cam.transform.position;
        var direction = transform.position - target;
        transform.rotation = Quaternion.LookRotation(direction);
        var size = direction.magnitude * Mathf.Tan(cam.fieldOfView / 2 * Mathf.Deg2Rad);
        transform.localScale = DefaultSize * size;
    }
}
