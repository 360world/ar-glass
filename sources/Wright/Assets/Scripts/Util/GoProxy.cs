﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoProxy : Object
{
    public T InstantiateProxy<T>(T g) where T : UnityEngine.Object
    {
        return Instantiate(g);
    }
}
