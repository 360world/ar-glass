﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testcode_MeshCreator : MonoBehaviour
{
    public GameObject InfoPoint;

    private Vector2[] _testVertices1 = {
            new Vector2(0,0),
            new Vector2(0,50),
            new Vector2(50,50),
            new Vector2(50,100),
            new Vector2(0,100),
            new Vector2(0,150),
            new Vector2(150,150),
            new Vector2(150,100),
            new Vector2(100,100),
            new Vector2(100,50),
            new Vector2(150,50),
            new Vector2(150,0),
        };

    private Vector2[] _testVertices2 =
    {
            new Vector2(0,0),
            new Vector2(0,50),
            new Vector2(50,50),
    };

    private Vector2[] _testVertices3 = {
            new Vector2(0,0),
            new Vector2(0,50),
            new Vector2(50,50),
            new Vector2(50,100),
            new Vector2(0,100),
            new Vector2(0,150),
            new Vector2(150,150),
            new Vector2(150,100),
            new Vector2(100,100),
            new Vector2(100,50),
            new Vector2(150,50),
        };

    private Vector2[] _badAirspaceExample =
    {
        new Vector2(-15087.8f, 24093.0f),
        new Vector2(7543.9f, 22240.0f),
        new Vector2(-5657.9f, 18533.9f),
        new Vector2(-13830.5f, 20386.9f),
        //new Vector2(-15087.8f, 24093.0f),
    };

    void Start ()
    {
        var g = new GameObject("test");
        g.AddComponent<MeshFilter>();
        g.AddComponent<MeshRenderer>();
        g.GetComponent<MeshFilter>().mesh = MeshCreator.CreateMesh(_badAirspaceExample, 0, 5000.0f, InfoPoint);
    }

}
