﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Testcode_EyeScaling : MonoBehaviour
{
    public GameObject Scale1;
    public GameObject Scale10;
    public GameObject Scale10f;

    public Text Debugtext;

    public Camera LeftEye;

    public Camera Righteye;

    private float _originalStereoSeparation;

    void Start ()
    {
        //_originalStereoSeparation = Camera.main.stereoSeparation;
	    StartCoroutine(Switcher());
	}

    private IEnumerator Switcher()
    {
        while (true)
        {
            Scale1.SetActive(true);
            Scale10.SetActive(false);
            Scale10f.SetActive(false);
            //Camera.main.stereoSeparation = _originalStereoSeparation;
            yield return new WaitForSeconds(5.0f);
            Scale1.SetActive(false);
            Scale10.SetActive(true);
            Scale10f.SetActive(false);
            //Camera.main.stereoSeparation = _originalStereoSeparation;
            yield return new WaitForSeconds(5.0f);
            Scale1.SetActive(false);
            Scale10.SetActive(false);
            Scale10f.SetActive(true);
            //Camera.main.stereoSeparation = _originalStereoSeparation * 10.0f;
            yield return new WaitForSeconds(5.0f);
        }
    }

    // Update is called once per frame
	void Update ()
	{
	    //Debugtext.text = Camera.main.stereoSeparation.ToString("N8");
	    Righteye.transform.Rotate(new Vector3(0,0,1.0f*Time.deltaTime), Space.Self);
	}
}
