﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class NotamLogic : MonoBehaviour
{
    public GameObject Notam1;
    public GameObject Notam2;
    public GameObject Notam3;

    private int _state = 1;

    void Start()
    {
        InteractionManager.SourceReleased += InteractionManager_SourceReleased;
    }

    private void InteractionManager_SourceReleased(InteractionSourceState state)
    {
        switch (_state)
        {
            case 1:
                Notam1.SetActive(false);
                Notam2.SetActive(true);
                _state += 1;
                break;
            case 2:
                Notam2.SetActive(false);
                Notam3.SetActive(true);
                _state += 1;
                break;
            case 3:
                Notam3.SetActive(false);
                InteractionManager.SourceReleased -= InteractionManager_SourceReleased;
                break;
        }
    }

#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            InteractionManager_SourceReleased(new InteractionSourceState());
        }
    }
#endif
}
