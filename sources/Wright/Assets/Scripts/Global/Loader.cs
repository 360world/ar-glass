﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Soufflage;
using Soufflage.Core.Entities;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Loader : MonoBehaviour
{
    [Header("Data")]
    public string DataFileName;

    [Header("Look & Feel")]
    public Material AirspaceMaterial;


    public Material GroundObstacleMaterial;

    public float GroundObstacleSize = 1.0f;

    public Color GroundObstacleColor;

    public Material PointOfInterestMaterial;

    public float PointOfInterestSize = 1.0f;

    public Color PointOfInterestColor;

    public Material AirportHeliportMaterial;

    public float AirportHeliportSize = 10.0f;

    public Color AirportHeliportColor;

    public float AirspaceOpacity;

    public Material RouteMaterial;

    public Color RouteColor;

    public bool IfRouteExistsOnlyDisplayRouteMarkers;

    [Header("Debug")]
    public GameObject InfoPoint;

    [Header("Generation settings")]

    public bool EnforceIgnored;

	public Transform AirspaceHolder;

    public double ScaleReduceFactor;

    public Transform TerrainHolder;

    public bool CreateBackfaceGeometry;

    public GameObject PositionMarker;

    public float MarkerYOffset = 10.0f;

    [Header("UI")]

    public Text CurrentAirspaces;

	private string LoadJson()
	{
//        var s = "";
//#if WINDOWS_UWP

//        Task task = new Task(

//            async () =>
//            {
//                StorageFile textFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Data/soufflage_result.json"));
//                plainText = await FileIO.ReadTextAsync(textFile);
//                s = plainText;
//            });
//        task.Start();
//        task.Wait();
//#else
		var res = Resources.Load<TextAsset>(DataFileName);
		return res.text;
	}

    private Vector2[] CreateBase(Vector2 offset, float size)
    {
        var sa = size/2.0f;

        const int sides = 16;
        var a = new Vector2[sides];
        for (var i = 0; i < sides; i++)
        {
            a[i] = new Vector2(offset.x + sa*Mathf.Cos(2*Mathf.PI/sides*i),
                               offset.y - sa*Mathf.Sin(2*Mathf.PI/sides*i));
        }
        //var a = new Vector2[4];
        //a[0] = new Vector2(offset.x - sa, offset.y - sa);
        //a[1] = new Vector2(offset.x - sa, offset.y + sa);
        //a[2] = new Vector2(offset.x + sa, offset.y + sa);
        //a[3] = new Vector2(offset.x + sa, offset.y - sa);
        return a;
    }

    // Use this for initialization
    void Start ()
	{
        StartCoroutine(CreateObjects());
    }

    private Vector3 CreateQuick(Vector2 xy, float height)
    {
        return new Vector3(xy.x, height, xy.y);
    }

    IEnumerator CreateObjects()
    {
        var json = LoadJson();
        var data = JsonConvert.DeserializeObject<ResultWrapper>(json, new JsonSerializerSettings
        {
            Converters = new List<JsonConverter>
            {
                new ShapeConverter()
            }
        });
        var colorChoices = new[] {new Color(0.0f, 1.0f, 0.0f), new Color(0.0f, 0.0f, 1.0f),
                                   new Color(1.0f, 0.0f, 1.0f), new Color(0.0f, 1.0f, 1.0f)};

        var af = new List<string>
        {
            "ASE1940",
            "ASE1942",
            "ASE1944",
            "ASE2096",
            "ASE2539",
            "ASE2119",
            //"ASE1880",
            //"ASE1881",
            "ASE1879",
            "ASE1878",
            "ASE1888",
            "ASE1889",
            "ASE1885",
            "ASE1998"
        };
        var sb = new StringBuilder();

        var itemHeightScaleFactor = !data.IsCameraFirstPerson ? 10.0f : 1.0f;
        var sPm = IfRouteExistsOnlyDisplayRouteMarkers && data.Groups.Any(x => x.GroupFor == "Route");

        foreach (var gr in data.Groups)
        {
            if(EnforceIgnored && gr.Ignore) continue;
            if (gr.GroupFor == "AirportHeliport")
            {
                var shape = (Point)gr.Shapes[0];
                // create
                var offset = new Vector2((float) (shape.Location.X/ScaleReduceFactor), (float) (shape.Location.Y/ScaleReduceFactor));
                var v2d = CreateBase(offset, AirportHeliportSize);
                var bottom = 0.0f;
                var top = (float) ((shape.Height.UpperLimit ?? 0.0)/ScaleReduceFactor);
                var genMesh = MeshCreator.CreateMesh(v2d, bottom, top, InfoPoint);
                // place
                var go = Place(gr.Identifier, genMesh, AirportHeliportMaterial, new Color(1.0f, 1.0f, 1.0f, 1.0f), AirspaceHolder);
                go.AddComponent<EntityInfo>();
                go.GetComponent<EntityInfo>().Type = gr.GroupFor;
                go.GetComponent<EntityInfo>().LocalType = gr.LocalType ?? "n/a";
                go.GetComponent<EntityInfo>().Information = gr.FriendlyName;
                go.GetComponent<EntityInfo>().Description = gr.Description ?? "n/a";
                if (!sPm) PlaceMarker("marker_" + gr.Identifier, gr.FriendlyName, AirspaceHolder, CreateQuick(offset, top + MarkerYOffset), AirportHeliportColor, (float)ScaleReduceFactor);
            }
            else if (gr.GroupFor == "GroundObstacle" || gr.GroupFor == "PointOfInterest")
            {
                var isPoi = gr.GroupFor.StartsWith("P");
                var shape = (Point)gr.Shapes[0];
                // create
                var offset = new Vector2((float) (shape.Location.X/ScaleReduceFactor), (float) (shape.Location.Y/ScaleReduceFactor));
                var v2d = CreateBase(offset, isPoi ? PointOfInterestSize : GroundObstacleSize);
                var bottom = 0.0f;
                var top = (shape.Height == null ? PointOfInterestSize : (float)((shape.Height.UpperLimit ?? 0.0) / ScaleReduceFactor)) * itemHeightScaleFactor;
                var genMesh = MeshCreator.CreateMesh(v2d, bottom, top, InfoPoint);
                // place
                var go = Place(gr.Identifier, genMesh, isPoi ? PointOfInterestMaterial : GroundObstacleMaterial, isPoi ? PointOfInterestColor : GroundObstacleColor, AirspaceHolder);
                go.AddComponent<EntityInfo>();
                go.GetComponent<EntityInfo>().Type = gr.GroupFor;
                go.GetComponent<EntityInfo>().LocalType = gr.LocalType ?? "n/a";
                go.GetComponent<EntityInfo>().Information = gr.FriendlyName;
                go.GetComponent<EntityInfo>().Description = gr.Description ?? "n/a";
                if (!sPm) PlaceMarker("marker_" + gr.Identifier, gr.FriendlyName, AirspaceHolder, CreateQuick(offset, top + MarkerYOffset),
                    isPoi ? PointOfInterestColor : GroundObstacleColor, (float)ScaleReduceFactor);
            }
            else if (gr.GroupFor == "Airspace")
            {
                var shape = (StaticPolygon)gr.Shapes[0];
                // create
                var vertices2D = shape.ExteriorBoundary.Select(x => new Vector2((float)(x.Location.X / ScaleReduceFactor), (float)(x.Location.Y / ScaleReduceFactor))).ToArray();
                var bottom = (float)((shape.GlobalHeightOverride.LowerLimit ?? 0.0) / ScaleReduceFactor);
                var top = (float)((shape.GlobalHeightOverride.UpperLimit ?? 15488.0) / ScaleReduceFactor); // max height 51k ft, by FAA
                var sc = colorChoices[Random.Range(0, (colorChoices.Length))];
                var genMesh = MeshCreator.CreateMesh(vertices2D, bottom, top, InfoPoint);
                // filter
                if (!af.Contains(gr.Identifier))
                {
                    // place
                    var currentAirspaceColor = new Color(sc.r, sc.g, sc.b, AirspaceOpacity);
                    var shouldAddAp = false;
                    if (gr.FriendlyName.Contains("LHP"))
                    {
                        currentAirspaceColor = new Color(1.0f, 0.0f, 0.0f, AirspaceOpacity);
                        shouldAddAp = true;
                    }
                    var currentAirspaceInnerColor = new Color(currentAirspaceColor.r, currentAirspaceColor.g, currentAirspaceColor.b, currentAirspaceColor.a * 0.5f);
                    var go = Place(gr.Identifier, genMesh, AirspaceMaterial, currentAirspaceColor, AirspaceHolder);
                    go.AddComponent<EntityInfo>();
                    go.GetComponent<EntityInfo>().Type = gr.GroupFor;
                    go.GetComponent<EntityInfo>().LocalType = gr.LocalType ?? "n/a";
                    go.GetComponent<EntityInfo>().Information = gr.FriendlyName;
                    go.GetComponent<EntityInfo>().Description = gr.Description ?? "n/a";
                    if (shouldAddAp)
                    {
                        var mc = go.AddComponent<MeshCollider>();
                        mc.sharedMesh = genMesh;
                        var apo = go.AddComponent<AlertProvider>();
                        apo.AlertText = "Please change course to avoid" + Environment.NewLine +"prohibited airspace " + gr.FriendlyName.Split('-')[1].Trim() + "!";
                    }
                    if (CreateBackfaceGeometry)
                    {
                        var invMesh = Instantiate(genMesh);
                        MeshCreator.ReverseNormals(invMesh);
                        Place(gr.Identifier + "_inv", invMesh, AirspaceMaterial, currentAirspaceInnerColor, AirspaceHolder);
                    }
                }
            } else if (gr.GroupFor == "Route")
            {
                var shape = (Polyline)gr.Shapes[0];
                //create
                var go = new GameObject(gr.Identifier);
                go.transform.parent = AirspaceHolder;
                var lr = go.AddComponent<LineRenderer>();
                lr.useWorldSpace = false;
                lr.numPositions = shape.Locations.Count;
                var vertices3D = shape.Locations.Select(x =>
                    CreateQuick(new Vector2((float) (x.Location.X/ScaleReduceFactor), (float) (x.Location.Y/ScaleReduceFactor)),
                                (float) ((x.Height.UpperLimit ?? 0.0)/ScaleReduceFactor)*itemHeightScaleFactor)).ToArray();
                lr.SetPositions(vertices3D);
                lr.startWidth = 0.01f;
                lr.endWidth = 0.01f;
                lr.startColor = RouteColor;
                lr.endColor = RouteColor;
                lr.material = RouteMaterial;
                var moffsetVector = new Vector3(0, MarkerYOffset, 0);
                for (var i = 0; i < vertices3D.Length; i++)
                {
                    PlaceMarker("marker_" + shape.Locations[i].ShapeIdentifier, shape.Locations[i].ShapeFriendlyName,
                        AirspaceHolder, vertices3D[i] + moffsetVector, AirportHeliportColor, (float) ScaleReduceFactor);
                }
            }
            yield return new WaitForEndOfFrame();
        }

        // reposition terrain
        TerrainHolder.localPosition = new Vector3((float)(data.TerrainTopLeftPosition.X / ScaleReduceFactor),
                                                  0.0f,
                                                  (float)(data.TerrainBottomRightPosition.Y / ScaleReduceFactor));
        var rootTerrain = TerrainHolder.GetChild(0).GetChild(0);
        var cmod = data.IsCameraFirstPerson ? -1*(float) (data.CameraHeight/ScaleReduceFactor) : 0.0f;
        for (var i = 0; i < rootTerrain.childCount; i++)
        {
            var chm = rootTerrain.GetChild(i).gameObject.GetComponent<MeshRenderer>().material;
            chm.SetFloat("_WaterLevel", chm.GetFloat("_WaterLevel") + cmod);
            chm.SetFloat("_Level2", chm.GetFloat("_Level2") + cmod);
            chm.SetFloat("_Level3", chm.GetFloat("_Level3") + cmod);
            chm.SetFloat("_PeakLevel", chm.GetFloat("_PeakLevel") + cmod);
        }

        if (data.IsCameraFirstPerson)
        {
            // reposition to camera, assuming 0,0,0 rotation as north
            AirspaceHolder.position = new Vector3(-1*(float) (data.CameraPosition.X/ScaleReduceFactor),
                -1*(float) (data.CameraHeight/ScaleReduceFactor),
                -1*(float) (data.CameraPosition.Y/ScaleReduceFactor));
        }
        else
        {
            AirspaceHolder.position = new Vector3(0, -1.5f, 1.5f);
        }
        yield return new WaitForEndOfFrame();

        // list ignored
        //CurrentAirspaces.text = sb.ToString().Trim();
        // manual at the moment...
    }

    private GameObject PlaceMarker(string n, string title, Transform parent, Vector3 position, Color c, float scaleFactor)
    {
        var m = Instantiate(PositionMarker);
        m.name = n;
        m.GetComponent<PositionMarkerBehavior>().TextBox.text = title;
        m.GetComponent<PositionMarkerBehavior>().Background.color = c;
        m.GetComponent<PositionMarkerBehavior>().DefaultSize = new Vector3(100/scaleFactor, 100/scaleFactor, 100/scaleFactor);
        m.transform.parent = parent;
        m.transform.position = position;
        return m;
    }

    private GameObject Place(string n, Mesh mesh, Material material, Color color, Transform parent)
    {
        var g = new GameObject(n);
        g.AddComponent<MeshFilter>();
        g.AddComponent<MeshRenderer>();
        g.GetComponent<MeshFilter>().mesh = mesh;
        g.GetComponent<MeshRenderer>().material = material;
        g.GetComponent<Renderer>().material.color = color;
        g.transform.parent = parent;
        return g;
    }
}
