﻿/*     INFINITY CODE 2013-2015      */
/*   http://www.infinity-code.com   */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (RealWorldTerrainOSMBuilding))]
public class RealWorldTerrainOSMBuildingEditor : Editor
{
    public static GameObject baseContainer;
    public static GameObject houseContainer;
    
    private static Material defHouseRoofMaterial;
    private static Material defHouseWallMaterial;
    private RealWorldTerrainOSMBuilding building;

    public static List<RealWorldTerrainOSMNode> nodes;
    public static List<RealWorldTerrainOSMWay> ways;
    public static List<RealWorldTerrainOSMRelation> relations;
    public static bool loaded;

    private static string url
    {
        get
        {
            string format = string.Format("node({0},{1},{2},{3});way(bn)['building'];(._;>;);out;", prefs.coordinatesTo.y, prefs.coordinatesFrom.x, prefs.coordinatesFrom.y, prefs.coordinatesTo.x);
            string data = format;
            return "http://overpass.osm.rambler.ru/cgi/interpreter?data=" + data;
        }
    }

    private static RealWorldTerrainPrefs prefs
    {
        get { return RealWorldTerrain.prefs; }
    }

    public static string filename
    {
        get
        {
            return Path.Combine(RealWorldTerrainEditorUtils.osmCacheFolder, string.Format("buildings_{0}_{1}_{2}_{3}.osm", prefs.coordinatesTo.y, prefs.coordinatesFrom.x, prefs.coordinatesFrom.y, prefs.coordinatesTo.x));
        }
    }

    public static string compressedFilename
    {
        get
        {
            return filename + "c";
        }
    }

    private static float AddHouseWallVerticle(List<Vector3> vertices, Vector3 p, float topPoint, ref Vector2 vp1,
        ref Vector2 vp2, float angle)
    {
        Vector3 tv = new Vector3(p.x, topPoint, p.z);
        Vector2 rp = new Vector2(p.x, p.z);

        vertices.Add(p);
        vertices.Add(tv);

        return RealWorldTerrainOSM.GetTriangleDirectionOffset(rp, ref vp1, ref vp2, angle);
    }

    private static void AddWallBaseVerticles(Vector3[] points, int i, float wallWidth, List<Vector3> side1,
        List<Vector3> side2)
    {
        Vector3 p1 = points[i - 1];
        Vector3 p2 = points[i];
        Vector3 p3 = points[i + 1];
        float a1 = RealWorldTerrainUtils.Angle2DRad(p1, p2, 90);
        float a2 = RealWorldTerrainUtils.Angle2DRad(p2, p3, 90);
        Vector3 off1 = new Vector3(Mathf.Cos(a1) * wallWidth, 0, Mathf.Sin(a1) * wallWidth);
        Vector3 off2 = new Vector3(Mathf.Cos(a2) * wallWidth, 0, Mathf.Sin(a2) * wallWidth);
        Vector3 p21 = p2 + off1;
        Vector3 p22 = p2 - off1;
        Vector3 p31 = p2 + off2;
        Vector3 p32 = p2 - off2;
        int state1, state2;
        Vector2 is1 = RealWorldTerrainUtils.GetIntersectionPointOfTwoLines(p1 + off1, p21, p31, p3 + off2, out state1);
        Vector2 is2 = RealWorldTerrainUtils.GetIntersectionPointOfTwoLines(p1 - off1, p22, p32, p3 - off2, out state2);
        if (state1 == 1) side1.Add(new Vector3(is1.x, (p21.y + p31.y) / 2, is1.y));
        if (state2 == 1) side2.Add(new Vector3(is2.x, (p22.y + p32.y) / 2, is2.y));
    }

    private static void AddWallBaseVerticlesEnd(Vector3[] points, int i, float wallWidth, List<Vector3> side1,
        List<Vector3> side2)
    {
        Vector3 p = points[i];
        float a = RealWorldTerrainUtils.Angle2DRad(points[i - 1], p, 90);
        Vector3 off = new Vector3(Mathf.Cos(a) * wallWidth, 0, Mathf.Sin(a) * wallWidth);
        side1.Add(p + off);
        side2.Add(p - off);
    }

    private static void AddWallBaseVerticlesStart(Vector3[] points, int i, float wallWidth, List<Vector3> side1,
        List<Vector3> side2)
    {
        Vector3 p = points[i];
        float a = RealWorldTerrainUtils.Angle2DRad(p, points[i + 1], 90);
        Vector3 off = new Vector3(Mathf.Cos(a) * wallWidth, 0, Mathf.Sin(a) * wallWidth);
        side1.Add(p + off);
        side2.Add(p - off);
    }

    private static void AnalizeHouseRoofType(RealWorldTerrainOSMWay way, ref float baseHeight,
        ref RealWorldTerrainOSMRoofType roofType, ref float roofHeight)
    {
        string roofShape = way.GetTagValue("roof:shape");
        string roofHeightStr = way.GetTagValue("roof:height");
        string minHeightStr = way.GetTagValue("min_height");
        if (!String.IsNullOrEmpty(roofShape))
        {
            if ((roofShape == "dome" || roofShape == "pyramidal") && !String.IsNullOrEmpty(roofHeightStr))
            {
                GetHeightFromString(roofHeightStr, ref roofHeight);
                baseHeight -= roofHeight;
                roofType = RealWorldTerrainOSMRoofType.dome;
            }
        }
        else if (!String.IsNullOrEmpty(roofHeightStr))
        {
            GetHeightFromString(roofHeightStr, ref roofHeight);
            baseHeight -= roofHeight;
            roofType = RealWorldTerrainOSMRoofType.dome;
        }
        else if (!String.IsNullOrEmpty(minHeightStr))
        {
            float totalHeight = baseHeight;
            GetHeightFromString(minHeightStr, ref baseHeight);
            roofHeight = totalHeight - baseHeight;
            roofType = RealWorldTerrainOSMRoofType.dome;
        }
    }

    private static void AnalizeHouseTags(RealWorldTerrainOSMWay way, ref Material wallMaterial,
        ref Material roofMaterial, ref float baseHeight)
    {
        string heightStr = way.GetTagValue("height");
        string levelsStr = way.GetTagValue("building:levels");
        GetHeightFromString(heightStr, ref baseHeight);
        if (String.IsNullOrEmpty(heightStr) && !String.IsNullOrEmpty(levelsStr))
            baseHeight = float.Parse(levelsStr) * 3.5f;
        else baseHeight = RealWorldTerrain.prefs.buildingLevelLimits.Random() * 3.5f;

        string colorStr = way.GetTagValue("building:colour");
        if (!String.IsNullOrEmpty(colorStr))
            wallMaterial.color = roofMaterial.color = RealWorldTerrainUtils.StringToColor(colorStr);
    }

    private static void CreateHouse(RealWorldTerrainOSMWay way, RealWorldTerrainContainer globalContainer)
    {
        List<Vector3> points = RealWorldTerrainOSM.GetGlobalPointsFromWay(way, nodes);
        if (points.Count < 3) return;
        if (points.First() == points.Last())
        {
            points.Remove(points.Last());
            if (points.Count < 3) return;
        }

        for (int i = 0; i < points.Count; i++)
        {
            Vector3 p = RealWorldTerrainEditorUtils.GlobalToLocalWithElevation(points[i], globalContainer.scale);
            p.y = RealWorldTerrainEditorUtils.NormalizeElevation(globalContainer, p.y);
            points[i] = p;
        }

        Vector3 centerPoint = Vector3.zero;
        centerPoint = points.Aggregate(centerPoint, (current, point) => current + point) / points.Count;
        centerPoint.y = points.Min(p => p.y);

        bool generateWall = true;

        if (way.HasTagKey("building"))
        {
            string buildingType = way.GetTagValue("building");
            if (buildingType == "roof") generateWall = false;
        }

        float baseHeight = 15;
        float roofHeight = 0;

        Material wallMaterial = GetMaterialByTags("OSM-House-Wall-Material", way.tags, defHouseWallMaterial);
        Material roofMaterial = GetMaterialByTags("OSM-House-Roof-Material", way.tags, defHouseRoofMaterial);

        RealWorldTerrainOSMRoofType roofType = RealWorldTerrainOSMRoofType.flat;
        AnalizeHouseTags(way, ref wallMaterial, ref roofMaterial, ref baseHeight);
        AnalizeHouseRoofType(way, ref baseHeight, ref roofType, ref roofHeight);

        Vector3[] baseVerticles = points.Select(p => p - centerPoint).ToArray();

        GameObject houseGO = RealWorldTerrainUtils.CreateGameObject(houseContainer, "House " + way.id);
        houseGO.transform.position = centerPoint;

        RealWorldTerrainOSMBuilding house = houseGO.AddComponent<RealWorldTerrainOSMBuilding>();
        house.baseHeight = baseHeight;
        house.baseVerticles = baseVerticles;
        house.container = RealWorldTerrain.container;
        house.roofHeight = roofHeight;
        house.roofType = roofType;
        house.type = RealWorldTerrainOSMBuildingType.house;

        house.wall = generateWall ? CreateHouseWall(houseGO, baseVerticles, globalContainer.scale, baseHeight, wallMaterial, way.id) : null;
        house.roof = CreateHouseRoof(houseGO, baseVerticles, globalContainer.scale, baseHeight, roofHeight, roofType, roofMaterial, way.id);

        houseGO.AddComponent<RealWorldTerrainMeta>().GetFromOSM(way);
    }

    private static MeshFilter CreateHouseRoof(GameObject houseGO, Vector3[] baseVerticles, Vector3 scale,
        float baseHeight, float roofHeight, RealWorldTerrainOSMRoofType roofType, Material material, string id)
    {
        GameObject wallGO = new GameObject("Roof");
        wallGO.transform.parent = houseGO.transform;
        wallGO.transform.localPosition = Vector3.zero;

        return RealWorldTerrainOSM.AppendMesh(wallGO,
            CreateHouseRoofMesh(baseVerticles, scale, baseHeight, roofHeight, roofType, houseGO.name), material,
            string.Format("House_{0}_Roof", id));
    }

    private static void CreateHouseRoofDome(Vector3 scale, float height, List<Vector3> vertices, List<int> triangles)
    {
        Vector3 roofTopPoint = Vector3.zero;
        roofTopPoint = vertices.Aggregate(roofTopPoint, (current, point) => current + point) / vertices.Count;
        roofTopPoint.y = height * scale.y;
        int vIndex = vertices.Count;

        for (int i = 0; i < vertices.Count; i++)
        {
            int p1 = i;
            int p2 = i + 1;
            if (p2 >= vertices.Count) p2 -= vertices.Count;

            triangles.AddRange(new[] {p1, p2, vIndex});
        }

        vertices.Add(roofTopPoint);
    }

    private static Mesh CreateHouseRoofMesh(Vector3[] baseVerticles, Vector3 scale, float baseHeight, float roofHeight,
        RealWorldTerrainOSMRoofType roofType, string name, bool inverted = false)
    {
        List<Vector2> roofPoints = new List<Vector2>();
        List<Vector3> vertices = new List<Vector3>();

        CreateHouseRoofVerticles(baseVerticles, vertices, roofPoints, scale, baseHeight);
        int[] triangles =
            CreateHouseRoofTriangles(scale, vertices, roofType, roofPoints, baseHeight, roofHeight).ToArray();

        Vector3 side1 = vertices[triangles[1]] - vertices[triangles[0]];
        Vector3 side2 = vertices[triangles[2]] - vertices[triangles[0]];
        Vector3 perp = Vector3.Cross(side1, side2);

        bool reversed = perp.y < 0;
        if (inverted) reversed = !reversed;
        if (reversed) triangles = triangles.Reverse().ToArray();

        float minX = vertices.Min(p => p.x);
        float minZ = vertices.Min(p => p.z);
        float maxX = vertices.Max(p => p.x);
        float maxZ = vertices.Max(p => p.z);
        float offX = maxX - minX;
        float offZ = maxZ - minZ;

        Vector2[] uvs = vertices.Select(v => new Vector2((v.x - minX) / offX, (v.z - minZ) / offZ)).ToArray();

        Mesh mesh = new Mesh
        {
            name = name + " Roof",
            vertices = vertices.ToArray(),
            uv = uvs,
            triangles = triangles.ToArray()
        };

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        return mesh;
    }

    private static List<int> CreateHouseRoofTriangles(Vector3 scale, List<Vector3> vertices,
        RealWorldTerrainOSMRoofType roofType, List<Vector2> roofPoints, float baseHeight, float roofHeight)
    {
        List<int> triangles = new List<int>();
        if (roofType == RealWorldTerrainOSMRoofType.flat)
            triangles.AddRange(RealWorldTerrainUtils.Triangulate(roofPoints).Select(index => index));
        else if (roofType == RealWorldTerrainOSMRoofType.dome)
            CreateHouseRoofDome(scale, baseHeight + roofHeight, vertices, triangles);
        return triangles;
    }

    private static void CreateHouseRoofVerticles(Vector3[] baseVerticles, List<Vector3> verticles,
        List<Vector2> roofPoints, Vector3 scale, float baseHeight)
    {
        float topPoint = baseHeight * scale.y;
        foreach (Vector3 p in baseVerticles)
        {
            Vector3 tv = new Vector3(p.x, topPoint, p.z);
            Vector2 rp = new Vector2(p.x, p.z);

            if (!verticles.Contains(tv)) verticles.Add(tv);
            if (!roofPoints.Contains(rp)) roofPoints.Add(rp);
        }
    }

    private static MeshFilter CreateHouseWall(GameObject houseGO, Vector3[] baseVerticles, Vector3 scale,
        float baseHeight, Material material, string id)
    {
        GameObject wallGO = new GameObject("Wall");
        wallGO.transform.parent = houseGO.transform;
        wallGO.transform.localPosition = Vector3.zero;

        return RealWorldTerrainOSM.AppendMesh(wallGO,
            CreateHouseWallMesh(baseVerticles, scale, baseHeight, houseGO.name), material, string.Format("House_{0}_Wall", id));
    }

    private static Mesh CreateHouseWallMesh(Vector3[] baseVerticles, Vector3 scale, float baseHeight, string name,
        bool inverted = false)
    {
        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();

        bool reversed = CreateHouseWallVerticles(scale, baseHeight, baseVerticles, vertices, uvs);
        if (inverted) reversed = !reversed;
        int[] triangles = CreateHouseWallTriangles(vertices, reversed).ToArray();

        Mesh mesh = new Mesh
        {
            name = name + " Wall",
            vertices = vertices.ToArray(),
            uv = uvs.ToArray(),
            triangles = triangles.ToArray()
        };

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        return mesh;
    }

    private static List<int> CreateHouseWallTriangles(List<Vector3> vertices, bool reversed)
    {
        List<int> triangles = new List<int>();
        for (int i = 0; i < vertices.Count / 2 - 1; i++)
            triangles.AddRange(GetHouseWallTriangle(vertices.Count, reversed, i));
        return triangles;
    }

    private static bool CreateHouseWallVerticles(Vector3 scale, float baseHeight, Vector3[] baseVerticles,
        List<Vector3> vertices, List<Vector2> uvs)
    {
        float angle = 0;
        float topPoint = baseHeight * scale.y;

        Vector2 vp1 = Vector2.zero;
        Vector2 vp2 = Vector2.zero;

        foreach (Vector3 p in baseVerticles)
            angle += AddHouseWallVerticle(vertices, p, topPoint, ref vp1, ref vp2, angle);
        AddHouseWallVerticle(vertices, baseVerticles.First(), topPoint, ref vp1, ref vp2, angle);

        float totalDistance = 0;

        for (int i = 0; i < vertices.Count / 2; i++)
        {
            int i1 = Mathf.RoundToInt(Mathf.Repeat(i * 2, vertices.Count));
            int i2 = Mathf.RoundToInt(Mathf.Repeat((i + 1) * 2, vertices.Count));
            totalDistance += (vertices[i1] - vertices[i2]).magnitude;
        }

        float currentDistance = 0;

        for (int i = 0; i < vertices.Count / 2; i++)
        {
            int i1 = Mathf.RoundToInt(Mathf.Repeat(i * 2, vertices.Count));
            int i2 = Mathf.RoundToInt(Mathf.Repeat((i + 1) * 2, vertices.Count));
            float curU = currentDistance / totalDistance;
            uvs.Add(new Vector2(curU, 0));
            uvs.Add(new Vector2(curU, 1));

            currentDistance += (vertices[i1] - vertices[i2]).magnitude;
        }

        return angle > 0;
    }

    private static Mesh CreateWallFrontMesh(Vector3[] baseVerticles, Vector3 scale, float wallHeight)
    {
        wallHeight *= scale.y;
        List<Vector3> vertices = GetWallBaseVerticles(baseVerticles, scale);
        List<Vector2> uvs = new List<Vector2>();
        List<Vector2> uvsTop = new List<Vector2>();

        vertices.Add(vertices.First());

        int vCount = vertices.Count;
        float totalDistance = 0;
        for (int i = 0; i < vCount; i++)
        {
            Vector3 v = vertices[i];
            vertices.Add(new Vector3(v.x, v.y + wallHeight, v.z));
            if (i < vCount - 1) totalDistance += (v - vertices[i + 1]).magnitude;
        }

        float currentDistance = 0;

        for (int i = 0; i < vCount; i++)
        {
            int i1 = Mathf.RoundToInt(Mathf.Repeat(i + 1, vCount));
            float curU = currentDistance / totalDistance;
            uvs.Add(new Vector2(curU, 0));
            uvsTop.Add(new Vector2(curU, 1));
            currentDistance += (vertices[i] - vertices[i1]).magnitude;
        }

        uvs.AddRange(uvsTop);

        int[] triangles = GetWallFrontTriangles(vertices, vCount).ToArray();

        Mesh mesh = new Mesh
        {
            vertices = vertices.ToArray(),
            uv = uvs.ToArray(),
            triangles = triangles.ToArray()
        };

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        return mesh;
    }

    private static Mesh CreateWallTopMesh(Vector3[] baseVerticles, Vector3 scale, float wallHeight)
    {
        wallHeight *= scale.y;
        List<Vector3> vertices = GetWallBaseVerticles(baseVerticles, scale);
        List<Vector2> uvs = new List<Vector2>();
        List<Vector2> uvsReverse = new List<Vector2>();

        for (int i = 0; i < vertices.Count; i++)
        {
            Vector3 v = vertices[i];
            v.y += wallHeight;
            vertices[i] = v;
        }

        float totalDistance = 0;
        for (int i = 0; i < vertices.Count / 2 - 1; i++) totalDistance += (vertices[i] - vertices[i + 1]).magnitude;

        float currentDistance = 0;
        for (int i = 0; i < vertices.Count / 2; i++)
        {
            float curU = currentDistance / totalDistance;
            uvs.Add(new Vector2(curU, 0));
            uvsReverse.Add(new Vector2(curU, 1));
            if (i < vertices.Count / 2 - 1) currentDistance += (vertices[i] - vertices[i + 1]).magnitude;
        }

        uvsReverse.Reverse();
        uvs.AddRange(uvsReverse);

        int[] triangles = GetWallTopTriangles(vertices).ToArray();

        Mesh mesh = new Mesh
        {
            vertices = vertices.ToArray(),
            uv = uvs.ToArray(),
            triangles = triangles.ToArray()
        };

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        return mesh;
    }

    public static void Dispose()
    {
        loaded = false;

        ways = null;
        nodes = null;
        relations = null;

        defHouseRoofMaterial = null;
        defHouseWallMaterial = null;

        baseContainer = null;
        houseContainer = null;
        RealWorldTerrainOSMBuildREditor.alreadyCreated = null;
    }

    public static void Download()
    {
        if (!prefs.generateBuildings || File.Exists(compressedFilename)) return;
        if (File.Exists(filename))
        {
            byte[] data = File.ReadAllBytes(filename);
            OnDownloadComplete(ref data);
        }
        else
        {
            RealWorldTerrainDownloaderItem item = RealWorldTerrainDownloader.Add(url, filename, RealWorldTerrainDownloadType.data, "OSM roads", 600000);
            item.OnComplete += OnDownloadComplete;
        }
    }

    public static string FixPathString(string path)
    {
        return path.Replace(new[] {":", "/", "\\", "="}, "-");
    }

    public static void Generate(RealWorldTerrainContainer globalContainer)
    {
        if (!loaded)
        {
            Load();

            if (ways.Count == 0)
            {
                RealWorldTerrain.phaseComplete = true;
                return;
            }

            baseContainer = RealWorldTerrainUtils.CreateGameObject(globalContainer, "Buildings");
            houseContainer = RealWorldTerrainUtils.CreateGameObject(baseContainer, "Houses");
#if UNITY_4_3 || UNITY_4_5 || UNITY_4_6
            defHouseWallMaterial = RealWorldTerrainEditorUtils.FindMaterial("Default-House-Wall-Material.mat");
            defHouseRoofMaterial = RealWorldTerrainEditorUtils.FindMaterial("Default-House-Roof-Material.mat");
#else
            defHouseWallMaterial = RealWorldTerrainEditorUtils.FindMaterial("Default-House-Wall-Material.mat");
            defHouseRoofMaterial = RealWorldTerrainEditorUtils.FindMaterial("Default-House-Roof-Material.mat");
#endif
            globalContainer.generatedBuildings = true;
        }

        GenerateHouses(globalContainer);

        if (!RealWorldTerrain.phaseComplete) RealWorldTerrain.phaseProgress = RealWorldTerrain.phaseIndex / (float)(ways.Count);
        else RealWorldTerrain.phaseProgress = 1;
    }

    private static void GenerateHouses(RealWorldTerrainContainer globalContainer)
    {
        long startTime = DateTime.Now.Ticks;

        while (RealWorldTerrain.phaseIndex < ways.Count)
        {
            if (new TimeSpan(DateTime.Now.Ticks - startTime).TotalSeconds > 1) return;

            RealWorldTerrainOSMWay way = ways[RealWorldTerrain.phaseIndex];
            RealWorldTerrain.phaseIndex++;

            if (way.GetTagValue("building") == "bridge") continue;
            string layer = way.GetTagValue("layer");
            if (!String.IsNullOrEmpty(layer) && Int32.Parse(layer) < 0) continue;

            CreateHouse(way, globalContainer);
        }

        RealWorldTerrain.phaseComplete = true;
    }

    private static void GetHeightFromString(string str, ref float height)
    {
        if (!String.IsNullOrEmpty(str))
        {
            if (!float.TryParse(str, out height))
            {
                if (str.Substring(str.Length - 2, 2) == "cm")
                {
                    float.TryParse(str.Substring(0, str.Length - 2), out height);
                    height /= 10;
                }
                else if (str.Substring(str.Length - 1, 1) == "m")
                    float.TryParse(str.Substring(0, str.Length - 1), out height);
            }
        }
    }

    private static int[] GetHouseWallTriangle(int countVertices, bool reversed, int i)
    {
        int p1 = i * 2;
        int p2 = (i + 1) * 2;
        int p3 = (i + 1) * 2 + 1;
        int p4 = i * 2 + 1;

        if (p2 >= countVertices) p2 -= countVertices;
        if (p3 >= countVertices) p3 -= countVertices;

        if (reversed) return new[] {p1, p4, p3, p1, p3, p2};
        return new[] {p2, p3, p1, p3, p4, p1};
    }

    public static Material GetMaterialByTags(string materialName, List<RealWorldTerrainOSMTag> tags, Material defaultMaterial)
    {
        if (RealWorldTerrainOSM.projectMaterials == null)
            RealWorldTerrainOSM.projectMaterials = Directory.GetFiles("Assets", "*.mat", SearchOption.AllDirectories);
        foreach (RealWorldTerrainOSMTag tag in tags)
        {
            string matName = string.Format("{0}({1}={2})", materialName, FixPathString(tag.key), FixPathString(tag.value));
            foreach (string projectMaterial in RealWorldTerrainOSM.projectMaterials)
            {
                if (projectMaterial.Contains(matName))
                {
                    string assetFN = projectMaterial.Replace("\\", "/");
                    return new Material((Material)AssetDatabase.LoadAssetAtPath(assetFN, typeof(Material)));
                }
            }
        }

        return new Material(defaultMaterial) { color = Color.white }; ;
    }

    private static List<Vector3> GetWallBaseVerticles(Vector3[] points, Vector3 scale)
    {
        float wallWidth = scale.x * RealWorldTerrainUtils.arcSecond / 6f;
        List<Vector3> side1 = new List<Vector3>();
        List<Vector3> side2 = new List<Vector3>();
        List<Vector3> verticles = new List<Vector3>();
        for (int i = 0; i < points.Length; i++)
        {
            if (i == 0) AddWallBaseVerticlesStart(points, i, wallWidth, side1, side2);
            else if (i == points.Length - 1) AddWallBaseVerticlesEnd(points, i, wallWidth, side1, side2);
            else AddWallBaseVerticles(points, i, wallWidth, side1, side2);
        }

        side2.Reverse();
        verticles.AddRange(side1);
        verticles.AddRange(side2);
        return verticles;
    }

    private static List<int> GetWallFrontTriangles(List<Vector3> verticles, int vCount)
    {
        List<int> triangles = new List<int>();
        for (int i = 0; i < verticles.Count / 2 - 1; i++)
        {
            int p1 = i;
            int p2 = i + 1;
            int p3 = i + vCount + 1;
            int p4 = i + vCount;

            if (p2 >= vCount) p2 -= vCount;
            if (p3 >= verticles.Count) p3 -= vCount;

            triangles.AddRange(new[] {p2, p3, p1, p3, p4, p1});
        }
        return triangles;
    }

    private static List<int> GetWallTopTriangles(List<Vector3> verticles)
    {
        List<int> triangles = new List<int>();
        int vCount = verticles.Count;
        for (int i = 0; i < verticles.Count / 2; i++)
        {
            int p1 = i;
            int p2 = i + 1;
            int p3 = vCount - i - 1;
            int p4 = vCount - i - 2;

            if (p2 >= vCount) p2 -= vCount;
            if (p4 < 0) p4 += vCount;

            triangles.AddRange(new[] {p2, p3, p1, p3, p2, p4});
        }
        return triangles;
    }

    public static void Load()
    {
        RealWorldTerrainOSM.LoadOSM(compressedFilename, out nodes, out ways, out relations);
        loaded = true;
    }

    private static void OnDownloadComplete(ref byte[] data)
    {
        RealWorldTerrainOSM.GenerateCompressedFile(data, ref nodes, ref ways, ref relations, compressedFilename);
    }

    public void OnEnable()
    {
        building = (RealWorldTerrainOSMBuilding) target;
    }

    public override void OnInspectorGUI()
    {
        building.baseHeight = EditorGUILayout.FloatField("Base Height (meters): ", building.baseHeight);

        if (building.type == RealWorldTerrainOSMBuildingType.house)
        {
            building.roofType =
                (RealWorldTerrainOSMRoofType) EditorGUILayout.EnumPopup("Roof type: ", building.roofType);
            if (building.roofType != RealWorldTerrainOSMRoofType.flat)
                building.roofHeight = EditorGUILayout.FloatField("Roof Height (meters): ", building.roofHeight);

            if (GUILayout.Button("Invert wall normals") && building.wall != null)
            {
                building.invertWall = !building.invertWall;
                building.wall.mesh =
                    building.wall.sharedMesh =
                        CreateHouseWallMesh(building.baseVerticles, building.container.scale, building.baseHeight,
                            building.name,
                            building.invertWall);
            }

            if (GUILayout.Button("Invert roof normals") && building.roof != null)
            {
                building.invertRoof = !building.invertRoof;
                building.roof.mesh =
                    building.roof.sharedMesh =
                        CreateHouseRoofMesh(building.baseVerticles, building.container.scale, building.baseHeight,
                            building.roofHeight, building.roofType, building.name, building.invertRoof);
            }
        }

        if (GUILayout.Button("Update"))
        {
            if (building.type == RealWorldTerrainOSMBuildingType.house)
            {
                if (building.wall != null)
                {
                    building.wall.mesh =
                        building.wall.sharedMesh =
                            CreateHouseWallMesh(building.baseVerticles, building.container.scale, building.baseHeight,
                                building.name,
                                building.invertWall);
                }

                if (building.roof != null)
                {
                    building.roof.mesh =
                        building.roof.sharedMesh =
                            CreateHouseRoofMesh(building.baseVerticles, building.container.scale, building.baseHeight,
                                building.roofHeight, building.roofType, building.name, building.invertRoof);
                }
            }
            else if (building.type == RealWorldTerrainOSMBuildingType.wall)
            {
                if (building.wall != null)
                {
                    building.wall.mesh =
                        building.wall.sharedMesh =
                            CreateWallFrontMesh(building.baseVerticles, building.container.scale, building.baseHeight);
                }
                if (building.roof != null)
                {
                    building.roof.mesh =
                        building.roof.sharedMesh =
                            CreateWallTopMesh(building.baseVerticles, building.container.scale, building.baseHeight);
                }
            }
        }

        if (GUILayout.Button("Export mesh to OBJ"))
        {
            string path = EditorUtility.SaveFilePanel("Save building to OBJ", "", building.name + ".obj", "obj");
            if (path.Length != 0)
            {
                if (building.type == RealWorldTerrainOSMBuildingType.house)
                    RealWorldTerrainUtils.ExportMesh(path, building.wall, building.roof);
                else if (building.type == RealWorldTerrainOSMBuildingType.wall)
                    RealWorldTerrainUtils.ExportMesh(path, building.GetComponent<MeshFilter>());
            }
        }
    }
}